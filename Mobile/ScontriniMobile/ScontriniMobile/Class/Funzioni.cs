﻿using ScontriniMobile.Class;
using ScontriniOratori;
using System;
using System.Collections.Generic;

namespace ScontriniMobile.Views
{
    internal class Funzioni
    {
        internal static List<Prodotto> Trova(string text)
        {
            List<Prodotto> LP = new List<Prodotto>();
            foreach(Prodotto p in Variabili.Magazzino)
            {
                string b = p.ToString();
                int a = b.IndexOf(text);      
                if (a>=0 && a<b.Length)
                {
                    LP.Add(p);
                }
            }
            return LP;
        }

        internal static void Cose_Iniziali()
        {
            Funzioni.CaricaProdotti();
            Funzioni.SvuotaCarrello();
        }

        private static void SvuotaCarrello()
        {
            if (Variabili.Carrello == null)
            {
                Variabili.Carrello = new List<Prodotto>();
            }
            else
            {
                Variabili.Carrello.Clear();
            }
        }

        private static void CaricaProdotti()
        {
            Variabili.Magazzino = new List<Prodotto>
            {

                //TODO: per ora metto prodotti a caso, in futuro cambiare
                new Prodotto(),
                new Prodotto(),
                new Prodotto()
            };
        }
    }
}