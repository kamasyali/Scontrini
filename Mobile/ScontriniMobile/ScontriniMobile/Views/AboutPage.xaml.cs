﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScontriniMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage(string v)
        {
            InitializeComponent();

            Title = v;
            
            if (Title=="Ricerca")
            {
                RisultatiRicerca = (StackLayout)FindByName("RisultatiRicerca");
                BarraRicerca = (SearchBar)FindByName("BarraRicerca");
                BarraRicerca.SearchButtonPressed += new EventHandler(Premuto_Ricerca);
            }
        }

        private void Premuto_Ricerca(object sender, EventArgs e2)
        {
            //TODO: fare la ricerca
            List<Prodotto> LP = Funzioni.Trova(BarraRicerca.Text);
            RisultatiRicerca.Children.Clear();
            foreach (Prodotto p in LP)
            {
                Label v = new Label
                {
                    Text = p.ToString()
                };
                // Your label tap event
                var forgetPassword_tap = new TapGestureRecognizer();
                forgetPassword_tap.Tapped += (s, e) =>
                {
                    Premuto_Prodotto(s, e);
                };
                v.GestureRecognizers.Add(forgetPassword_tap);
                RisultatiRicerca.Children.Add(v);
            }
        }

        private void Premuto_Prodotto(object s, EventArgs e)
        {
            //TODO: si aggiunge il prodotto al carrello
            //TODO: si aggiorna il carrello
        }
    }
}