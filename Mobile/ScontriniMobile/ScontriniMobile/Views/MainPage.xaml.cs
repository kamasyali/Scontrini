﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ScontriniMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {

        public MainPage()
        {
            InitializeComponent();

            var x = (TabbedPage)FindByName("TabbedMenu");
            NavigationPage x2 = new NavigationPage(new AboutPage("Ricerca"))
            {
                Title = "Ricerca"
            };

            x.Children.Add(x2);

            Funzioni.Cose_Iniziali();

        }

    }
}