﻿using System;
using System.Runtime.Serialization;

namespace ScontriniOratori
{
    [Serializable]
    public class MainModeUndefinedException : Exception
    {
        public MainModeUndefinedException()
        {
        }

        public MainModeUndefinedException(string message) : base(message)
        {
        }

        public MainModeUndefinedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MainModeUndefinedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}