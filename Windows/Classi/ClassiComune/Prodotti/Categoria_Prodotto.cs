﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScontriniOratorio.Classi.ClassiComune.Prodotti
{
    public class Categoria_Prodotto
    {
        public List<SottoCategoria_Prodotto> L;
        public string nome;

        public Categoria_Prodotto(string nome)
        {
            this.nome = nome;
            this.L = new List<SottoCategoria_Prodotto>();
        }

        internal SottoCategoria_Prodotto get(int i)
        {
            return this.L[i];
        }

        internal int Count()
        {
            return this.L.Count;
        }

        internal void AddProduct(int v, Prodotto prodotto)
        {
            this.L[v].Add(prodotto);
        }
    }
}