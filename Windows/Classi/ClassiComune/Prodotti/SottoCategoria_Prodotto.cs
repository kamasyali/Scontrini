﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScontriniOratorio.Classi.ClassiComune.Prodotti
{
    public class SottoCategoria_Prodotto
    {
        public List<Prodotto> L;
        public string nome;

        public SottoCategoria_Prodotto(string nome)
        {
            this.nome = nome;
            this.L = new List<Prodotto>();
        }

        internal int Count()
        {
            return this.L.Count;
        }

        internal Prodotto Get(int j)
        {
            return this.L[j];
        }

        internal void Scambia(int a1, int a2)
        {
            Prodotto t = this.L[a1];
            this.L[a1] = this.L[a2];
            this.L[a2] = t;
        }

        internal void Add(Prodotto prodotto)
        {
            this.L.Add(prodotto);
        }

        internal void SetQuantity(int j, int v)
        {
            this.L[j].quantita = v;
        }
    }
}