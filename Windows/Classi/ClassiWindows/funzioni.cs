﻿using Common;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using ScontriniOratorio.Forms;
using ScontriniOratorio.Forms.Main;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Numerics;
using System.Windows.Forms;

namespace ScontriniOratori
{
    internal static class Funzioni
    {
        internal static void Aspetta(int v)
        {
            Stopwatch sw2 = new Stopwatch(); // sw cotructor
            sw2.Start(); // starts the stopwatch
            for (int i = 0; ; i++)
            {
                if (i % 100000 == 0) // if in 100000th iteration (could be any other large number
                                     // depending on how often you want the time to be checked)
                {
                    sw2.Stop(); // stop the time measurement
                    if (sw2.ElapsedMilliseconds > v) // check if desired period of time has elapsed
                    {
                        break; // if more than 5000 milliseconds have passed, stop looping and return
                               // to the existing code
                    }
                    else
                    {
                        sw2.Start(); // if less than 5000 milliseconds have elapsed, continue looping
                                     // and resume time measurement
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode")]
        public static void Prepara_pannelli_new(Size dim)
        {
            const int rientranza = 10;
            Variabili.Pannelli.Clear();
            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                Variabili.Pannelli.Add(new List<Pannello_intestato>());
            }
            //Console.WriteLine(dim);

            //PIZZE
            Variabili.Pannelli[0].Add(new Pannello_intestato());
            Variabili.Pannelli[0][0].p.Size = new Size(dim.Width - rientranza, dim.Height / 4 * 3);
            Variabili.Pannelli[0][0].p.Location = new Point(0, 0);
            Variabili.Pannelli[0][0].p.Parent = Variabili.Tabs.TabPages[0];
            Variabili.Pannelli[0][0].p.AutoScroll = true;
            Variabili.Pannelli[0][0].t.Text = "Pizze";
            Variabili.Pannelli[0][0].ShowText = true;

            Variabili.Pannelli[2].Add(new Pannello_intestato());
            Variabili.Pannelli[2][0].p.Size = new Size((dim.Width - rientranza) / 2, dim.Height / 4);
            Variabili.Pannelli[2][0].p.Location = new Point(0, dim.Height / 4 * 3);
            Variabili.Pannelli[2][0].p.Parent = Variabili.Tabs.TabPages[0];
            Variabili.Pannelli[2][0].p.AutoScroll = true;
            Variabili.Pannelli[2][0].t.Text = "Patatine";
            Variabili.Pannelli[2][0].ShowText = true;

            //BEVANDE
            Variabili.Pannelli[1].Add(new Pannello_intestato());
            Variabili.Pannelli[1][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[1][0].p.Location = new Point(0, 0);
            Variabili.Pannelli[1][0].p.Parent = Variabili.Tabs.TabPages[1];
            Variabili.Pannelli[1][0].p.AutoScroll = true;
            Variabili.Pannelli[1][0].t.Text = "Acqua";
            Variabili.Pannelli[1][0].ShowText = true;

            Variabili.Pannelli[1].Add(new Pannello_intestato());
            Variabili.Pannelli[1][1].p.Size = new Size((dim.Width - rientranza), dim.Height / 6 * 2);
            Variabili.Pannelli[1][1].p.Location = new Point(0, dim.Height / 6);
            Variabili.Pannelli[1][1].p.Parent = Variabili.Tabs.TabPages[1];
            Variabili.Pannelli[1][1].p.AutoScroll = true;
            Variabili.Pannelli[1][1].t.Text = "Bibite";
            Variabili.Pannelli[1][1].ShowText = true;

            Variabili.Pannelli[1].Add(new Pannello_intestato());
            Variabili.Pannelli[1][2].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[1][2].p.Location = new Point(0, dim.Height / 6 * 3);
            Variabili.Pannelli[1][2].p.Parent = Variabili.Tabs.TabPages[1];
            Variabili.Pannelli[1][2].p.AutoScroll = true;
            Variabili.Pannelli[1][2].t.Text = "Vino";
            Variabili.Pannelli[1][2].ShowText = true;

            Variabili.Pannelli[15].Add(new Pannello_intestato());
            Variabili.Pannelli[15].Add(new Pannello_intestato());
            Variabili.Pannelli[15][1].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[15][1].p.Location = new Point(0, dim.Height / 6 * 4);
            Variabili.Pannelli[15][1].p.Parent = Variabili.Tabs.TabPages[1];
            Variabili.Pannelli[15][1].p.AutoScroll = true;
            Variabili.Pannelli[15][1].t.Text = "Alcolici";
            Variabili.Pannelli[15][1].ShowText = true;

            Variabili.Pannelli[1].Add(new Pannello_intestato());
            Variabili.Pannelli[1][3].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[1][3].p.Location = new Point(0, dim.Height / 6 * 5);
            Variabili.Pannelli[1][3].p.Parent = Variabili.Tabs.TabPages[1];
            Variabili.Pannelli[1][3].p.AutoScroll = true;
            Variabili.Pannelli[1][3].t.Text = "Birra";
            Variabili.Pannelli[1][3].ShowText = true;

            //SELF
            Variabili.Pannelli[4].Add(new Pannello_intestato());
            Variabili.Pannelli[4][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[4][0].p.Location = new Point(0, dim.Height / 6 * 0);
            Variabili.Pannelli[4][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[4][0].p.AutoScroll = true;
            Variabili.Pannelli[4][0].t.Text = "Primi piatti";
            Variabili.Pannelli[4][0].ShowText = true;

            Variabili.Pannelli[5].Add(new Pannello_intestato());
            Variabili.Pannelli[5][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[5][0].p.Location = new Point(0, dim.Height / 6 * 1);
            Variabili.Pannelli[5][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[5][0].p.AutoScroll = true;
            Variabili.Pannelli[5][0].t.Text = "Secondi piatti";
            Variabili.Pannelli[5][0].ShowText = true;

            Variabili.Pannelli[6].Add(new Pannello_intestato());
            Variabili.Pannelli[6][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[6][0].p.Location = new Point(0, dim.Height / 6 * 2);
            Variabili.Pannelli[6][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[6][0].p.AutoScroll = true;
            Variabili.Pannelli[6][0].t.Text = "Secondi freddi";
            Variabili.Pannelli[6][0].ShowText = true;

            Variabili.Pannelli[7].Add(new Pannello_intestato());
            Variabili.Pannelli[7][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[7][0].p.Location = new Point(0, dim.Height / 6 * 3);
            Variabili.Pannelli[7][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[7][0].p.AutoScroll = true;
            Variabili.Pannelli[7][0].t.Text = "Contorni";
            Variabili.Pannelli[7][0].ShowText = true;

            Variabili.Pannelli[19].Add(new Pannello_intestato());
            Variabili.Pannelli[19][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[19][0].p.Location = new Point(0, dim.Height / 6 * 4);
            Variabili.Pannelli[19][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[19][0].p.AutoScroll = true;
            Variabili.Pannelli[19][0].t.Text = "Menu";
            Variabili.Pannelli[19][0].ShowText = true;

            Variabili.Pannelli[9].Add(new Pannello_intestato());
            Variabili.Pannelli[9][0].p.Size = new Size((dim.Width - rientranza) / 2, dim.Height / 6);
            Variabili.Pannelli[9][0].p.Location = new Point(0, dim.Height / 6 * 5);
            Variabili.Pannelli[9][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[9][0].p.AutoScroll = true;
            Variabili.Pannelli[9][0].t.Text = "Panini";
            Variabili.Pannelli[9][0].ShowText = true;

            Variabili.Pannelli[8].Add(new Pannello_intestato());
            Variabili.Pannelli[8][0].p.Size = new Size((dim.Width - rientranza) / 2, dim.Height / 6);
            Variabili.Pannelli[8][0].p.Location = new Point((dim.Width - rientranza) / 2, dim.Height / 6 * 5);
            Variabili.Pannelli[8][0].p.Parent = Variabili.Tabs.TabPages[2];
            Variabili.Pannelli[8][0].p.AutoScroll = true;
            Variabili.Pannelli[8][0].t.Text = "Dolci";
            Variabili.Pannelli[8][0].ShowText = true;

            //BAR
            const int caffetteria = 11;
            Variabili.Pannelli[caffetteria].Add(new Pannello_intestato());
            Variabili.Pannelli[caffetteria][0].p.Size = new Size((dim.Width - rientranza) / 7 * 4, dim.Height / 6 * 4);
            Variabili.Pannelli[caffetteria][0].p.Location = new Point(0, dim.Height / 7 * 0);
            Variabili.Pannelli[caffetteria][0].p.Parent = Variabili.Tabs.TabPages[3];
            Variabili.Pannelli[caffetteria][0].p.AutoScroll = true;
            Variabili.Pannelli[caffetteria][0].t.Text = "Caffetteria";
            Variabili.Pannelli[caffetteria][0].ShowText = true;

            Variabili.Pannelli[12].Add(new Pannello_intestato());
            Variabili.Pannelli[12].Add(new Pannello_intestato());
            Variabili.Pannelli[12][1].p.Size = new Size((dim.Width - rientranza) / 7, dim.Height / 6 * 4);
            Variabili.Pannelli[12][1].p.Location = new Point((dim.Width - rientranza) / 7 * 4, dim.Height / 6 * 0);
            Variabili.Pannelli[12][1].p.Parent = Variabili.Tabs.TabPages[3];
            Variabili.Pannelli[12][1].p.AutoScroll = true;
            Variabili.Pannelli[12][1].t.Text = "Bibite";
            Variabili.Pannelli[12][1].ShowText = true;

            Variabili.Pannelli[15].Add(new Pannello_intestato());
            Variabili.Pannelli[15][2].p.Size = new Size((dim.Width - rientranza) / 7 * 2, dim.Height / 6 * 4);
            Variabili.Pannelli[15][2].p.Location = new Point((dim.Width - rientranza) / 7 * 5, dim.Height / 6 * 0);
            Variabili.Pannelli[15][2].p.Parent = Variabili.Tabs.TabPages[3];
            Variabili.Pannelli[15][2].p.AutoScroll = true;
            Variabili.Pannelli[15][2].t.Text = "Alcolici";
            Variabili.Pannelli[15][2].ShowText = true;

            const int dolciumi = 13;
            Variabili.Pannelli[dolciumi].Add(new Pannello_intestato());
            Variabili.Pannelli[dolciumi][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[dolciumi][0].p.Location = new Point(0, dim.Height / 6 * 4);
            Variabili.Pannelli[dolciumi][0].p.Parent = Variabili.Tabs.TabPages[3];
            Variabili.Pannelli[dolciumi][0].p.AutoScroll = true;
            Variabili.Pannelli[dolciumi][0].t.Text = "Dolciumi";
            Variabili.Pannelli[dolciumi][0].ShowText = true;

            const int salati = 14;
            Variabili.Pannelli[salati].Add(new Pannello_intestato());
            Variabili.Pannelli[salati][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 6);
            Variabili.Pannelli[salati][0].p.Location = new Point(0, dim.Height / 6 * 5);
            Variabili.Pannelli[salati][0].p.Parent = Variabili.Tabs.TabPages[3];
            Variabili.Pannelli[salati][0].p.AutoScroll = true;
            Variabili.Pannelli[salati][0].t.Text = "Snack";
            Variabili.Pannelli[salati][0].ShowText = true;

            //GELATI
            const int gelati = 16;
            Variabili.Pannelli[gelati].Add(new Pannello_intestato());
            Variabili.Pannelli[gelati][0].p.Size = new Size((dim.Width - rientranza), dim.Height / 3);
            Variabili.Pannelli[gelati][0].p.Location = new Point(0, 0);
            Variabili.Pannelli[gelati][0].p.Parent = Variabili.Tabs.TabPages[4];
            Variabili.Pannelli[gelati][0].p.AutoScroll = true;
            Variabili.Pannelli[gelati][0].t.Text = "Gelati";
            Variabili.Pannelli[gelati][0].ShowText = true;

            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18][2].p.Size = new Size((dim.Width - rientranza), dim.Height / 3);
            Variabili.Pannelli[18][2].p.Location = new Point(0, dim.Height / 3 * 1);
            Variabili.Pannelli[18][2].p.Parent = Variabili.Tabs.TabPages[4];
            Variabili.Pannelli[18][2].p.AutoScroll = true;
            Variabili.Pannelli[18][2].t.Text = "Granite";
            Variabili.Pannelli[18][2].ShowText = true;

            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18][5].p.Size = new Size((dim.Width - rientranza), dim.Height / 3);
            Variabili.Pannelli[18][5].p.Location = new Point(0, dim.Height / 3 * 2);
            Variabili.Pannelli[18][5].p.Parent = Variabili.Tabs.TabPages[4];
            Variabili.Pannelli[18][5].p.AutoScroll = true;
            Variabili.Pannelli[18][5].t.Text = "Sorbetto";
            Variabili.Pannelli[18][5].ShowText = true;

            //ALTRO
            const int altezza_coperto = 130;
            Variabili.Pannelli[3].Add(new Pannello_intestato());
            Variabili.Pannelli[3][0].p.Size = new Size((dim.Width - rientranza), altezza_coperto / 5 * 4);
            Variabili.Pannelli[3][0].p.Location = new Point(0, 0);
            Variabili.Pannelli[3][0].p.Parent = Variabili.Tabs.TabPages[5];
            Variabili.Pannelli[3][0].p.AutoScroll = true;
            Variabili.Pannelli[3][0].t.Text = "Coperto";
            Variabili.Pannelli[3][0].ShowText = true;

            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18][3].p.Size = new Size((dim.Width - rientranza) / 2, (dim.Height - altezza_coperto) / 2);
            Variabili.Pannelli[18][3].p.Location = new Point((dim.Width - rientranza) / 2 * 0, altezza_coperto);
            Variabili.Pannelli[18][3].p.Parent = Variabili.Tabs.TabPages[5];
            Variabili.Pannelli[18][3].p.AutoScroll = true;
            Variabili.Pannelli[18][3].t.Text = "Piadine";
            Variabili.Pannelli[18][3].ShowText = true;

            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18][0].p.Size = new Size((dim.Width - rientranza) / 2, (dim.Height - altezza_coperto) / 2);
            Variabili.Pannelli[18][0].p.Location = new Point((dim.Width - rientranza) / 2 * 1, altezza_coperto);
            Variabili.Pannelli[18][0].p.Parent = Variabili.Tabs.TabPages[5];
            Variabili.Pannelli[18][0].p.AutoScroll = true;
            Variabili.Pannelli[18][0].t.Text = "Crepes";
            Variabili.Pannelli[18][0].ShowText = true;

            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18][1].p.Size = new Size((dim.Width - rientranza) / 2, (dim.Height - altezza_coperto) / 2);
            Variabili.Pannelli[18][1].p.Location = new Point((dim.Width - rientranza) / 2 * 0, (dim.Height - altezza_coperto) / 2 + altezza_coperto);
            Variabili.Pannelli[18][1].p.Parent = Variabili.Tabs.TabPages[5];
            Variabili.Pannelli[18][1].p.AutoScroll = false;
            Variabili.Pannelli[18][1].t.Text = "Altro";
            Variabili.Pannelli[18][1].ShowText = true;

            Variabili.Pannelli[18].Add(new Pannello_intestato());
            Variabili.Pannelli[18][4].p.Size = new Size((dim.Width - rientranza) / 2, (dim.Height - altezza_coperto) / 2);
            Variabili.Pannelli[18][4].p.Location = new Point((dim.Width - rientranza) / 2 * 1, (dim.Height - altezza_coperto) / 2 + altezza_coperto);
            Variabili.Pannelli[18][4].p.Parent = Variabili.Tabs.TabPages[5];
            Variabili.Pannelli[18][4].p.AutoScroll = true;
            Variabili.Pannelli[18][4].t.Text = "Gonfiabili";
            Variabili.Pannelli[18][4].ShowText = true;

            //crea i testi al posto giusto ridemensionando i pannelli
            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                for (int i2 = 0; i2 < Variabili.Pannelli[i].Count; i2++)
                {
                    bool x;
                    try
                    {
                        x = Variabili.Pannelli[i][i2].ShowText.Value;
                    }
                    catch
                    {
                        continue;
                    }

                    if (x)
                    {
                        Variabili.Pannelli[i][i2].p.Size = new Size(Variabili.Pannelli[i][i2].p.Size.Width, Variabili.Pannelli[i][i2].p.Size.Height - Form1.spazio_intestazione);
                        Variabili.Pannelli[i][i2].p.Location = new Point(Variabili.Pannelli[i][i2].p.Location.X, Variabili.Pannelli[i][i2].p.Location.Y + Form1.spazio_intestazione);

                        Variabili.Pannelli[i][i2].t.Location = new Point(Variabili.Pannelli[i][i2].p.Location.X, Variabili.Pannelli[i][i2].p.Location.Y - Form1.spazio_intestazione);
                        Variabili.Pannelli[i][i2].t.Size = new Size(Variabili.Pannelli[i][i2].p.Size.Width, Form1.spazio_intestazione);
                        Variabili.Pannelli[i][i2].t.Parent = Variabili.Pannelli[i][i2].p.Parent;
                        Variabili.Pannelli[i][i2].t.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Bold);
                    }
                    else
                    {
                        Variabili.Pannelli[i][i2].p.Size = new Size(Variabili.Pannelli[i][i2].p.Size.Width, Variabili.Pannelli[i][i2].p.Size.Height);
                        Variabili.Pannelli[i][i2].p.Location = new Point(Variabili.Pannelli[i][i2].p.Location.X, Variabili.Pannelli[i][i2].p.Location.Y);
                    }
                }
            }

            return;
        }

        internal static string StampaPercentuale(BigDecimal v)
        {
            char terminatore = '%';
            string s = Prodotto.Stampa_formato_euro(v, terminatore);
            for (int i = s.Length - 1; i >= 0; i--)
            {
                if (s[i] == '0')
                {
                    s = s.Substring(0, i) + terminatore;
                }
                else if (s[i] == ',' || s[i] == '.')
                {
                    break;
                }
            }

            if (s[s.Length - 2] == ',' || s[s.Length - 2] == '.')
            {
                s = s.Substring(0, s.Length - 2) + terminatore;
            }
            return s;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        public static void Prepara_pannelli(Size dim)
        {
            const int rientranza = 5;
            Variabili.Pannelli.Clear();
            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                Pannello_intestato x = new Pannello_intestato
                {
                    p = new Panel(),
                    t = new Label()
                };
                x.p.AutoScroll = true;
                x.t.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Bold);
                Variabili.Pannelli.Add(new List<Pannello_intestato>());
                Variabili.Pannelli[i].Add(x);
            }
            //Console.WriteLine(dim);

            for (int i = 0; i < 3; i++)
            {
                Variabili.Pannelli[i][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height);
                Variabili.Pannelli[i][0].p.Location = new Point(dim.Width / 3 * (3 - i - 1), 0);
                Variabili.Pannelli[i][0].p.Parent = Variabili.Tabs.TabPages[0];
            }

            Variabili.Pannelli[0][0].t.Text = "Pizze";
            Variabili.Pannelli[1][0].t.Text = "Bevande";
            Variabili.Pannelli[2][0].t.Text = "Altro";

            const int spazio_coperto = 50;
            for (int i = 3; i < 11; i++)
                Variabili.Pannelli[i][0].p.Parent = Variabili.Tabs.TabPages[1];
            Variabili.Pannelli[18][0].p.Parent = Variabili.Tabs.TabPages[1];

            Variabili.Pannelli[6][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 40 * 10);
            Variabili.Pannelli[6][0].p.Location = new Point(dim.Width / 3, 0);
            Variabili.Pannelli[7][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 40 * 7);
            Variabili.Pannelli[7][0].p.Location = new Point(dim.Width / 3, dim.Height / 40 * 10);
            Variabili.Pannelli[8][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 40 * 5);
            Variabili.Pannelli[8][0].p.Location = new Point(dim.Width / 3, dim.Height / 40 * 17);
            Variabili.Pannelli[9][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 40 * 10);
            Variabili.Pannelli[9][0].p.Location = new Point(dim.Width / 3, dim.Height / 40 * 23);
            Variabili.Pannelli[3][0].p.Size = new Size(dim.Width / 3 - rientranza, spazio_coperto);
            Variabili.Pannelli[3][0].p.Location = new Point(dim.Width / 3, dim.Height / 40 * 33);

            Variabili.Pannelli[10][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height);
            Variabili.Pannelli[10][0].p.Location = new Point(0, 0);

            Variabili.Pannelli[4][0].p.Size = new Size(dim.Width / 3 - rientranza, (dim.Height) / 9 * 2);
            Variabili.Pannelli[4][0].p.Location = new Point(dim.Width / 3 * 2, 0);
            Variabili.Pannelli[5][0].p.Size = new Size(dim.Width / 3 - rientranza, (dim.Height) / 9 * 2);
            Variabili.Pannelli[5][0].p.Location = new Point(dim.Width / 3 * 2, (dim.Height) / 9 * 2);
            Variabili.Pannelli[18][0].p.Size = new Size(dim.Width / 3 - rientranza, (dim.Height) / 9 * 5);
            Variabili.Pannelli[18][0].p.Location = new Point(dim.Width / 3 * 2, (dim.Height) / 9 * 4);

            Variabili.Pannelli[3][0].t.Text = "Coperto";
            Variabili.Pannelli[4][0].t.Text = "Primi piatti";
            Variabili.Pannelli[5][0].t.Text = "Secondi piatti";
            Variabili.Pannelli[6][0].t.Text = "Secondi freddi";
            Variabili.Pannelli[7][0].t.Text = "Contorno";
            Variabili.Pannelli[8][0].t.Text = "Dolce";
            Variabili.Pannelli[9][0].t.Text = "Panini";
            Variabili.Pannelli[10][0].t.Text = "Bevande";
            Variabili.Pannelli[18][0].t.Text = "Altro";

            for (int i = 11; i < 18; i++)
                Variabili.Pannelli[i][0].p.Parent = Variabili.Tabs.TabPages[2];

            Variabili.Pannelli[11][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 2);
            Variabili.Pannelli[11][0].p.Location = new Point(dim.Width / 3 * 2, 0);
            Variabili.Pannelli[12][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 2);
            Variabili.Pannelli[12][0].p.Location = new Point(dim.Width / 3 * 2, dim.Height / 2);
            Variabili.Pannelli[14][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 40 * 9);
            Variabili.Pannelli[14][0].p.Location = new Point(dim.Width / 3, 0);
            Variabili.Pannelli[15][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 40 * 31);
            Variabili.Pannelli[15][0].p.Location = new Point(dim.Width / 3, dim.Height / 40 * 9);
            Variabili.Pannelli[13][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 120 * 31);
            Variabili.Pannelli[13][0].p.Location = new Point(0, 0);
            Variabili.Pannelli[16][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 120 * 45);
            Variabili.Pannelli[16][0].p.Location = new Point(0, dim.Height / 120 * 31);
            Variabili.Pannelli[17][0].p.Size = new Size(dim.Width / 3 - rientranza, dim.Height / 120 * 52);
            Variabili.Pannelli[17][0].p.Location = new Point(0, dim.Height / 120 * 76);

            Variabili.Pannelli[11][0].t.Text = "Caffetteria";
            Variabili.Pannelli[12][0].t.Text = "Bibite";
            Variabili.Pannelli[13][0].t.Text = "Dolciumi";
            Variabili.Pannelli[14][0].t.Text = "Snack e salati";
            Variabili.Pannelli[15][0].t.Text = "Alcolici";
            Variabili.Pannelli[16][0].t.Text = "Gelati";
            Variabili.Pannelli[17][0].t.Text = "Altro";

            //crea i testi al posto giusto ridemensionando i pannelli
            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                Variabili.Pannelli[i][0].p.Size = new Size(Variabili.Pannelli[i][0].p.Size.Width, Variabili.Pannelli[i][0].p.Size.Height - Form1.spazio_intestazione);
                Variabili.Pannelli[i][0].t.Location = Variabili.Pannelli[i][0].p.Location;
                Variabili.Pannelli[i][0].p.Location = new Point(Variabili.Pannelli[i][0].p.Location.X, Variabili.Pannelli[i][0].p.Location.Y + Form1.spazio_intestazione);
                Variabili.Pannelli[i][0].t.Size = new Size(Variabili.Pannelli[i][0].p.Size.Width, Form1.spazio_intestazione);
                Variabili.Pannelli[i][0].t.Parent = Variabili.Pannelli[i][0].p.Parent;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.IndexOf(System.String)")]
        internal static BigDecimal BigDecimal_Da_Prezzo_Vecchio(string sl)
        {
            int a1 = sl.IndexOf("<prezzo>");
            int a2 = sl.IndexOf("</prezzo>");
            a1 += 8; //lunghezza di "<prezzo>"
            string s2 = sl.Substring(a1, a2 - a1);
            BigDecimal bd = s2;
            return bd;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "quantita")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "incasso2")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "label")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "label_quantita")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "incasso")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "nome")]
        public static void Refresh_pannelli_prodotti(ref List<List<List<Prodotto>>> Prodotti, Label totale_label)
        {
            const int spazio_verticale = 19;
            const int spazio_quantita = 60;
            const int spazio_incasso = 77;
            BigDecimal totale_totale = 0;
            for (int p = 0; p < Prodotti.Count; p++)
            {
                for (int pi2 = 0; pi2 < Prodotti[p].Count; pi2++)
                {
                    int spazio_horizontal1 = 0;
                    int a1 = Prodotti[p].Count * 20;
                    Pannello_intestato p2;
                    try
                    {
                        p2 = Variabili.Pannelli[p][pi2];
                    }
                    catch
                    {
                        continue;
                    }

                    //int x8 = 1;
                    Panel p3 = p2.p;
                    int a2 = p3.Size.Height;
                    if (a1 > a2)
                        spazio_horizontal1 = 17;

                    BigDecimal totale_cibi = 0;
                    int i = 0;
                    BigInteger quantita_totale = 0;
                    for (; i < Prodotti[p][pi2].Count; i++)
                    {
                        string pn = Prodotti[p][pi2][i].nome;
                        if (Prodotti[p][pi2][i].old)
                        {
                            pn = "[OLD] " + pn;
                        }

#pragma warning disable IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        Label nome = new Label
#pragma warning restore IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        {
                            Visible = true,
                            Location = new Point(0, spazio_verticale * i),
                            Text = pn,
                            Parent = Variabili.Pannelli[p][0].p,
                            Size = new Size(Variabili.Pannelli[p][0].p.Size.Width - spazio_quantita - spazio_incasso, spazio_verticale),
                            BorderStyle = BorderStyle.FixedSingle
                        };

#pragma warning disable IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        Label quantita = new Label
#pragma warning restore IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        {
                            Visible = true,
                            Location = new Point(Variabili.Pannelli[p][0].p.Size.Width - spazio_incasso - spazio_quantita, spazio_verticale * i),
                            Text = "x" + Prodotti[p][pi2][i].quantita,
                            Parent = Variabili.Pannelli[p][0].p,
                            Size = new Size(spazio_quantita, spazio_verticale),
                            BorderStyle = BorderStyle.FixedSingle
                        };

#pragma warning disable IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        Label incasso2 = new Label
#pragma warning restore IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        {
                            Visible = true,
                            Location = new Point(Variabili.Pannelli[p][0].p.Size.Width - spazio_incasso, spazio_verticale * i),
                            Text = Prodotto.Stampa_formato_euro(Prodotti[p][pi2][i].quantita * Prodotti[p][pi2][i].prezzo),
                            Parent = Variabili.Pannelli[p][0].p,
                            Size = new Size(spazio_incasso - spazio_horizontal1 - 2, spazio_verticale),
                            BorderStyle = BorderStyle.FixedSingle
                        };

                        totale_cibi += Prodotti[p][pi2][i].quantita * Prodotti[p][pi2][i].prezzo;
                        quantita_totale += Prodotti[p][pi2][i].quantita;
                    }
                    if (Prodotti[p][pi2].Count > 1)
                    {
#pragma warning disable IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        Label label = new Label
#pragma warning restore IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        {
                            Visible = true,
                            Location = new Point(0, spazio_verticale * i),
                            Text = "■ TOTALE",
                            Parent = Variabili.Pannelli[p][0].p,
                            Size = new Size(Variabili.Pannelli[p][0].p.Size.Width - (2 * spazio_incasso), spazio_verticale),
                            BorderStyle = BorderStyle.FixedSingle
                        };

#pragma warning disable IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        Label label_quantita = new Label
#pragma warning restore IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        {
                            Visible = true,
                            Location = new Point(Variabili.Pannelli[p][0].p.Size.Width - (2 * spazio_incasso), spazio_verticale * i),
                            Text = quantita_totale.ToString(),
                            Parent = Variabili.Pannelli[p][0].p,
                            Size = new Size(spazio_incasso - spazio_horizontal1 - 2, spazio_verticale),
                            BorderStyle = BorderStyle.FixedSingle
                        };

#pragma warning disable IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        Label incasso = new Label
#pragma warning restore IDE0059 // Il valore assegnato al simbolo non viene mai usato
                        {
                            Visible = true,
                            Location = new Point(Variabili.Pannelli[p][0].p.Size.Width - spazio_incasso, spazio_verticale * i),
                            Text = Prodotto.Stampa_formato_euro(totale_cibi),
                            Parent = Variabili.Pannelli[p][0].p,
                            Size = new Size(spazio_incasso - spazio_horizontal1 - 2, spazio_verticale),
                            BorderStyle = BorderStyle.FixedSingle
                        };
                    }
                    totale_totale += totale_cibi;
                }
            }

            totale_label.Text = Prodotto.Stampa_formato_euro(totale_totale);
            //Console.WriteLine(totale_label.Text);
            //Console.WriteLine("Totale totale: " + Form1.stampa_formato_euro(totale_totale));
        }

        public static DateTime Versione_Attuale()
        {
            string p = Funzioni.Path_Del_Software();
            return System.IO.File.GetLastWriteTime(p + "/ScontriniOratorio.exe");
        }

        public static string Path_Del_Software()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        public static string DataItaliana(DateTime? d)
        {
            if (d == null || d.HasValue == false)
                return "";

            return d.Value.Day.ToString().PadLeft(2, '0') + "/" + d.Value.Month.ToString().PadLeft(2, '0') + "/" + d.Value.Year.ToString();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        internal static void TryDeleteDirectory(string v1, bool v2)
        {
            try
            {
                Directory.Delete(v1, v2);
            }
            catch
            {
                ;
            }
        }

        internal static void ExtractToDirectory(string in_path, string out_folder)
        {
            UnZip(in_path, out_folder);
        }

        public static void UnZip(string zipFile, string folderPath)
        {
            if (!File.Exists(zipFile))
                throw new FileNotFoundException();

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            Shell32.Shell objShell = new Shell32.Shell();
            Shell32.Folder destinationFolder = objShell.NameSpace(folderPath);
            Shell32.Folder sourceFile = objShell.NameSpace(zipFile);

            foreach (var file in sourceFile.Items())
            {
                destinationFolder.CopyHere(file, 4 | 16);
            }
        }

        internal static int Arrotonda(float h2)
        {
            int n = (int)h2;
            float d = h2 - n;
            if (d >= 0.5)
                return n + 1;
            return n;
        }

        internal static int ArrotondaEccesso(float colonne)
        {
            int n = (int)colonne;
            float d = colonne - n;
            if (d > 0)
                return n + 1;
            return n;
        }

        internal static BigDecimal PrezzoConSconto(BigDecimal totale, BigDecimal sconto)
        {
            BigDecimal d = totale * sconto;
            BigDecimal r = totale - d;
            BigDecimal r2 = Funzioni.ArrotondaPerDifettoAlleDecineDiCentesimo(r);
            return r2;
        }

        private static BigDecimal ArrotondaPerDifettoAlleDecineDiCentesimo(BigDecimal r)
        {
            BigDecimal r2 = r * 10;
            int r3 = (int)r2;
            BigDecimal r4 = r3;
            r4 /= 10;
            return r4;
        }

        internal static void SvuotaCarrello()
        {
            AbstractMode.Carrello.Clear();
            AbstractMode.me_form.Refresh_pannello_carrello();
        }

        internal static void CopiaCartella(string SourcePath, string DestinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
        }
    }
}