﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ScontriniOratorio
{
    [Serializable]
    public class Tabellone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "L")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<Ordinazione> L;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class Ordinazione
        {
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public List<string> Nomi;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public List<int> Quantita;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public string cognome;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public int numero_pizza;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public Color colore = Color.Red;

            public Ordinazione()
            {
                Nomi = new List<string>();
                Quantita = new List<int>();
                colore = Color.Red;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
            public override string ToString()
            {
                string c = this.cognome;
                if (c.Length < 1)
                {
                    c = "\"SENZA NOME\"";
                }
                return ("#" + this.numero_pizza.ToString().PadLeft(4, '0') + " - " + c);
            }
        }

        public Tabellone()
        {
            L = new List<Ordinazione>();
        }

        internal static void Aggiungi(Tabellone da_inviare)
        {
            if (Form1.tabellone == null)
            {
                Form1.tabellone = new Tabellone();
            }

            foreach (Ordinazione o in da_inviare.L)
            {
                o.colore = Color.Red;
                Form1.tabellone.L.Add(o);
            }

            Ridisegna_tabellone();
        }

        private static void Ridisegna_tabellone()
        {
            if (Form1.tabellone_form == null)
            {
                ;
            }
            else
            {
                Form1.tabellone_form.Ridisegna_tabellone();
            }
        }
    }
}
