﻿using System;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ScontriniOratori
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "stampante")]
    [Serializable]
    public class Impostazione_stampante
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public bool is_valid;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        public PrinterSettings s;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "NIENTE")]
        public const char NIENTE = 'N';

        internal void Salva(string sp)
        {
            byte[] b;
            b = ToByteArray(this);
            File.WriteAllBytes(sp, b);
        }



        public static byte[] ToByteArray(object source)
        {
            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, source);
                return stream.ToArray();
            }
        }

        internal bool? Carica(string sp)
        {
            var x2 = FromByteArray(sp);
            s = x2.s;
            is_valid = x2.is_valid;

            if (s == null)
                return false;

            return true;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        public static Impostazione_stampante FromByteArray(string s)
        {
            var formatter = new BinaryFormatter();
            FileStream fs;
            try
            {
                fs = new FileStream(s, FileMode.Open);
            }
            catch
            {
                return null;
            }

            Impostazione_stampante x2 = null;
            try
            {
                x2 = (Impostazione_stampante)formatter.Deserialize(fs);
            }
            catch
            {
                ;
            }
            fs.Close();

            return x2;
        }
    }
}