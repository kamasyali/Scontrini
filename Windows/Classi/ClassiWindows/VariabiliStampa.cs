﻿namespace ScontriniOratorio.Forms
{
    public class VariabiliStampa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ultimo")]
        public bool Ultimo { get; internal set; }
    }
}