﻿using Common;
using Newtonsoft.Json.Linq;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using ScontriniOratorio.Classi.ClassiComune.Stat;
using ScontriniOratorio.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ScontriniOratori
{
    public class Statistiche
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "C")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<Lista_Categorie> C; //catalogo delle cose

        public Statistiche()
        {
            C = new List<Lista_Categorie>();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.FileDialog.set_Filter(System.String)")]
        public void Salva()
        {
            for (int i = 0; i < C.Count; i++)
            {
                for (int i2 = 0; i2 < C[i].L.Count; i2++)
                {
                    for (int j = 0; j < C[i].L[i2].L.Count; j++)
                    {
                        C[i].L[i2].L[j].p.prezzo.Mantissa_BigToString();
                    }
                }
            }

            SaveFileDialog f = new SaveFileDialog
            {
                DefaultExt = "jsonscontr",
                Filter = "Statistiche scontrini oratorio (*.jsonscontr)|*.jsonscontr|All files (*.*)|*.*"
            };
            if (f.ShowDialog() == DialogResult.OK)
            {
                var path = f.FileName;

                string extension = Path.GetExtension(path);
                if (extension == ".xmlscontr")
                {
                    System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Statistiche));
                    System.IO.FileStream file = System.IO.File.Create(path);
                    writer.Serialize(file, this);
                    file.Close();
                }
                else
                {
                    var s = Newtonsoft.Json.JsonConvert.SerializeObject(this);
                    File.WriteAllText(path, s);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.FileDialog.set_Filter(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        public void Carica(string s = "")
        {
            bool ok = false;
            string path = "";
            if (String.IsNullOrEmpty(s))
            {
                OpenFileDialog f = new OpenFileDialog
                {
                    DefaultExt = "xmlscontr",
                    Filter = "Statistiche scontrini oratorio (*.xmlscontr)|*.xmlscontr|Statistiche scontrini oratorio (*.jsonscontr)|*.jsonscontr|All files (*.*)|*.*"
                };
                if (f.ShowDialog() == DialogResult.OK)
                {
                    ok = true;
                    path = f.FileName;
                }
            }
            else
            {
                ok = true;
                path = s;
            }

            if (ok)
            {
                FixMaiuscole(path);
                if (Variabili.MainMode == 1)
                    ElaboraStat(ref Form1.Prodotti);
                else
                    ElaboraStat(ref Form2.Prodotti);

                string extension = Path.GetExtension(path);

                Statistiche x = null;
                if (extension == ".xmlscontr")
                {
                    x = Statistiche.LeggiXML(path);
                }
                else if (extension == ".jsonscontr")
                {
                    string s7 = File.ReadAllText(path);
                    var jsonData = JObject.Parse(s7);
                    x = this.ConvertiDaJSON(jsonData);
                }

                this.C = x.C;

                for (int i = 0; i < C.Count; i++)
                {
                    for (int i2 = 0; i2 < C[i].L.Count; i2++)
                    {
                        for (int j = 0; j < C[i].L[i2].L.Count; j++)
                        {
                            C[i].L[i2].L[j].p.prezzo.Mantissa_StringToBig();
                            C[i].L[i2].L[j].p.prezzo.Normalize();
                        }
                    }
                }
            }
        }

        private void ElaboraStat(ref List<Categoria_Prodotto> L_Prodotti)
        {
            for (int i = 0; i < C.Count; i++)
            {
                for (int i2 = 0; i2 < L_Prodotti[i].Count(); i2++)
                {
                    for (int j = 0; j < C[i].L[i2].L.Count; j++)
                    {
                        if (String.IsNullOrEmpty(C[i].L[i2].L[j].p.prezzo.Mantissa_String))
                        {
                            bool trovato = false;
                            for (int k = 0; k < L_Prodotti[i].Count(); k++)
                            {
                                if (C[i].L[i2].L[j].p.nome == L_Prodotti[i].get(i2).Get(k).nome)
                                {
                                    trovato = true;
                                    C[i].L[i2].L[j].p.prezzo = L_Prodotti[i].get(i2).Get(k).prezzo;
                                }
                            }
                            if (trovato)
                            {
                                ; //bene
                            }
                            else
                            {
                                MessageBox.Show("Qualcosa è andato storto nella conversione. Prodotto: " + C[i].L[i2].L[j].p.nome.ToString());
                            }
                        }
                    }
                }
            }
        }

        private static Statistiche LeggiXML(string path)
        {
            string[] r = File.ReadAllLines(path);
            if (Statistiche.Contiene(r, "<Lista_Categorie>"))
            {
                return XML_Deserialize(path);
            }

            return LeggiXML_v1(r);
        }

        private static Statistiche LeggiXML_v1(string[] r)
        {
            Statistiche x = new Statistiche
            {
                C = new List<Lista_Categorie>()
            };

            int i = 0;
            while (i < r.Length)
            {
                int a1 = TrovaPrimo(r, i, r.Length - 1, new List<string> { "<Categoria>" });
                int a2 = -1;
                if (a1 >= 0)
                    a2 = TrovaPrimo(r, a1, r.Length - 1, new List<string> { "</Categoria>" });

                if (a1 >= 0 && a2 >= 0)
                {
                    Lista_Categorie c1 = new Lista_Categorie
                    {
                        L = new List<Categoria_Stat>()
                    };
                    Categoria_Stat c2 = LeggiXML_Categoria(r, a1, a2);

                    c1.L.Add(c2);
                    x.C.Add(c1);

                    i = a2 + 1;
                }
                else
                    break;
            }

            return x;
        }

        private static Categoria_Stat LeggiXML_Categoria(string[] r, int a1, int a2)
        {
            List<String> s = new List<string>
            {
                "<nome>",
                "</nome>"
            };
            int n = TrovaUltimo(r, a1, a2, s);

            Categoria_Stat c = new Categoria_Stat
            {
                nome = EstraiNomeXML(r[n], "nome"),
                L = LeggiXML_ListaProdottoStat(r, a1, n)
            };

            return c;
        }

        private static List<Prodotto_stat> LeggiXML_ListaProdottoStat(string[] r, int a1, int a2)
        {
            List<Prodotto_stat> L = new List<Prodotto_stat>();

            int k = a1;
            while (k <= a2)
            {
                int b1 = TrovaPrimo(r, k, a2, new List<string> { "<Prodotto_stat>" });
                int b2 = -1;
                if (b1 >= 0)
                    b2 = TrovaPrimo(r, b1, a2, new List<string> { "</Prodotto_stat>" });

                if (b1 >= 0 && b2 >= 0)
                {
                    Prodotto_stat p = LeggiXML_ProdottoStat(r, b1, b2);
                    L.Add(p);

                    k = b2 + 1;
                }
                else
                    break;
            }

            return L;
        }

        private static Prodotto_stat LeggiXML_ProdottoStat(string[] r, int a1, int a2)
        {
            Prodotto_stat p = new Prodotto_stat();

            List<string> s = new List<string>
            {
                "<momento>",
                "</momento>"
            };
            int m = TrovaUltimo(r, a1, a2, s);

            string m2 = EstraiNomeXML(r[m], "momento");
            p.momento = StringToDateTime(m2);

            int b1 = TrovaPrimo(r, a1, a2, new List<string> { "<p>" });

            List<string> s2 = new List<string>
            {
                "</p>"
            };
            int b2 = TrovaUltimo(r, a1, m, s2);

            p.p = LeggiXML_ProdottoStatProdotto(r, b1, b2);

            return p;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private static Prodotto LeggiXML_ProdottoStatProdotto(string[] r, int b1, int b2)
        {
            Prodotto p = new Prodotto();

            int prezzo1 = TrovaPrimo(r, b1, b2, new List<string> { "<prezzo>" });
            int prezzo2 = TrovaPrimo(r, prezzo1, b2, new List<string> { "</prezzo>" });

            p.prezzo = LeggiXML_Prezzo(r, prezzo1 + 1, prezzo2 - 1);
            p.nome = LeggiXML_Proprieta(r, b1, b2, "nome");
            p.tipo = Convert.ToInt32(LeggiXML_Proprieta(r, b1, b2, "tipo"));
            p.quantita = Convert.ToInt32(LeggiXML_Proprieta(r, b1, b2, "quantita"));

            int c1 = TrovaPrimo(r, b1, b2, new List<string> { "<con_senza>" });
            int c2 = TrovaPrimo(r, b1, b2, new List<string> { "</con_senza>" });
            p.con_senza = LeggiXML_ListaInt(r, c1 + 1, c2 - 1);

            int e1 = TrovaPrimo(r, b1, b2, new List<string> { "<nome_extra>" });
            int e2 = TrovaPrimo(r, b1, b2, new List<string> { "</nome_extra>" });
            p.nome_extra = LeggiXML_ListaString(r, e1 + 1, e2 - 1);

            int d1 = TrovaPrimo(r, b1, b2, new List<string> { "<componente_extra>" });
            int d2 = TrovaPrimo(r, b1, b2, new List<string> { "</componente_extra>" });
            p.componente_extra = LeggiXML_ListaInt(r, d1 + 1, d2 - 1);

            return p;
        }

        private static List<string> LeggiXML_ListaString(string[] r, int v1, int v2)
        {
            List<string> L = new List<string>();

            for (int i = v1; i <= v2; i++)
            {
                L.Add(LeggiXML_Proprieta(r, i, i, "string"));
            }

            return L;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private static List<int> LeggiXML_ListaInt(string[] r, int v1, int v2)
        {
            List<int> L = new List<int>();

            for (int i = v1; i <= v2; i++)
            {
                L.Add(Convert.ToInt32(LeggiXML_Proprieta(r, i, i, "int")));
            }

            return L;
        }

        private static string LeggiXML_Proprieta(string[] r, int b1, int b2, string v)
        {
            List<string> s = new List<string>
            {
                "<" + v + ">",
                "</" + v + ">"
            };

            int n = TrovaPrimo(r, b1, b2, s);
            if (n >= 0)
                return EstraiNomeXML(r[n], v);
            return null;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt64(System.String)")]
        private static BigDecimal LeggiXML_Prezzo(string[] r, int v1, int v2)
        {
            BigInteger mantissa = Convert.ToInt64(LeggiXML_Proprieta(r, v1, v2, "Mantissa_String"));
            int exponent = Convert.ToInt32(LeggiXML_Proprieta(r, v1, v2, "Exponent"));

            BigDecimal d = new BigDecimal(mantissa, exponent);
            return d;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private static DateTime StringToDateTime(string m)
        {
            //esempio
            //        2018-06-10
            //        19:12:30.6323954
            //        02:00

            string[] s = m.Split('+');
            string[] s2 = s[0].Split('T');

            string data = s2[0];
            string orario = s2[1];

            string[] data2 = data.Split('-');
            string[] orario2 = orario.Split(':');

            int year = Convert.ToInt32(data2[0]);
            int month = Convert.ToInt32(data2[1]);
            int day = Convert.ToInt32(data2[2]);

            int ora = Convert.ToInt32(orario2[0]);
            int minuto = Convert.ToInt32(orario2[1]);

            string[] secondi = orario2[2].Split('.');
            int secondi_i = Convert.ToInt32(secondi[0]);
            int millisecondi_i = Convert.ToInt32(secondi[1].Substring(0, 3));

            DateTime d = new DateTime(year, month, day, ora, minuto, secondi_i, millisecondi_i);

            return d;
        }

        private static string EstraiNomeXML(string v, string extra)
        {
            int n = extra.Length + 2;

            for (int i = 0; i < v.Length; i++)
            {
                char c = v[i];
                if (c == ' ' || c == '\t' || c == '\n')
                {
                    v = v.Substring(1);
                    i--;
                }
            }

            v = v.Substring(n);
            v = v.Substring(0, v.Length - (n + 1));
            return v;
        }

        private static int TrovaUltimo(string[] r, int a1, int a2, List<string> s)
        {
            for (int k = a2; k >= a1; k--)
            {
                string s2 = r[k];
                bool tutti = true;
                foreach (var s3 in s)
                {
                    if (!s2.Contains(s3))
                    {
                        tutti = false;
                        break;
                    }
                }

                if (tutti)
                    return k;
            }

            return -1;
        }

        private static int TrovaPrimo(string[] r, int i1, int i2, List<string> v)
        {
            for (int k = i1; k <= i2; k++)
            {
                string s = r[k];

                bool tutti = true;
                foreach (var s2 in v)
                {
                    if (!s.Contains(s2))
                        tutti = false;
                }

                if (tutti)
                    return k;
            }

            return -1;
        }

        private static bool Contiene(string[] r, string v)
        {
            foreach (var r2 in r)
            {
                if (r2.Contains(v))
                    return true;
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private static Statistiche XML_Deserialize(string path)
        {
            XmlSerializer reader = new XmlSerializer(typeof(Statistiche));
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            Statistiche x = (Statistiche)reader.Deserialize(file);
            file.Close();
            return x;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDateTime(System.Object)")]
        private Statistiche ConvertiDaJSON(JObject jsonData)
        {
            Statistiche x = new Statistiche
            {
                C = new List<Lista_Categorie>()
            };
            JProperty i1 = (JProperty)jsonData.First;

            JArray i2 = (JArray)i1.First;
            foreach (JObject i3 in i2)
            {
                Lista_Categorie c1 = new Lista_Categorie();
                JArray lista = (JArray)i3["L"];

                List<Categoria_Stat> c2 = new List<Categoria_Stat>();
                foreach (JObject i4 in lista)
                {
                    Categoria_Stat c3 = new Categoria_Stat();

                    List<Prodotto_stat> c4 = new List<Prodotto_stat>();
                    JArray lista2 = (JArray)i4["L"];
                    foreach (JObject i5 in lista2)
                    {
                        Prodotto_stat c5 = new Prodotto_stat();

                        JObject p = (JObject)(i5["p"]);
                        JValue momento = (JValue)i5["momento"];
                        DateTime d = Convert.ToDateTime(momento);

                        c5.momento = d;
                        c5.p = Statistiche.GetProdotto(p);

                        c4.Add(c5);
                    }
                    JToken nome2 = i4["nome"];

                    c3.nome = nome2.ToString();
                    c3.L = c4;
                    c2.Add(c3);
                }
                JToken nome = i3["nome"];

                c1.nome = nome.ToString();
                c1.L = c2;
                x.C.Add(c1);
            }

            return x;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.Object)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt64(System.String)")]
        private static Prodotto GetProdotto(JObject p)
        {
            Prodotto n = new Prodotto();

            JObject p2 = (JObject)p["prezzo"];
            JValue p3 = (JValue)p2["Mantissa_BigInt"];
            BigInteger p4 = Convert.ToInt64(p3.ToString());
            n.prezzo = new BigDecimal(p4, (int)(JValue)(p2["Exponent"]));

            n.nome = p["nome"].ToString();
            n.tipo = Convert.ToInt32((JValue)p["tipo"]);
            n.quantita = Convert.ToInt32((JValue)p["quantita"]);
            n.old = (bool)(JValue)(p["old"]);
            n.nomecategoria = p["nomecategoria"].ToString();

            JArray a1 = (JArray)p["con_senza"];
            n.con_senza = new List<int>();
            foreach (var t1 in a1) { n.con_senza.Add(Convert.ToInt32(t1)); }

            JArray a2 = (JArray)p["nome_extra"];
            n.nome_extra = new List<string>();
            foreach (var t1 in a2) { n.nome_extra.Add(t1.ToString()); }

            JArray a3 = (JArray)p["componente_extra"];
            n.componente_extra = new List<int>();
            foreach (var t1 in a3) { n.componente_extra.Add(Convert.ToInt32(t1)); }

            n.checkExtraEuro = (bool)(JValue)(p["checkExtraEuro"]);
            n.Sottotipo = Convert.ToInt32((JValue)p["Sottotipo"]);

            return n;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "L")]
        public void ElaboraStat(ref List<List<List<Prodotto>>> L_Prodotti)
        {
            for (int i = 0; i < C.Count; i++)
            {
                for (int i2 = 0; i2 < L_Prodotti[i].Count; i2++)
                {
                    for (int j = 0; j < C[i].L[i2].L.Count; j++)
                    {
                        if (String.IsNullOrEmpty(C[i].L[i2].L[j].p.prezzo.Mantissa_String))
                        {
                            bool trovato = false;
                            for (int k = 0; k < L_Prodotti[i].Count; k++)
                            {
                                if (C[i].L[i2].L[j].p.nome == L_Prodotti[i][i2][k].nome)
                                {
                                    trovato = true;
                                    C[i].L[i2].L[j].p.prezzo = L_Prodotti[i][i2][k].prezzo;
                                }
                            }
                            if (trovato)
                            {
                                ; //bene
                            }
                            else
                            {
                                MessageBox.Show("Qualcosa è andato storto nella conversione. Prodotto: " + C[i].L[i2].L[j].p.nome.ToString());
                            }
                        }
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.IndexOf(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)")]
        private static void FixMaiuscole(string path)
        {
            string[] lines2 = File.ReadAllLines(path);
            List<string> lines = new List<string>(lines2);

            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].StartsWith("<statistiche"))
                {
                    System.Text.StringBuilder strBuilder = new System.Text.StringBuilder(lines[i]);
                    strBuilder[1] = 'S';
                    string st = strBuilder.ToString();
                    lines[i] = st;
                }
                else if (lines[i].StartsWith("</statistiche"))
                {
                    System.Text.StringBuilder strBuilder = new System.Text.StringBuilder(lines[i]);
                    strBuilder[2] = 'S';
                    string st = strBuilder.ToString();
                    lines[i] = st;
                }
                else if (lines[i].Contains("</prodotto_stat>"))
                {
                    int xf = lines[i].IndexOf("</prodotto_stat>");
                    System.Text.StringBuilder strBuilder = new System.Text.StringBuilder(lines[i]);
                    strBuilder[xf + 2] = 'P';
                    string st = strBuilder.ToString();
                    lines[i] = st;
                }
                else if (lines[i].Contains("<prodotto_stat>"))
                {
                    int xf = lines[i].IndexOf("<prodotto_stat>");
                    System.Text.StringBuilder strBuilder = new System.Text.StringBuilder(lines[i]);
                    strBuilder[xf + 1] = 'P';
                    string st = strBuilder.ToString();
                    lines[i] = st;
                }
                else if (lines[i].Contains("<categoria>"))
                {
                    int xf = lines[i].IndexOf("<categoria>");
                    System.Text.StringBuilder strBuilder = new System.Text.StringBuilder(lines[i]);
                    strBuilder[xf + 1] = 'C';
                    string st = strBuilder.ToString();
                    lines[i] = st;
                }
                else if (lines[i].Contains("</categoria>"))
                {
                    int xf = lines[i].IndexOf("</categoria>");
                    System.Text.StringBuilder strBuilder = new System.Text.StringBuilder(lines[i]);
                    strBuilder[xf + 2] = 'C';
                    string st = strBuilder.ToString();
                    lines[i] = st;
                }
                else if (lines[i].Contains("<prezzo>") && lines[i].Contains("</prezzo>") && !lines[i + 1].Contains("<Mantissa_BigInt />"))
                {
                    string sl = lines[i];
                    lines.RemoveAt(i);
                    BigDecimal bd = Funzioni.BigDecimal_Da_Prezzo_Vecchio(sl);
                    bd.Mantissa_BigToString();
                    lines.Insert(i + 0, "<prezzo>");
                    lines.Insert(i + 1, "<Mantissa_BigInt />");
                    lines.Insert(i + 2, "<Mantissa_String>" + bd.Mantissa_String + "</Mantissa_String>");
                    lines.Insert(i + 3, "<Exponent>" + bd.Exponent.ToString() + "</Exponent>");
                    lines.Insert(i + 4, "</prezzo>");
                }
            }
            File.WriteAllLines(path, lines);
        }
    }
}