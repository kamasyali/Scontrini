﻿using ScontriniOratorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratori
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "invernali")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "giochi")]
    public class Dati_giochi_invernali
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class Dato2
        {
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public string nome;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public string cognome;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public bool is_aperitivo;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public decimal sconto;
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public DateTime data;

            public Dato2()
            {
                ;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "L")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<Dato2> L;

        public Dati_giochi_invernali()
        {
            L = new List<Dato2>();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.FileDialog.set_Filter(System.String)")]
        public void Salva()
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Dati_giochi_invernali));
            SaveFileDialog f = new SaveFileDialog
            {
                DefaultExt = "dati_invernali",
                Filter = "Dati invernali oratorio (*.dati_invernali)|*.dati_invernali|All files (*.*)|*.*"
            };
            if (f.ShowDialog() == DialogResult.OK)
            {
                var path = f.FileName;
                System.IO.FileStream file = System.IO.File.Create(path);
                writer.Serialize(file, this);
                file.Close();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.FileDialog.set_Filter(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        public void Carica(string s = "")
        {
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Dati_giochi_invernali));
            if (String.IsNullOrEmpty(s))
            {
                OpenFileDialog f = new OpenFileDialog
                {
                    DefaultExt = "dati_invernali",
                    Filter = "Dati invernali oratorio (*.dati_invernali)|*.dati_invernali|All files (*.*)|*.*"
                };
                if (f.ShowDialog() == DialogResult.OK)
                {
                    var path = f.FileName;
                    System.IO.StreamReader file = new System.IO.StreamReader(path);
                    Dati_giochi_invernali  x = (Dati_giochi_invernali)reader.Deserialize(file);

                    this.L = x.L;

                    file.Close();
                }
            }
            else
            {
                var path = s;
                System.IO.StreamReader file = new System.IO.StreamReader(path);
                Dati_giochi_invernali x = (Dati_giochi_invernali)reader.Deserialize(file);

                this.L = x.L;

                file.Close();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        public static void Visualizza()
        {
            Visualizza_dati_invernali x = new Visualizza_dati_invernali();
            x.ShowDialog();
        }
    }
}

