﻿using ScontriniOratori;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "invernali")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "giochi")]
    public partial class giochi_invernali_2015 : Form
    {
        public giochi_invernali_2015()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static giochi_invernali_2015 me;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int numero_giochi_invernali;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool aperitivo_g;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static decimal sconto;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Dati_giochi_invernali dati;

        private void Giochi_invernali_2015_Load(object sender, EventArgs e)
        {
            Carica_numero_giochi_invernali();
            me = this;
            dati = new Dati_giochi_invernali();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDecimal(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "finalmente")]
        public static void Stampa_finalmente(bool aperitivo)
        {
            aperitivo_g = aperitivo;
            sconto = Convert.ToDecimal(me.textBox3.Text);
            bool riuscito = BtnPrintGiochi_Invernali(new object(), new EventArgs());
            if (riuscito)//stampa scontrino riesce
            {
                numero_giochi_invernali++;
                File.WriteAllText(Form1.folder + "numero_giochi.txt", numero_giochi_invernali.ToString());
                Dati_giochi_invernali.Dato2 y = new Dati_giochi_invernali.Dato2()
                {
                    nome = me.textBox1.Text,
                    cognome = me.textBox2.Text,
                    data = DateTime.Now,
                    is_aperitivo = aperitivo_g,
                    sconto = sconto
                };
                dati.L.Add(y);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static bool BtnPrintGiochi_Invernali(object v, EventArgs eventArgs)
        {
            PrintDialog printDialog = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDialog.Document = printDocument;
            printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);

            Funzioni.Aspetta(500);

            //Impostazioni_stampante d2 = impostazioni.stampante;
            if (impostazioni.stampante == null || impostazioni.stampante.d[0] == null)
            {
                MessageBox.Show("Imposta una stampante dalle impostazioni!");
                return false;
            }
            else
            {
                printDocument.PrinterSettings = impostazioni.stampante.d[0].s;
                printDocument.Print();
                return true;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "1")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        public static void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics graphic = e.Graphics;

            Font font = new Font("Courier New", 8, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
            //float fontHeight = font.GetHeight();

            int startX = 10;
            int startY = -1;
            int offset = 0;

            if (impostazioni.intestazione == impostazioni.cima)
            {
                offset -= 3;
                Form1.Stampa_Intestazione(ref graphic, ref offset, startX, startY);
            }


            graphic.DrawString("  • Giochi Sportivi Invernali 2015 •", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 13;
            graphic.DrawString("          Numero scontrino: " + numero_giochi_invernali.ToString(), font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 25;
            graphic.DrawString("    "+me.textBox2.Text, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 13;
            graphic.DrawString("    "+me.textBox1.Text, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 30;
            if (aperitivo_g)
                graphic.DrawString("  APERITIVO 5.00€", font, new SolidBrush(Color.Black), startX, startY + offset);
            else
                graphic.DrawString("  TORNEO 0.00€", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 13;
            if (sconto > 0 && aperitivo_g)
            {
                graphic.DrawString("  SCONTO    "+ Prodotto.Stampa_formato_euro(sconto), font, new SolidBrush(Color.Black), startX, startY + offset);
                offset += 13;
                graphic.DrawString("  TOTALE    " + Prodotto.Stampa_formato_euro(5-sconto), font, new SolidBrush(Color.Black), startX, startY + offset);
                offset += 13;
            }
            graphic.DrawString("  Buon appetito!", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset -= 5;

            offset += 25;
            graphic.DrawString("         " + DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year + " - " + DateTime.Now.Hour + ":" + Form1.Completa_minuto(DateTime.Now.Minute), new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);

            offset += 13;
            graphic.DrawString("      www.oratoriodibrusaporto.it", font, new SolidBrush(Color.Black), startX, startY + offset);

            if (impostazioni.intestazione == impostazioni.fondo)
                Form1.Stampa_Intestazione(ref graphic, ref offset, startX, startY);

            offset += Form1.spazio_da_lasciare_per_il_taglio; //SPAZIO DA LASCIARE DOVE POI TAGLIA
            graphic.DrawString(".", font, new SolidBrush(Color.Black), startX, startY + offset);
        }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "numero")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "invernali")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "giochi")]
        public static void Carica_numero_giochi_invernali()
        {
            try
            {
                numero_giochi_invernali = Convert.ToInt32(File.ReadAllText(Form1.folder + "numero_giochi.txt"));
            }
            catch
            {
                numero_giochi_invernali = 1;
            }
            if (numero_giochi_invernali < 1)
                numero_giochi_invernali = 1;
            File.WriteAllText(Form1.folder + "numero.txt", numero_giochi_invernali.ToString());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        private void Button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                textBox1.Text = textBox1.Text.ToUpper();
                textBox2.Text = textBox2.Text.ToUpper();
                Stampa_finalmente(true);
            }
            else
            {
                MessageBox.Show("Devi inserire un nome e un cognome");
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        private void Button2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                textBox1.Text = textBox1.Text.ToUpper();
                textBox2.Text = textBox2.Text.ToUpper();
                Stampa_finalmente(false);
            }
            else
            {
                MessageBox.Show("Devi inserire un nome e un cognome");
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            dati.Salva();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            dati.Carica();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            Dati_giochi_invernali.Visualizza();
        }
    }
}
