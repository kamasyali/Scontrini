﻿using Common;
using ScontriniOratori;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    public partial class Pagato : Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sconto")]
        public Pagato()
        {
            InitializeComponent();
        }

        decimal n = 0;

        private void Pagato_Load(object sender, EventArgs e)
        {
            label3.Text = Prodotto.Stampa_formato_euro(Form1.totale_da_pagare);
            Refresh_label();
        }

        private void PictureBox4_Click(object sender, EventArgs e)
        {
            n += 5;
            Refresh_label();         
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        private void Refresh_label()
        {
            label2.Text = Prodotto.Stampa_formato_euro(n);
            BigDecimal x = n - Form1.totale_da_pagare;
            if (x < 0)
            {
                string s = Prodotto.Stampa_formato_euro(-x);
                label5.Text = s;
                label6.Text = "Mancano:";
            }
            else
            {
                label5.Text = Prodotto.Stampa_formato_euro(n - Form1.totale_da_pagare);
                label6.Text = "Resto:";
            }
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            n += 10;
            Refresh_label();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            n += 20;
            Refresh_label();
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            n += 50;
            Refresh_label();
        }

        private void PictureBox5_Click(object sender, EventArgs e)
        {
            n += 100;
            Refresh_label();
        }

        private void PictureBox6_Click(object sender, EventArgs e)
        {
            n += 200;
            Refresh_label();
        }

        private void PictureBox7_Click(object sender, EventArgs e)
        {
            n += 500;
            Refresh_label();
        }

        private void PictureBox8_Click(object sender, EventArgs e)
        {
            n += 0.01m;
            Refresh_label();
        }

        private void PictureBox9_Click(object sender, EventArgs e)
        {
            n += 0.02m;
            Refresh_label();
        }

        private void PictureBox10_Click(object sender, EventArgs e)
        {
            n += 0.05m;
            Refresh_label();
        }

        private void PictureBox11_Click(object sender, EventArgs e)
        {
            n += 0.1m;
            Refresh_label();
        }

        private void PictureBox12_Click(object sender, EventArgs e)
        {
            n += 0.2m;
            Refresh_label();
        }

        private void PictureBox13_Click(object sender, EventArgs e)
        {
            n += 0.5m;
            Refresh_label();
        }

        private void PictureBox14_Click(object sender, EventArgs e)
        {
            n += 1;
            Refresh_label();
        }

        private void PictureBox15_Click(object sender, EventArgs e)
        {
            n += 2;
            Refresh_label();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            n = 0;
            Refresh_label();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form1.pagato = n;
            Form1.me_form.Stampa_finalmente(0);
            this.Close();
        }
    }
}
