﻿using Common;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio.Forms
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    public partial class Form_Test_BigDecimal : Form
    {
        public Form_Test_BigDecimal()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt64(System.String)")]
        private void Button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text.PadRight(2,'0').Substring(0, 2);
            this.Refresh();
            Int64 p_intera = 0;
            try
            {
                p_intera = Convert.ToInt64(textBox1.Text);
            }
            catch
            {
                ;
            }
            string s_p_decimale = TrimmaZeriADestra(textBox2.Text);
            int l_p_decimale = s_p_decimale.Length;
            Int64 p_decimale = 0;
            try
            {
                p_decimale = Convert.ToInt64(s_p_decimale);
            }
            catch
            {
                ;
            }
            Int64 migliaia = (Int64)Math.Pow(10, l_p_decimale);
            decimal d2 = ((decimal)p_decimale) / ((decimal)migliaia);
            BigDecimal d = p_intera + d2;
            string s_r = Prodotto.Stampa_formato_euro(d);
            label4.Text = s_r;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "t")]
        public static string TrimmaZeriADestra(string t)
        {
            int q = 0;
            for (int i=t.Length-1; i>=0; i--)
            {
                if (t[i]=='0')
                {
                    q++;
                }
                else
                {
                    break;
                }
            }
            string r = t.Substring(0, t.Length - q);
            return r;
        }
    }
}
