﻿using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "dati")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "invernali")]
    public partial class Visualizza_dati_invernali : Form
    {
        public Visualizza_dati_invernali()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.DateTime.ToString")]
        private void Visualizza_dati_invernali_Load(object sender, EventArgs e)
        {
            panel1.AutoScroll = true;

            Label nome2 = new Label
            {
                Text = "NOME"
            };
            Label cognome2 = new Label
            {
                Text = "COGNOME"
            };
            Label data2 = new Label
            {
                Text = "DATA"
            };
            Label is_aperitivo2 = new Label
            {
                Text = "APER. O TORNEO"
            };
            Label sconto2 = new Label
            {
                Text = "SCONTO"
            };

            nome2.Location = new Point(0, 0);
            cognome2.Location = new Point(100, 0);
            is_aperitivo2.Location = new Point(200, 0);
            sconto2.Location = new Point(300, 0);
            data2.Location = new Point(400, 0);

            nome2.Size = new Size(100, 30);
            cognome2.Size = new Size(100, 30);
            is_aperitivo2.Size = new Size(100, 30);
            sconto2.Size = new Size(100, 30);
            data2.Size = new Size(100, 30);

            nome2.Parent = panel1;
            cognome2.Parent = panel1;
            is_aperitivo2.Parent = panel1;
            sconto2.Parent = panel1;
            data2.Parent = panel1;

            for (int i=0; i<giochi_invernali_2015.dati.L.Count; i++)
            {
                Label nome = new Label
                {
                    Text = giochi_invernali_2015.dati.L[i].nome.ToString()
                };
                Label cognome = new Label
                {
                    Text = giochi_invernali_2015.dati.L[i].cognome.ToString()
                };
                Label data = new Label
                {
                    Text = giochi_invernali_2015.dati.L[i].data.ToString()
                };
                Label is_aperitivo = new Label
                {
                    Text = giochi_invernali_2015.dati.L[i].is_aperitivo ? "Aperitivo" : "Torneo"
                };
                Label sconto = new Label
                {
                    Text = Prodotto.Stampa_formato_euro(giochi_invernali_2015.dati.L[i].sconto)
                };

                nome.Location = new Point(0, (i+1) * 30);
                cognome.Location= new Point(100, (i + 1) * 30);
                is_aperitivo.Location = new Point(200, (i + 1) * 30);
                sconto.Location = new Point(300, (i + 1) * 30);
                data.Location = new Point(400, (i + 1) * 30);

                nome.Size = new Size(100, 30);
                cognome.Size= new Size(100, 30);
                is_aperitivo.Size= new Size(100, 30);
                sconto.Size= new Size(100, 30);
                data.Size= new Size(100, 30);

                nome.Parent = panel1;
                cognome.Parent = panel1;
                is_aperitivo.Parent = panel1;
                sconto.Parent = panel1;
                data.Parent = panel1;

            }
        }
    }
}
