﻿using Common;
using ScontriniOratorio;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using ScontriniOratorio.Forms;
using ScontriniOratorio.Forms.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratori
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "prodotto")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "extra")]
    public partial class extra_prodotto : Form
    {
        public extra_prodotto()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public int index;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<ComboBox> con_senza_carrello;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<ComboBox> con_senza_extra;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int NUMERO_EXTRA = 17;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public Form princ;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "n")]
        public extra_prodotto(int n)
        {
            index = n;
            princ = Form1.me;
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        private void Extra_prodotto_Load(object sender, EventArgs e)
        {
            label4.Text = AbstractMode.Carrello[index].nome;
            label3.Text = Prodotto.Stampa_formato_euro(AbstractMode.Carrello[index].prezzo);
            label5.Text = label3.Text;

            checkBox1.Checked = AbstractMode.Carrello[index].checkExtraEuro;

            con_senza_carrello = new List<ComboBox>();
            con_senza_extra = new List<ComboBox>();
            con_senza_carrello.Clear();
            con_senza_extra.Clear();

            if (!(AbstractMode.Carrello[index].tipo == Costanti.Prodotto.pizza && AbstractMode.Carrello[index].tipo == Costanti.Prodotto.pizza_sottotipo))
            {
                //Form1.extra_form = null;
                this.Close();
            }
            else
            {
                for (int i = 0; i < NUMERO_EXTRA; i++)
                {
                    ComboBox con_senza = new ComboBox
                    {
                        Size = new Size(69, 25),
                        Location = new Point(0, 20 * i),
                        Visible = true,
                        Parent = panel1
                    };
                    con_senza.Items.Add("");
                    con_senza.Items.Add("CON");
                    con_senza.Items.Add("SENZA");
                    con_senza.Items.Add("ALTRO");
                    int c2 = AbstractMode.Carrello[index].con_senza[i];
                    if (c2 == Prodotto.niente)
                        con_senza.SelectedIndex = 0;
                    else if (c2 == Prodotto.con)
                        con_senza.SelectedIndex = 1;
                    else if (c2 == Prodotto.senza)
                        con_senza.SelectedIndex = 2;
                    else
                        con_senza.SelectedIndex = 3;

                    con_senza.DropDownStyle = ComboBoxStyle.DropDownList;

                    con_senza.SelectedIndexChanged += new EventHandler(Con_senza_changed);

                    ComboBox extra = new ComboBox
                    {
                        Location = new Point(70 - 2, 20 * i),
                        Size = new Size(panel1.Size.Width - 70, 20),
                        Parent = panel1
                    };
                    if (c2 == Prodotto.niente)
                        extra.Visible = false;
                    else
                        extra.Visible = true;
                    Riempi_ingredienti(ref extra, c2);
                    if (con_senza.SelectedIndex == 1) //con_index
                        extra.SelectedIndex = AbstractMode.Carrello[index].componente_extra[i];
                    else
                        extra.SelectedIndex = 0;
                    extra.Text = AbstractMode.Carrello[index].nome_extra[i];

                    if (extra.SelectedIndex > 0)
                    {
                        extra.DropDownStyle = ComboBoxStyle.DropDownList;
                    }

                    extra.SelectedIndexChanged += new EventHandler(Extra_cambiato_indice);
                    extra.TextChanged += new EventHandler(Extra_cambiato_indice);

                    con_senza_carrello.Add(con_senza);
                    con_senza_extra.Add(extra);
                    panel1.Controls.Add(con_senza);
                    panel1.Controls.Add(extra);
                }
            }
            con_senza_carrello[0].Visible = false;
            con_senza_carrello[1].Visible = false;
            con_senza_carrello[2].Visible = false;

            Label x1 = new Label
            {
                Text = "CON",
                Size = new Size(70, 13),
                Parent = panel1,
                Location = new Point(2, 4)
            };
            panel1.Controls.Add(x1);

            Label x2 = new Label
            {
                Text = "SENZA",
                Size = new Size(70, 13),
                Parent = panel1,
                Location = new Point(2, 24)
            };
            panel1.Controls.Add(x2);

            Label x3 = new Label
            {
                Text = "ALTRO",
                Size = new Size(70, 13),
                Parent = panel1,
                Location = new Point(2, 44)
            };
            panel1.Controls.Add(x3);
        }

        private void Extra_cambiato_indice(object sender, EventArgs e)
        {
            ComboBox s = (ComboBox)sender;
            int q = con_senza_extra.IndexOf(s);

            if (s.SelectedIndex < 1)
                con_senza_extra[q].DropDownStyle = ComboBoxStyle.DropDown;
            else
                con_senza_extra[q].DropDownStyle = ComboBoxStyle.DropDownList;

            Calcola_prezzo_modificato();

            return;
        }

        private void Calcola_prezzo_modificato()
        {
            if (con_senza_carrello == null)
                return;

            BigDecimal totale = Form1.Carrello[index].prezzo;
            for (int i = 0; i < NUMERO_EXTRA; i++)
            {
                if (con_senza_carrello[i].SelectedIndex == 1) //con_index
                {
                    if (con_senza_extra[i].SelectedIndex > 0)
                    {
                        totale += Form1.Extra_List[con_senza_extra[i].SelectedIndex - 1].prezzo;
                    }
                    else
                    {
                        if (checkBox1.Checked && !String.IsNullOrEmpty(con_senza_extra[i].Text))
                        {
                            totale += new BigDecimal(1, 0); // 1€
                        }
                    }
                }
            }

            label5.Text = Prodotto.Stampa_formato_euro(totale);
        }

        private void Con_senza_changed(object sender, EventArgs e)
        {
            ComboBox s = (ComboBox)sender;
            int n = s.SelectedIndex;

            int q = con_senza_carrello.IndexOf(s);
            if (n != 0)
                con_senza_extra[q].Visible = true;
            else
                con_senza_extra[q].Visible = false;
            int r;
            if (n == 0)
                r = Prodotto.niente;
            else if (n == 1)
                r = Prodotto.con;
            else if (n == 2)
                r = Prodotto.senza;
            else
                r = Prodotto.altro;

            ComboBox t = con_senza_extra[q];

            Riempi_ingredienti(ref t, r);

            Calcola_prezzo_modificato();

            return;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "q")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ingredienti")]
        public static void Riempi_ingredienti(ref ComboBox extra, int q)
        {
            extra.Items.Clear();
            extra.Items.Add("");
            if (q == Prodotto.con || q == Prodotto.senza)
                Riempi_ingredienti_da_extra_list(ref extra);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "list")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ingredienti")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "extra")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "da")]
        public static void Riempi_ingredienti_da_extra_list(ref ComboBox extra)
        {
            for (int i = 0; i < Form1.Extra_List.Count; i++)
            {
                extra.Items.Add(Form1.Extra_List[i].nome);
            }
        }

        private void Extra_prodotto_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1.extra_form = null;
        }

        private void Extra_prodotto_Deactivate(object sender, EventArgs e)
        {
            //this.Focus();
        }

        private void Extra_prodotto_Leave(object sender, EventArgs e)
        {
            //this.Focus();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Salva_modifiche();
            AbstractMode.me_form.Refresh_pannello_carrello();
            this.Close();
        }

        private void Salva_modifiche()
        {
            for (int i = 0; i < NUMERO_EXTRA; i++)
            {
                Form1.Carrello[index].componente_extra[i] = con_senza_extra[i].SelectedIndex;
                Form1.Carrello[index].nome_extra[i] = con_senza_extra[i].Text;
                int n = con_senza_carrello[i].SelectedIndex;
                int r;
                if (n == 0)
                    r = Prodotto.niente;
                else if (n == 1)
                    r = Prodotto.con;
                else if (n == 2)
                    r = Prodotto.senza;
                else
                    r = Prodotto.altro;
                Form1.Carrello[index].con_senza[i] = r;
            }

            Form1.Carrello[index].checkExtraEuro = checkBox1.Checked;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < NUMERO_EXTRA; i++)
            {
                con_senza_extra[i].SelectedIndex = 0;
                con_senza_extra[i].Text = "";
            }

            checkBox1.Checked = true;

            Calcola_prezzo_modificato();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            Calcola_prezzo_modificato();
        }
    }
}