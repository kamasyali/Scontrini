﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using ScontriniOratorio.Forms;
using ScontriniOratorio.Forms.Main;
using ScontriniOratorio.Classi.ClassiComune.Stat;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;

namespace ScontriniOratori
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "form")]
    public partial class Statistiche_form : Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Statistiche DaFile;

        private readonly int main_version = -1;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "v")]
        public Statistiche_form(int v)
        {
            this.main_version = v;
            InitializeComponent();
        }

        private void Statistiche_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1.stats = null;
        }

        private void Statistiche_Load(object sender, EventArgs e)
        {
            this.MinimumSize = this.Size;
            comboBox1.SelectedIndex = 0;
            listBox1.DoubleClick += new EventHandler(Rimuovi_file_dalla_lista);
            tabControl3.Size = tabControl2.Size;
        }

        private void Rimuovi_file_dalla_lista(object sender, EventArgs e)
        {
            if (listBox1.Items.Count != 0)
            {
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void Statistiche_form_SizeChanged(object sender, EventArgs e)
        {
            if (!(this.WindowState == FormWindowState.Maximized || this.WindowState == FormWindowState.Minimized))
                this.WindowState = FormWindowState.Maximized;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form1.Stats_Class.Salva();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dateTimePicker1.Visible = comboBox1.SelectedIndex != 0;
            numericUpDown1.Visible = dateTimePicker1.Visible;
            numericUpDown2.Visible = dateTimePicker1.Visible;
            numericUpDown3.Visible = dateTimePicker1.Visible;
            dateTimePicker2.Visible = dateTimePicker1.Visible;
            numericUpDown4.Visible = dateTimePicker1.Visible;
            numericUpDown5.Visible = dateTimePicker1.Visible;
            numericUpDown6.Visible = dateTimePicker1.Visible;
            label2.Visible = dateTimePicker1.Visible;
            label3.Visible = dateTimePicker1.Visible;
            label4.Visible = dateTimePicker1.Visible;
            label5.Visible = dateTimePicker1.Visible;
            button2.Visible = dateTimePicker1.Visible;

            DateTime da, a;
            da = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, (int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value);
            a = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, (int)numericUpDown6.Value, (int)numericUpDown5.Value, (int)numericUpDown4.Value);
            if (comboBox1.SelectedIndex == 0)
            {
                if (this.main_version == 1)
                    Visualizza(ref Form1.Stats_Class, ref tabControl2, false, da, a, label16);
                else
                    Visualizza(ref Form2.Stats_Class, ref tabControl2, false, da, a, label16);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "1")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "a")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tempo")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ristretto")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "nel")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Stats")]
        public void Visualizza(ref Statistiche Stats, ref TabControl tabControlX, bool is_ristretto_nel_tempo, DateTime da, DateTime a, Label totale)
        {
            foreach (TabPage x9 in tabControlX.TabPages)
            {
                x9.Controls.Clear();
            }
            //List<pannello_intestato> Pannelli = new List<pannello_intestato>();
            Variabili.Tabs = tabControlX;
            Variabili.Pannelli = new List<List<Pannello_intestato>>();

            if (this.main_version == 1)
            {
                if (tabControl2.TabPages.Count == 0)
                {
                    tabControl2.TabPages.Add(new TabPage("Pizze"));
                    tabControl2.TabPages.Add(new TabPage("Self-Service"));
                    tabControl2.TabPages.Add(new TabPage("Bar"));
                }

                if (tabControl3.TabPages.Count == 0)
                {
                    tabControl3.TabPages.Add(new TabPage("Pizze"));
                    tabControl3.TabPages.Add(new TabPage("Self-Service"));
                    tabControl3.TabPages.Add(new TabPage("Bar"));
                }
            }
            else
            {
                if (tabControl2.TabPages.Count == 0)
                {
                    foreach (var x2 in Form2.Prodotti)
                    {
                        tabControl2.TabPages.Add(new TabPage(x2.nome));
                    }
                }

                if (tabControl3.TabPages.Count == 0)
                {
                    foreach (var x2 in Form2.Prodotti)
                    {
                        tabControl3.TabPages.Add(new TabPage(x2.nome));
                    }
                }
            }

            Funzioni.Prepara_pannelli(tabControl2.TabPages[0].Size);
            List<List<Item_lista>> Oggetti_Prodotti = new List<List<Item_lista>>();
            for (int i = 0; i < AbstractMode.N_CATEGORIE; i++)
            {
                List<Item_lista> temp4 = new List<Item_lista>();
                Oggetti_Prodotti.Add(temp4);
            }
            Ordina(ref Stats);
            List<List<List<Prodotto>>> Prodotti;
            if (this.main_version == 1)
                Prodotti = Ottieni_Matrice_Base(Stats, is_ristretto_nel_tempo, da, a, ref Form1.Prodotti);
            else
                Prodotti = Ottieni_Matrice_Base(Stats, is_ristretto_nel_tempo, da, a, ref Form2.Prodotti);

            Funzioni.Refresh_pannelli_prodotti(ref Prodotti, totale);
        }

        private List<List<List<Prodotto>>> Ottieni_Matrice_Base(Statistiche stats, bool is_ristretto_nel_tempo, DateTime da, DateTime a, ref List<Categoria_Prodotto> L_Prodotti)
        {
            List<List<List<Prodotto>>> L = new List<List<List<Prodotto>>>();

            for (int i = 0; i < L_Prodotti.Count; i++)
            {
                L.Add(new List<List<Prodotto>>());
                for (int i2 = 0; i2 < L_Prodotti[i].Count(); i2++)
                {
                    List<Prodotto> L2 = new List<Prodotto>();

                    L[i].Add(L2);
                    for (int j = 0; j < L_Prodotti[i].get(i2).Count(); j++)
                    {
                        L[i][i2].Add(L_Prodotti[i].get(i2).Get(j));
                    }
                }
            }

            for (int i = 0; i < L_Prodotti.Count; i++)
            {
                for (int i2 = 0; i2 < L_Prodotti[i].Count(); i2++)
                {
                    for (int j = 0; j < L_Prodotti[i].get(i2).Count(); j++)
                    {
                        L_Prodotti[i].get(i2).SetQuantity(j, 0);
                    }
                }
            }

            for (int i = 0; i < L_Prodotti.Count; i++)
            {
                for (int i2 = 0; i2 < stats.C[i].L.Count; i2++)
                {
                    foreach (Prodotto_stat x in stats.C[i].L[i2].L)
                    {
                        //Console.WriteLine(x.p.quantita + "x " + x.p.nome + ", " + x.p.prezzo);
                        if (is_ristretto_nel_tempo)
                        {
                            if (Is_compreso(x.momento, da, a))
                            {
                                Aggiungi(x, ref L, i, i2);
                            }
                        }
                        else
                        {
                            Aggiungi(x, ref L, i, i2);
                        }
                    }
                }
            }

            return L;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "4#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "4")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "a")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "L")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "nel")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tempo")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ristretto")]
        public static List<List<List<Prodotto>>> Ottieni_Matrice_Base(Statistiche stats, bool is_ristretto_nel_tempo, DateTime da, DateTime a, ref List<List<List<Prodotto>>> L_Prodotti)
        {
            List<List<List<Prodotto>>> L = new List<List<List<Prodotto>>>();

            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                L.Add(new List<List<Prodotto>>());
                for (int i2 = 0; i2 < L_Prodotti[i].Count; i2++)
                {
                    List<Prodotto> L2 = new List<Prodotto>();

                    L[i].Add(L2);
                    for (int j = 0; j < L_Prodotti[i][i2].Count; j++)
                    {
                        L[i][i2].Add(L_Prodotti[i][i2][j]);
                    }
                }
            }

            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                for (int i2 = 0; i2 < L_Prodotti[i].Count; i2++)
                {
                    for (int j = 0; j < L_Prodotti[i][i2].Count; j++)
                    {
                        L_Prodotti[i][i2][j].quantita = 0;
                    }
                }
            }

            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                for (int i2 = 0; i2 < stats.C[i].L.Count; i2++)
                {
                    foreach (Prodotto_stat x in stats.C[i].L[i2].L)
                    {
                        //Console.WriteLine(x.p.quantita + "x " + x.p.nome + ", " + x.p.prezzo);
                        if (is_ristretto_nel_tempo)
                        {
                            if (Is_compreso(x.momento, da, a))
                            {
                                Aggiungi(x, ref L, i, i2);
                            }
                        }
                        else
                        {
                            Aggiungi(x, ref L, i, i2);
                        }
                    }
                }
            }

            return L;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "x")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "a")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "compreso")]
        public static bool Is_compreso(DateTime x, DateTime da, DateTime a)
        {
            if (x.Year > da.Year && x.Year < a.Year)
                return true;
            if (x.Year >= da.Year && x.Year < a.Year)
                return true;
            if (x.Year > da.Year && x.Year <= a.Year)
                return true;

            if (x.Month > da.Month && x.Month < a.Month)
                return true;
            if (x.Month >= da.Month && x.Month < a.Month)
                return true;
            if (x.Month > da.Month && x.Month <= a.Month)
                return true;

            if (x.Day > da.Day && x.Day < a.Day)
                return true;
            if (x.Day >= da.Day && x.Day < a.Day)
                return true;
            if (x.Day > da.Day && x.Day <= a.Day)
                return true;

            if (x.Hour > da.Hour && x.Hour < a.Hour)
                return true;
            if (x.Hour >= da.Hour && x.Hour < a.Hour)
                return true;
            if (x.Hour > da.Hour && x.Hour <= a.Hour)
                return true;

            if (x.Minute > da.Minute && x.Minute < a.Minute)
                return true;
            if (x.Minute >= da.Minute && x.Minute < a.Minute)
                return true;
            if (x.Minute > da.Minute && x.Minute <= a.Minute)
                return true;

            if (x.Second >= da.Second && x.Second <= a.Second)
                return true;

            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "1")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "x")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "b")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "a")]
        public static void Aggiungi(Prodotto_stat x, ref List<List<List<Prodotto>>> list, int i2a, int i2b)
        {
            var a2 = list[i2a][i2b];
            for (int i = 0; i < a2.Count; i++)
            {
                var a1 = a2[i];
                if (a1.nome == x.p.nome && a1.prezzo == x.p.prezzo)
                {
                    list[i2a][i2b][i].quantita++;
                    return;
                }
            }

            //non lo ha trovato
            x.p.old = true;
            list[i2a][i2b].Add(x.p);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.Compare(System.String,System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        public static void Ordina(ref Statistiche s)
        {
            for (int p = 0; p < s.C.Count; p++)
            {
                for (int p2 = 0; p2 < s.C[p].L.Count; p2++)
                {
                    for (int i = 0; i < s.C[p].L[p2].L.Count; i++)
                        for (int j = 0; j < s.C[p].L[p2].L.Count - 1; j++)
                        {
                            int x2 = String.Compare(s.C[p].L[p2].L[j].p.nome, s.C[p].L[p2].L[j + 1].p.nome);
                            if (x2 > 0)
                            {
                                Prodotto_stat temp = s.C[p].L[p2].L[j];
                                s.C[p].L[p2].L[j] = s.C[p].L[p2].L[j + 1];
                                s.C[p].L[p2].L[j + 1] = temp;
                            }
                        }
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DateTime da, a;
            da = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, (int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value);
            a = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, (int)numericUpDown6.Value, (int)numericUpDown5.Value, (int)numericUpDown4.Value);
            if (comboBox1.SelectedIndex == 1)
                Visualizza(ref Form1.Stats_Class, ref tabControl2, true, da, a, label16);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.FileDialog.set_Filter(System.String)")]
        private void Button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog x = new OpenFileDialog
            {
                DefaultExt = "jsonscontr",
                Filter = "Statistiche scontrini oratorio (*.jsonscontr)|*.jsonscontr|Statistiche scontrini oratorio (*.xmlscontr)|*.xmlscontr|All files (*.*)|*.*"
            };
            if (x.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    if (listBox1.Items[i].ToString() == x.FileName)
                        return;
                }
                listBox1.Items.Add(x.FileName);
            }
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dateTimePicker3.Visible = comboBox2.SelectedIndex != 0;
            numericUpDown7.Visible = dateTimePicker3.Visible;
            numericUpDown8.Visible = dateTimePicker3.Visible;
            numericUpDown9.Visible = dateTimePicker3.Visible;
            dateTimePicker4.Visible = dateTimePicker3.Visible;
            numericUpDown10.Visible = dateTimePicker3.Visible;
            numericUpDown11.Visible = dateTimePicker3.Visible;
            numericUpDown12.Visible = dateTimePicker3.Visible;
            label10.Visible = dateTimePicker3.Visible;
            label11.Visible = dateTimePicker3.Visible;
            label12.Visible = dateTimePicker3.Visible;
            label13.Visible = dateTimePicker3.Visible;
            button5.Visible = dateTimePicker3.Visible;

            DateTime da, a;
            da = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, (int)numericUpDown7.Value, (int)numericUpDown8.Value, (int)numericUpDown9.Value);
            a = new DateTime(dateTimePicker4.Value.Year, dateTimePicker4.Value.Month, dateTimePicker4.Value.Day, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value);
            if (comboBox1.SelectedIndex == 0)
                Visualizza(ref DaFile, ref tabControl3, false, da, a, label17);
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            DateTime da, a;
            da = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, (int)numericUpDown7.Value, (int)numericUpDown8.Value, (int)numericUpDown9.Value);
            a = new DateTime(dateTimePicker4.Value.Year, dateTimePicker4.Value.Month, dateTimePicker4.Value.Day, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value);
            if (comboBox1.SelectedIndex == 0)
                Visualizza(ref DaFile, ref tabControl3, true, da, a, label17);
        }

        private void Statistiche_form_Shown(object sender, EventArgs e)
        {
            DaFile = new Statistiche();
            for (int i = 0; i < Form1.N_CATEGORIE; i++)
            {
                Categoria_Stat x = new Categoria_Stat();
                DaFile.C.Add(new Lista_Categorie());
                DaFile.C[i].L.Add(x);
            }
            comboBox2.SelectedIndex = 0;
        }

        private void TabControl1_Enter(object sender, EventArgs e)
        {
        }

        private void TabPage3_Enter(object sender, EventArgs e)
        {
            ;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private void Button4_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DaFile.C.Count; i++)
            {
                for (int i2 = 0; i2 < DaFile.C[i].L.Count; i2++)
                    DaFile.C[i].L[i2].L.Clear();
            }
            List<Statistiche> L = new List<Statistiche>();
            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                string s = listBox1.Items[i].ToString();
                Statistiche s2 = new Statistiche();
                s2.Carica(s);
                L.Add(s2);
            }
            for (int i = 0; i < L.Count; i++)
            {
                for (int p = 0; p < Form1.N_CATEGORIE; p++)
                {
                    var s5 = L[i].C;
                    Lista_Categorie s6;
                    try
                    {
                        s6 = s5[p];
                    }
                    catch
                    {
                        continue;
                    }

                    for (int p2 = 0; p2 < s6.L.Count; p2++)
                    {
                        for (int j = 0; true; j++)
                        {
                            bool x9;
                            try
                            {
                                x9 = j < L[i].C[p].L[p2].L.Count;
                            }
                            catch
                            {
                                MessageBox.Show("Errore imprevisto! Probabilmente il formato delle statistiche è corrotto.");
                                return;
                            }
                            if (!x9)
                                break;

                            DaFile.C[p].L[p2].L.Add(L[i].C[p].L[p2].L[j]);
                        }
                    }
                }
            }

            Visualizza(ref DaFile, ref tabControl3, false, new DateTime(), new DateTime(), label17);
        }
    }
}