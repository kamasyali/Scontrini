﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "scritta")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "form")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "evento")]
    public partial class Scritta_evento_form : Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "S")]
        public static List<string> S = null; //scritta
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "percorso")]
        public const string percorso = "scritta_evento";

        public Scritta_evento_form()
        {
            InitializeComponent();
        }

        private void Scritta_evento_form_Load(object sender, EventArgs e)
        {
            if (S==null)
            {
                S = new List<string>();
                File.WriteAllText(Form1.folder + percorso + ".txt", "");
            }
            textBox1.Text = "";
            string[] S2 = new string[S.Count];
            for (int i=0; i<S.Count; i++)
            {
                S2[i] = S[i];
            }
            textBox1.Lines = S2;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            S = new List<string>(textBox1.Lines);
            File.WriteAllText(Form1.folder + percorso + ".txt", textBox1.Text);
        }
       
    }
}
