﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tabellone")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "form")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "display")]
    public partial class Tabellone_display_form : Form
    {
        public Tabellone_display_form()
        {
            InitializeComponent();
        }

        private void Tabellone_display_form_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        internal void Ridisegna_tabellone()
        {
            if (Form1.tabellone == null)
            {
                Form1.tabellone = new Tabellone();
            }
            listBox1.Items.Clear();

            for (int j=Form1.tabellone.L.Count-1; j>=0; j--)
            {
                Tabellone.Ordinazione o = Form1.tabellone.L[j];
                listBox1.Items.Add(o);
                for (int i=0; i<o.Nomi.Count; i++)
                {
                    string riga = o.Quantita[i].ToString().PadLeft(4, ' ') + "x -- " + o.Nomi[i].ToString();
                    listBox1.Items.Add(riga);
                }
                string grechina = " ";
                listBox1.Items.Add(grechina);
                listBox1.Items.Add(grechina);
            }
        }

        private readonly SolidBrush reportsForegroundBrushSelected = new SolidBrush(Color.DarkGray);
        private readonly SolidBrush reportsForegroundBrush = new SolidBrush(Color.Black);
        //private SolidBrush reportsBackgroundBrushSelected = new SolidBrush(Color.FromKnownColor(KnownColor.Highlight));
        //private SolidBrush reportsBackgroundBrush1 = new SolidBrush(Color.White);
        //private SolidBrush reportsBackgroundBrush2 = new SolidBrush(Color.Gray);

        //custom method to draw the items, don't forget to set DrawMode of the ListBox to OwnerDrawFixed
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void LbReports_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBox lbReports = listBox1;
            e.DrawBackground();
            bool selected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            int index = e.Index;
            if (index >= 0 && index < lbReports.Items.Count)
            {
                string text = lbReports.Items[index].ToString();
                Graphics g = e.Graphics;

                //background:
                SolidBrush backgroundBrush;

                Color colore = Color.White;
                string s = text;
                if (s.Length >= 1)
                {
                    if (s[0] == '#')
                    {
                        Tabellone.Ordinazione p = (Tabellone.Ordinazione)lbReports.Items[index];
                        colore = p.colore;
                    }
                }
                backgroundBrush = new SolidBrush(colore);

                g.FillRectangle(backgroundBrush, e.Bounds);

                //text:
                SolidBrush foregroundBrush = (selected) ? reportsForegroundBrushSelected : reportsForegroundBrush;
                g.DrawString(text, e.Font, foregroundBrush, lbReports.GetItemRectangle(index).Location);
            }

            e.DrawFocusRectangle();
        }

        private void Tabellone_display_form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1.tabellone_form = null;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Ridisegna_tabellone();
        }

        private void Timer1_Tick_1(object sender, EventArgs e)
        {
            Ridisegna_tabellone();
        }

        private void ListBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            /*
            Color colore = Color.White;
            string s = (string)sender;
            if (s.Length>=1)
            {
                if (s[0] == '#')
                {
                    tabellone.ordinazione p = (tabellone.ordinazione)sender;
                    colore = p.colore;
                }
            }

            e.DrawBackground();
            Graphics g = e.Graphics;

            g.FillRectangle(new SolidBrush(colore), e.Bounds);

            // Print text

            e.DrawFocusRectangle();
            */

            LbReports_DrawItem(sender, e);
        
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        public static void Cambiacolore(object s)
        {
            string s2 = s.ToString();
            if (s2.Length >= 1)
            {
                if (s2[0] == '#')
                {
                    Inverti_colore(ref s);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1007:UseGenericsWhereAppropriate")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "colore")]
        public static void Inverti_colore(ref object s)
        {
            Tabellone.Ordinazione p = (Tabellone.Ordinazione)s;
            if (p.colore==Color.Red)
            {
                p.colore = Color.Green;
            }
            else if (p.colore==Color.Green)
            {
                p.colore = Color.Red;
            }
        }

        private void ListBox1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                Cambiacolore(listBox1.SelectedItem);
                Ridisegna_tabellone();
            }
        }
    }
}
