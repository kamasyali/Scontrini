﻿using ScontriniOratori;
using ScontriniOratorio.Forms.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tabellone")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "form")]
    public partial class Indirizzo_tabellone_form : Form
    {

        public Indirizzo_tabellone_form()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private void Button1_Click(object sender, EventArgs e)
        {
            byte[] b2 = null;
            try
            {
                b2 = new byte[] { (byte)Convert.ToInt32(textBox1.Text), (byte)Convert.ToInt32(textBox2.Text), (byte)Convert.ToInt32(textBox3.Text), (byte)Convert.ToInt32(textBox4.Text) };
            }
            catch
            {
                MessageBox.Show("Errore di conversione in byte. Sei sicuro di aver scritto l'indirizzo in modo corretto?");
            }
            finally
            {
                AbstractMode.Indirizzo_ip_tabellone = new IPAddress(b2);
                Salva_su_disco();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Byte.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "su")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "disco")]
        public static void Salva_su_disco()
        {
            byte[] b2 = Form1.Indirizzo_ip_tabellone.GetAddressBytes();
            File.WriteAllText(Form1.folder + "i1.txt", b2[0].ToString());
            File.WriteAllText(Form1.folder + "i2.txt", b2[1].ToString());
            File.WriteAllText(Form1.folder + "i3.txt", b2[2].ToString());
            File.WriteAllText(Form1.folder + "i4.txt", b2[3].ToString());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Byte.ToString")]
        private void Indirizzo_tabellone_form_Load(object sender, EventArgs e)
        {
            if (Form1.Indirizzo_ip_tabellone == null)
            {
                byte[] b2 = { 0, 0, 0, 0 };
                AbstractMode.Indirizzo_ip_tabellone = new IPAddress(b2);
                Salva_su_disco();
            }
            else
            {
                try
                {
                    textBox1.Text = Form1.Indirizzo_ip_tabellone.GetAddressBytes()[0].ToString();
                    textBox2.Text = Form1.Indirizzo_ip_tabellone.GetAddressBytes()[1].ToString();
                    textBox3.Text = Form1.Indirizzo_ip_tabellone.GetAddressBytes()[2].ToString();
                    textBox4.Text = Form1.Indirizzo_ip_tabellone.GetAddressBytes()[3].ToString();
                }
                catch {; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (Form1.Indirizzo_ip_tabellone.GetAddressBytes()[0] == 0)
            {
                MessageBox.Show("Non è possibile cambiare lo stato del server se non si imposta prima l'indirizzo ANCHE per il server. Lo leggi dal tasto qui a fianco");
            }
            else
            {
                if (checkBox1.Checked)
                {
                    Thread thread = new Thread(() => Server2.StartListening(Form1.Indirizzo_ip_tabellone));
                    thread.Start();
                }
                else
                {
                    Thread thread = new Thread(() => Server2.StopListening());
                    thread.Start();
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String)")]
        private void Button2_Click(object sender, EventArgs e)
        {
            String strHostName = Dns.GetHostName();
            IPHostEntry x = Dns.GetHostEntry(strHostName);
            string risposta = "";
            foreach (IPAddress ipaddress in x.AddressList)
            {
                string s = ipaddress.ToString();
                if (s.Length <= 15)
                {
                    if (Char.IsDigit(s[0]))
                    {
                        risposta += ipaddress.ToString() + '\n';
                    }
                }
            }
            MessageBox.Show(risposta, "Indirizzo di rete");
        }
    }
}
