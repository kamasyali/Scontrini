﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ScontriniOratorio;
using System.Deployment.Application;
using System.Net;

namespace ScontriniOratori
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "impostazioni")]
    public partial class impostazioni : Form
    {
        public impostazioni()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Impostazioni_stampante stampante;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int intestazione = niente;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool intestazione_estesa;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int ordine = niente;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static char seconda_stampante = '1';
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int separa_stampa; //separiamo la stampa di prodotti diversi
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int avvisa_nostampanti;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "niente")]
        public const int niente = 0;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "cima")]
        public const int cima = 1;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "fondo")]
        public const int fondo = 2;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "alfabetico")]
        public const int alfabetico = 1;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "utente")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "definito")]
        public const int definito_utente = 2;



        private void Impostazioni_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1.imp = null;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button1_Click(object sender, EventArgs e)
        {
            password_cartella_programma a = new password_cartella_programma();
            a.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Carica_stampante(0);           
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "i")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "stampante")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2233:OperationsShouldNotOverflow", MessageId = "i+1")]
        public static void Carica_stampante(int i)
        {
            if (stampante == null)
            {
                stampante = new Impostazioni_stampante();
                for (int j = 0; j < 2; j++)
                    stampante.d.Add(null);
            }
            if (stampante.d == null)
            {
                stampante.d = new List<Impostazione_stampante>();
                Carica_stampante(i);
            }
            else if (stampante.d[i] == null)
            {
                stampante.d[i] = new Impostazione_stampante();
                PrintDialog x2 = new PrintDialog();
                if (x2.ShowDialog() == DialogResult.OK)
                {
                    stampante.d[i].s = x2.PrinterSettings;

                    try
                    {
                        File.Delete(Form1.folder + ((i + 1).ToString()) + ".printersettings");
                    }
                    catch
                    {
                        ;
                    }
                    stampante.Salva(Form1.folder + ((i + 1).ToString()) + ".printersettings", i);
                }
                else
                {
                    stampante.d = null;
                }
            }
            else
            {
                PrintDialog x2 = new PrintDialog();
                if (x2.ShowDialog() == DialogResult.OK)
                {
                    stampante.d[i].s = x2.PrinterSettings;
                    stampante.d[i].is_valid = true;

                    try
                    {
                        File.Delete(Form1.folder + ((i + 1).ToString()) + ".printersettings");
                    }
                    catch
                    {
                        ;
                    }

                    Funzioni.Aspetta(1000);

                    stampante.Salva(Form1.folder + ((i + 1).ToString()) + ".printersettings", i);
                }
            }
        }

        private void Impostazioni_Load(object sender, EventArgs e)
        {
            if (intestazione == niente)
            {
                impostazioni.intestazione = impostazioni.fondo;
                radioButton2.Checked = true;
                File.WriteAllText(Form1.folder + Form1.inte, "FONDO");
            }
            else if (intestazione == fondo)
            {
                radioButton2.Checked = true;
            }
            else if (intestazione == cima)
            {
                radioButton1.Checked = true;
            }
            radioButton1.CheckedChanged += new EventHandler(Cambiato_scelta_intestazione);


            if (impostazioni.ordine == impostazioni.niente)
            {
                impostazioni.ordine = impostazioni.definito_utente;
                radioButton3.Checked = true;
                File.WriteAllText(Form1.folder + Form1.ordine, "NO");
            }
            else if (impostazioni.ordine == impostazioni.definito_utente)
            {
                radioButton3.Checked = true;
            }
            else if (impostazioni.ordine == impostazioni.alfabetico)
            {
                radioButton4.Checked = true;
            }
            radioButton3.CheckedChanged += new EventHandler(Cambiato_scelta_ordine);

            radioButton5.Checked = (impostazioni.seconda_stampante=='2');
            radioButton6.Checked = (impostazioni.seconda_stampante == '1');
            radioButton7.Checked = (impostazioni.seconda_stampante == 'T');
            radioButton5.CheckedChanged += new EventHandler(Cambiato_scelta_seconda_stampante);
            radioButton6.CheckedChanged += new EventHandler(Cambiato_scelta_seconda_stampante);
            radioButton7.CheckedChanged += new EventHandler(Cambiato_scelta_seconda_stampante);
            radioButton7.CheckedChanged += new EventHandler(Cambiato_tabellone);

            if (impostazioni.separa_stampa==0)
            {
                checkBox1.Checked = false;
            }
            else
            {
                checkBox1.Checked = true;
            }


            if (impostazioni.avvisa_nostampanti == 0)
            {
                checkBox2.Checked = false;
            }
            else
            {
                checkBox2.Checked = true;
            }

            checkBox3.Checked = (impostazioni.intestazione_estesa);

            Carica_versione_programma();

            button3.Enabled = radioButton5.Checked;
        }

        private void Cambiato_tabellone(object sender, EventArgs e)
        {
            button6.Enabled = radioButton7.Checked;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        private void Carica_versione_programma()
        {
            DateTime d = Funzioni.Versione_Attuale();
            label2.Text = "v " + Funzioni.DataItaliana(d) + " " + d.Hour.ToString().PadLeft(2,'0') + ":" + d.Minute.ToString().PadLeft(2, '0') + ":" + d.Second.ToString().PadLeft(2, '0');
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void Cambiato_scelta_seconda_stampante(object sender, EventArgs e)
        {
            try
            {
                File.Delete(Form1.folder + Form1.seconda);
            }
            catch
            {
                ;
            }
            if (radioButton5.Checked)
            {
                File.WriteAllText(Form1.folder + Form1.seconda, "2");
                seconda_stampante = '2';
            }
            else
            {
                if (radioButton6.Checked)
                {
                    File.WriteAllText(Form1.folder + Form1.seconda, "1");
                    seconda_stampante = '1';
                }
                else
                {
                    File.WriteAllText(Form1.folder + Form1.seconda, "T");
                    seconda_stampante = 'T';
                }
            }
            button3.Enabled = radioButton5.Checked;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private void Cambiato_scelta_ordine(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
            {
                impostazioni.ordine = impostazioni.definito_utente;
                File.WriteAllText(Form1.folder + Form1.ordine, "NO");
            }
            else
            {
                impostazioni.ordine = impostazioni.alfabetico;
                File.WriteAllText(Form1.folder + Form1.ordine, "ALFABETICO");
            }
            MessageBox.Show("Perchè la modifica dell'ordine risulti effettiva, bisogna riavviare il programma");
        }

        private void Cambiato_scelta_intestazione(object sender, EventArgs e)
        {
            if (radioButton1.Checked==true)
            {
                impostazioni.intestazione = impostazioni.cima;
                File.WriteAllText(Form1.folder + Form1.inte, "CIMA");
            }
            else
            {
                impostazioni.intestazione = impostazioni.fondo;
                File.WriteAllText(Form1.folder + Form1.inte, "FONDO");
            }
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            ;
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            ;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Carica_stampante(1);
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            ;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button4_Click(object sender, EventArgs e)
        {
            Scritta_evento_form x = new Scritta_evento_form();
            x.ShowDialog();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button6_Click(object sender, EventArgs e)
        {
            Indirizzo_tabellone_form x = new Indirizzo_tabellone_form();
            x.ShowDialog();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button5_Click_1(object sender, EventArgs e)
        {
            Numero_pizze_form x = new Numero_pizze_form();
            x.ShowDialog();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons)")]
        private void Button7_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Sei sicuro di voler reimpostare tutto? Saranno ripristinati anche tutti i prezzi!", "Conferma", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Form1.Cose_iniziali();
                MessageBox.Show("Ripristino eseguito con successo!");
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                File.Delete(Form1.folder + "separa_stampa.txt");
            }
            catch
            {
                ;
            }
            if (checkBox1.Checked)
            {
                File.WriteAllText(Form1.folder + "separa_stampa.txt", "1");
                impostazioni.separa_stampa = 1;
            }
            else
            {            
                File.WriteAllText(Form1.folder + "separa_stampa.txt", "0");
                impostazioni.separa_stampa = 0;
            }
        }

        private void RadioButton5_CheckedChanged(object sender, EventArgs e)
        {

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        private void Button8_Click(object sender, EventArgs e)
        {
            stampante = new Impostazioni_stampante();
            for (int i=0; i<stampante.d.Count; i++)
            {
                stampante.d[i].s = null;
                stampante.d[i].is_valid = false;
                stampante.d[i].Salva(Form1.folder + ((i + 1).ToString()) + ".printersettings");
            }

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            string sp = "avvisa_nostampanti.txt";
            try
            {
                File.Delete(Form1.folder + sp);
            }
            catch
            {
                ;
            }
            if (checkBox2.Checked)
            {
                File.WriteAllText(Form1.folder + sp, "1");
                impostazioni.avvisa_nostampanti = 1;
            }
            else
            {
                File.WriteAllText(Form1.folder + sp, "0");
                impostazioni.avvisa_nostampanti = 0;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            string sp = "intestazione_estesa.txt";
            try
            {
                File.Delete(Form1.folder + sp);
            }
            catch
            {
                ;
            }
            if (checkBox3.Checked)
            {
                File.WriteAllText(Form1.folder + sp, "1");
                impostazioni.intestazione_estesa = true;
            }
            else
            {
                File.WriteAllText(Form1.folder + sp, "0");
                impostazioni.intestazione_estesa = false;
            }
        }
    }
}
