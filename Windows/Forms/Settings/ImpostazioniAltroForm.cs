﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio.Forms
{
    public partial class ImpostazioniAltroForm : Form
    {
        public ImpostazioniAltroForm()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (Form2.stats == null)
            {
                Form2.stats = new Statistiche_form(2);
            }
            Form2.stats.Show();
            Form2.stats.Focus();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (Form2.imp == null)
                Form2.imp = new impostazioni();
            Form2.imp.ShowDialog();
        }
    }
}
