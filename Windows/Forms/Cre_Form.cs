﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    public partial class Cre_Form : Form
    {
        public Cre_Form()
        {
            InitializeComponent();
        }
        decimal s = 0;
        Font font;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int spazio_da_lasciare_per_il_taglio = 20;
        bool seconda = false;

        private void Cre_Form_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDecimal(System.String)")]
        private void Button1_Click(object sender, EventArgs e)
        {
            string s2 = textBox1.Text;
            if (String.IsNullOrEmpty(s2)||String.IsNullOrWhiteSpace(s2))
            {
                MessageBox.Show("Devi scrivere qualcosa per stampare!");
            }
            else
            {
                s = 0;
                if (s2 == "0")
                {
                    Errore_Zero();
                }
                else {
                    try
                    {
                        s2 = s2.Replace('.', ',');
                        s = Convert.ToDecimal(s2);
                    }
                    catch {;}
                    if (s > 0)
                    {
                        Stampa();
                    }
                    else
                    {
                        if (s2 == "0.0" || s2 == "0.00")
                        {
                            Errore_Zero();
                        }
                        else
                        {
                            MessageBox.Show("Devi inserire un numero valido!");
                        }
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static void Errore_Zero()
        {
            MessageBox.Show("Devi inserire un valore maggiore di 0");
        }

        private void Stampa()
        {
            bool b = BtnPrintReciept_Click(new object(), new EventArgs());
            if (b)
            {
                textBox1.Text = "";
            }
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public bool BtnPrintReciept_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDialog.Document = printDocument;

            printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);

            Funzioni.Aspetta(500);

            //Impostazioni_stampante d2 = impostazioni.stampante;
            if (impostazioni.stampante == null || impostazioni.stampante.d[0] == null)
            {
                MessageBox.Show("Imposta una stampante dalle impostazioni!");
            }
            else if ((impostazioni.seconda_stampante == '2') && impostazioni.stampante.d[1] == null)
            {
                MessageBox.Show("Imposta una seconda stampante dalle impostazioni!");
            }
            else
            {
                printDocument.PrinterSettings = impostazioni.stampante.d[0].s;
                printDocument.Print();

                Funzioni.Aspetta(500);

                if (seconda)
                {

                    PrintDialog printDialog2 = new PrintDialog();
                    PrintDocument printDocument2 = new PrintDocument();
                    printDialog2.Document = printDocument2;
                    printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);

                    Funzioni.Aspetta(500);

                    printDocument2.PrinterSettings = impostazioni.stampante.d[0].s;
                    printDocument2.Print();

                }

                return true;
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "1")]
        public void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics graphic = e.Graphics;

            font = new Font("Courier New", 15, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
            float fontHeight = font.GetHeight();

            int startX = 10;
            int startY = -1;
            int offset = 0;



            Stampa_singolo_prodotto(ref graphic, s ,ref offset, startX, startY);


            string d = "  " + DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year + " - " + DateTime.Now.Hour + ":" + Form1.Completa_minuto(DateTime.Now.Minute);
            graphic.DrawString(d, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += (int)fontHeight;


            offset += spazio_da_lasciare_per_il_taglio; //SPAZIO DA LASCIARE DOVE POI TAGLIA
            graphic.DrawString(".", font, new SolidBrush(Color.Black), startX, startY + offset);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Stampa_singolo_prodotto(ref Graphics graphic, decimal s3, ref int offset, int startX, int startY)
        {
            Font font2 = new Font("Courier New", 15, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
            float fontHeight = font.GetHeight();

            string s2 = "PREZZO: " + Formatta_Prezzo(s3);
            graphic.DrawString(s2.PadLeft(17), font2, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight + 3;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Decimal.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static string Formatta_Prezzo(decimal s2)
        {
            int i = (int)s2;
            decimal d;
            d = s2 - i;
            if (d == 0)
            {
                return i.ToString() + ".00";
            }
            else
            {
                string d3 = d.ToString();
                d3 = d3.Remove(0, 2);
                string d2 = (d3).PadRight(2, '0');
                return i.ToString() + "." + d2;
            }
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            seconda = checkBox1.Checked;
        }
    }
}
