﻿using Common;
using ScontriniOratori;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ScontriniOratorio.Forms.Main
{
    public partial class AbstractMode : Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static AbstractMode me_form;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<Prodotto> Carrello;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<Prodotto> Extra_List;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static extra_prodotto extra_form = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Font fontp = new Font("Courier New", 15, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Font font;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static BigDecimal fontHeightp;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static BigDecimal pagato = 0;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static BigDecimal totale_da_pagare = 0;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int numero_pizza_arrivato = -100;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "spazio")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "intestazione")]
        public const int spazio_intestazione = 19;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int spazio_da_lasciare_per_il_taglio = 20;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "spazio")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "pizzaiolo")]
        public const int spazio_pizzaiolo = 17;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Statistiche Stats_Class;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Statistiche_form stats = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static string folder = "";
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static string cognome = "";

        int quante_stampare = 0;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public string[] file_read = {
            "\\Pizze\\pizze.txt",           //00
            "\\Pizze\\bevande.txt",         //01
            "\\Pizze\\altro.txt",           //02
            "\\Self\\coperto.txt",          //03
            "\\Self\\primi_piatti.txt",     //04
            "\\Self\\secondi_piatti.txt",   //05
            "\\Self\\secondi_freddi.txt",   //06
            "\\Self\\contorno.txt",         //07
            "\\Self\\dolce.txt",            //08
            "\\Self\\panini.txt",           //09
            "\\Pizze\\bevande.txt",         //10
            "\\Bar\\caffetteria.txt",       //11
            "\\Bar\\bibite.txt",            //12
            "\\Bar\\dolciumi.txt",          //13
            "\\Bar\\snack.txt",             //14
            "\\Bar\\alcolici.txt",          //15
            "\\Bar\\gelati.txt",            //16
            "\\Bar\\altro.txt",             //17
            "\\Self\\altro.txt",            //18
            "\\Self\\menu.txt"              //19
        };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CATEGORIE")]
        public const int N_CATEGORIE = 20;
        /*
        * 00 => ...
        * 19 => Menu
        */



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool is_pizzaiolo = false;

        public AbstractMode()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static IPAddress Indirizzo_ip_tabellone;

        private void AbstractMode_Load(object sender, EventArgs e)
        {
            ;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "pannello")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "carrello")]
        public virtual void Refresh_pannello_carrello()
        {
            throw new NotImplementedException();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "1")]
        public static void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            BigDecimal cash = pagato;
            BigDecimal change;
            Graphics graphic = e.Graphics;

            font = new Font("Courier New", 8, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
            float fontHeight = font.GetHeight();

            int startX = 10;
            int startY = -1;
            int offset = 0;

            if (impostazioni.intestazione == impostazioni.cima)
            {
                offset -= 3;
                Stampa_Intestazione(ref graphic, ref offset, startX, startY);
            }

            if (is_pizzaiolo == false)
            {
                string top = " Nome prodotto".PadRight(30) + "  Prezzo";
                graphic.DrawString(top, font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + (int)fontHeight - 2; //make the spacing consistent
                graphic.DrawString("--------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
                offset += (int)fontHeight + 3; //make the spacing consistent
            }



            Stampa_Prodotti(ref graphic, ref offset, startX, startY);

            BigDecimal totalprice_parziale = CalcoloTotale();


            //BigDecimal scontato = totalprice_parziale * (AbstractMode.percentuale_sconto / 100d);
            BigDecimal totalprice_totale = Funzioni.PrezzoConSconto(totalprice_parziale, AbstractMode.percentuale_sconto / 100d);

            if (Variabili.Stampa.Ultimo && AbstractMode.me_form.quante_stampare > 0)
                change = cash - totale_da_pagare;
            else
                change = cash - totalprice_totale;

            if (is_pizzaiolo == false)
            {
                if (AbstractMode.percentuale_sconto == 0)
                {
                    offset += 19; //make some room so that the total stands out.
                    if (Variabili.Stampa.Ultimo && AbstractMode.me_form.quante_stampare > 0)
                    {
                        graphic.DrawString("SUB-TOTALE".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_totale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                        offset += 14; //make some room so that the total stands out.
                        graphic.DrawString("TOTALE".PadRight(31) + Prodotto.Stampa_formato_euro(totale_da_pagare).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                    else if (AbstractMode.me_form.quante_stampare > 0)
                    {
                        graphic.DrawString("SUB-TOTALE".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_totale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                    else
                    {
                        graphic.DrawString("TOTALE".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_totale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                }
                else //con sconto
                {
                    offset += 19; //make some room so that the total stands out.
                    graphic.DrawString("PARZIALE".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_parziale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    offset += 14; //make some room so that the total stands out.
                    graphic.DrawString("SCONTO".PadRight(31) + (Funzioni.StampaPercentuale(percentuale_sconto)).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    offset += 14; //make some room so that the total stands out.
                    if (Variabili.Stampa.Ultimo && AbstractMode.me_form.quante_stampare > 0)
                    {
                        graphic.DrawString("SUB-TOTALE".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_totale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                        offset += 14; //make some room so that the total stands out.
                        graphic.DrawString("TOTALE (CON SCONTO)".PadRight(31) + Prodotto.Stampa_formato_euro(totale_da_pagare).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                    else if (AbstractMode.me_form.quante_stampare > 0)
                    {
                        graphic.DrawString("SUB-TOTALE".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_totale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                    else
                    {
                        graphic.DrawString("TOTALE (CON SCONTO)".PadRight(31) + Prodotto.Stampa_formato_euro(totalprice_totale).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                }

                if (change > 0)
                {
                    if (Variabili.resto)
                    {
                        offset += 19;
                        graphic.DrawString("PAGATO".PadRight(31) + Prodotto.Stampa_formato_euro(cash).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                        offset += 13;
                        graphic.DrawString("RESTO".PadRight(31) + Prodotto.Stampa_formato_euro(change).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                    else if (Variabili.Stampa.Ultimo && AbstractMode.me_form.quante_stampare > 0)
                    {
                        offset += 19;
                        graphic.DrawString("PAGATO".PadRight(31) + Prodotto.Stampa_formato_euro(cash).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                        offset += 13;
                        graphic.DrawString("RESTO".PadRight(31) + Prodotto.Stampa_formato_euro(change).PadLeft(7), font, new SolidBrush(Color.Black), startX, startY + offset);
                    }
                }
            }

            string cognome = Form2.cognome;
            Stampa_Spazio_Pizza(ref graphic, ref offset, startX, startY, cognome);

            if (is_pizzaiolo == false)
            {
                //Stampa_Evento(ref graphic, ref offset, startX, startY);
                Stampa_Scritta_Evento(ref graphic, ref offset, startX, startY);
            }


            if (impostazioni.intestazione_estesa)
            {
                offset += 25;
            }
            else
            {
                offset += 8;
            }

            graphic.DrawString("         " + DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year + " - " + DateTime.Now.Hour + ":" + Completa_minuto(DateTime.Now.Minute), new Font("Courier New", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);

            if (is_pizzaiolo == false)
            {
                offset += 13;
                graphic.DrawString("      www.oratoriodibrusaporto.it", font, new SolidBrush(Color.Black), startX, startY + offset);
            }

            if (impostazioni.intestazione == impostazioni.fondo)
                Stampa_Intestazione(ref graphic, ref offset, startX, startY);

            offset += spazio_da_lasciare_per_il_taglio; //SPAZIO DA LASCIARE DOVE POI TAGLIA
            graphic.DrawString(".", font, new SolidBrush(Color.Black), startX, startY + offset);
        }

        private static BigDecimal CalcoloTotale()
        {
            BigDecimal totalprice_parziale = 0;
            if (impostazioni.separa_stampa == 1)
            {
                foreach (Prodotto item in Variabili.Carrello[Variabili.quale])
                {
                    BigDecimal x3 = item.prezzo;
                    for (int i = 0; i < extra_prodotto.NUMERO_EXTRA; i++)
                    {
                        if (item.con_senza[i] == Prodotto.con)
                        {
                            if (item.componente_extra[i] > 0)
                                x3 += Form2.Extra_List[item.componente_extra[i] - 1].prezzo;
                            else if (item.checkExtraEuro)
                                x3 += new BigDecimal(1, 0); //1€
                        }
                    }
                    totalprice_parziale += x3;
                }
            }
            else
            {
                foreach (Prodotto item in Carrello)
                {
                    BigDecimal x3 = item.prezzo;
                    for (int i = 0; i < extra_prodotto.NUMERO_EXTRA; i++)
                    {
                        if (item.con_senza[i] == Prodotto.con)
                        {
                            if (item.componente_extra[i] > 0)
                                x3 += Form2.Extra_List[item.componente_extra[i] - 1].prezzo;
                            else if (item.checkExtraEuro)
                                x3 += new BigDecimal(1, 0); //1€
                        }
                    }
                    totalprice_parziale += x3;
                }
            }

            return totalprice_parziale;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private static void Stampa_Scritta_Evento(ref Graphics graphic, ref int offset, int startX, int startY)
        {
            Stampa_grechina(ref graphic, ref offset, startX, startY, true);

            offset += 6; //spazio random dopo la grechina

            if (Scritta_evento_form.S != null)
            {
                for (int i = 0; i < Scritta_evento_form.S.Count; i++)
                {
                    string scritta = Scritta_evento_form.S[i];
                    offset += 13;
                    graphic.DrawString(scritta, font, new SolidBrush(Color.Black), startX, startY + offset);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "grechina")]
        public static void Stampa_grechina(ref Graphics graphic, ref int offset, int startX, int startY, bool piccola = false)
        {
            int grandezza = 9;
            string inizio = "";
            string intermezzo = "";
            string linea = "";
            if (piccola)
            {
                grandezza = 5;
                inizio = "      ";
                intermezzo = "۩۩";
                linea = "▬▬▬▬▬▬▬";
            }

            offset += 20;
            graphic.DrawString(inizio + "       ●" + linea + "▬▬▬▬▬▬▬๑۩" + intermezzo + "۩๑▬▬▬▬▬▬▬" + linea + "●", new Font("Courier New", grandezza, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static void Stampa_Intestazione(ref Graphics graphic, ref int offset, int startX, int startY)
        {
            if (impostazioni.intestazione == impostazioni.fondo)
                offset += 70 + 14; //spazio da lasciare per il prossimo scontrino
            graphic.DrawString("ORATORIO DI BRUSAPORTO", new Font("Courier New", 14, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);

            if (impostazioni.intestazione_estesa)
            {
                Stampa_grechina(ref graphic, ref offset, startX, startY);
            }
            else
            {
                //non stampa la grechina e si risparmia carta
                ;
            }

            if (impostazioni.intestazione == impostazioni.cima)
                offset += 25; //spazio da lasciare per l'inizio dei prodotti
        }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private static void Stampa_Prodotti(ref Graphics graphic, ref int offset, int startX, int startY)
        {
            List<Prodotto> Stampare = Stampa_Prodotti1();

            Font font2 = new Font("Courier New", 8, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
            BigDecimal fontHeight = font2.GetHeight();

            for (int i = 0; i < Stampare.Count; i++)
                for (int j = i + 1; j < Stampare.Count; j++)
                    if (Is_uguali(Stampare[i], Stampare[j]))
                    {
                        Stampare[i].quantita += Stampare[j].quantita;
                        Stampare.RemoveAt(j);
                        j--;
                    }

            foreach (Prodotto i in Stampare)
            {
                BigDecimal prezzo_finale = i.prezzo;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (i.con_senza[j] == Prodotto.con)
                    {
                        if (i.componente_extra[j] > 0)
                        {
                            List<int> i3 = i.componente_extra;
                            int i2 = i3[j];
                            Prodotto p = Extra_List[i2 - 1];
                            BigDecimal d = p.prezzo;
                            prezzo_finale += d;
                        }
                        else if (i.checkExtraEuro)
                        {
                            prezzo_finale += new BigDecimal(1,0); //1€
                        }
                    }                   
                }
                prezzo_finale *= i.quantita;

                Stampa_singolo_prodotto(ref graphic, i.quantita, i.nome, prezzo_finale, ref offset, startX, startY);

                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (i.con_senza[j] != Prodotto.niente)
                    {
                        if (!String.IsNullOrEmpty(i.nome_extra[j]))
                        {
                            string s2 = i.nome_extra[j];
                            if (i.con_senza[j] == Prodotto.con)
                                s2 = " CON " + s2;
                            else if (i.con_senza[j] == Prodotto.senza)
                                s2 = " SENZA " + s2;
                            else if (i.con_senza[j] == Prodotto.altro)
                                s2 = " " + s2;
                            if (!is_pizzaiolo)
                            {
                                graphic.DrawString("   " + s2.PadRight(28), font2, new SolidBrush(Color.Black), startX, startY + offset);
                                offset = offset + (int)fontHeight + 3;
                            }
                            else
                            {
                                List<string> d5 = Spezza(s2, spazio_pizzaiolo);
                                graphic.DrawString("   " + d5[0].PadRight(28), fontp, new SolidBrush(Color.Black), startX, startY + offset);
                                offset = offset + (int)fontHeightp + 3;
                                if (d5[1].Length > 0)
                                {
                                    graphic.DrawString("   " + d5[1].PadRight(28), fontp, new SolidBrush(Color.Black), startX, startY + offset);
                                    offset = offset + (int)fontHeightp + 3;
                                }
                            }
                        }
                    }
                }
            }
        }

        private static List<Prodotto> Stampa_Prodotti1()
        {
            List<Prodotto> Stampare = new List<Prodotto>();
            if (impostazioni.separa_stampa == 1)
            {
                foreach (Prodotto i in Variabili.Carrello[Variabili.quale])
                    i.quantita = 1;

                foreach (Prodotto i in Variabili.Carrello[Variabili.quale])
                {
                    if (is_pizzaiolo)
                    {
                        if (i.tipo == 0)
                            Stampare.Add(i);
                    }
                    else
                        Stampare.Add(i);
                }

            }
            else
            {
                foreach (Prodotto i in Carrello)
                    i.quantita = 1;

                foreach (Prodotto i in Carrello)
                {
                    if (is_pizzaiolo)
                    {
                        if (i.tipo == 0)
                            Stampare.Add(i);
                    }
                    else
                        Stampare.Add(i);
                }
            }

            return Stampare;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "4")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2233:OperationsShouldNotOverflow", MessageId = "startX+50")]
        public static void Stampa_Spazio_Pizza(ref Graphics graphic, ref int offset, int startX, int startY, string cognome)
        {
            const int fine_y = 272;
            const int inizio_y = 50;
            const int altezza_riquadro = 30;
            Font font2 = new Font("Courier New", 8, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up

            bool pizza = false;
            for (int i = 0; i < Carrello.Count; i++)
            {
                if (Carrello[i].tipo == 0)
                {
                    pizza = true;
                    break;
                }
            }
            if (pizza)
            {
                offset += 25;
                //graphic.DrawLine(Pens.Black, startX + inizio_y, startY + offset, fine_y, startY + offset);
                //graphic.DrawLine(Pens.Black, startX + inizio_y, startY + offset + altezza_riquadro, fine_y, startY + offset + altezza_riquadro);
                //graphic.DrawLine(Pens.Black, startX + inizio_y, startY + offset, startX + inizio_y, startY + offset + altezza_riquadro);
                //graphic.DrawLine(Pens.Black, fine_y, startY + offset + altezza_riquadro, fine_y, startY + offset);
                graphic.DrawString("NUMERO".PadLeft(6) + "   " + numero_pizza_arrivato.ToString(), font2, new SolidBrush(Color.Black), startX, startY + offset);
                offset += 15;
                graphic.DrawLine(Pens.Black, startX + inizio_y, startY + offset, fine_y, startY + offset);
                graphic.DrawLine(Pens.Black, startX + inizio_y, startY + offset + altezza_riquadro, fine_y, startY + offset + altezza_riquadro);
                graphic.DrawLine(Pens.Black, startX + inizio_y, startY + offset, startX + inizio_y, startY + offset + altezza_riquadro);
                graphic.DrawLine(Pens.Black, fine_y, startY + offset + altezza_riquadro, fine_y, startY + offset);

                cognome = cognome.ToUpper();
                graphic.DrawString(cognome, font2, new SolidBrush(Color.Black), startX + inizio_y + 3, startY + offset + 3);
                //Console.WriteLine((startX + inizio_y).ToString() + " " + (startY + offset).ToString() + " " + fine_y.ToString() + " " + (startY + offset).ToString());


                graphic.DrawString("TAVOLO".PadLeft(6), font2, new SolidBrush(Color.Black), startX, startY + offset);
                offset += altezza_riquadro;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "v")]
        public static List<string> Spezza(string nome, int v)
        {
            string extra = "";
            List<int> Spazi = new List<int>();
            string s1;
            if (nome.Length > v)
            {
                for (int i = 0; i < nome.Length; i++)
                    if (nome[i] == ' ')
                        Spazi.Add(i);
                int x = Spazi[Spazi.Count / 2];
                s1 = nome.Substring(0, x);
                extra = nome.Substring(x + 1);
            }
            else
            {
                s1 = nome;
            }
            List<string> X = new List<string>
            {
                s1,
                extra
            };
            if (X[1].Length < 0 || (X[1].Length == 1 && X[1][0] == ' '))
                X[1] = "";
            return X;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "4#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "singolo")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "prodotto")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "finale")]
        public static void Stampa_singolo_prodotto(ref Graphics graphic, int quantita, string nome, BigDecimal prezzo_finale, ref int offset, int startX, int startY)
        {
            Font font2 = new Font("Courier New", 8, FontStyle.Bold); //must use a mono spaced font as the spaces need to line up
            float fontHeight = font2.GetHeight();



            List<string> s3;
            if (!is_pizzaiolo)
                s3 = Spezza(nome, 28);
            else
                s3 = Spezza(nome, spazio_pizzaiolo);

            if (!is_pizzaiolo)
            {
                graphic.DrawString(quantita.ToString().PadLeft(2) + " " + Max_string(s3[0].PadRight(28)) + Prodotto.Stampa_formato_euro(prezzo_finale).PadLeft(7), font2, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + (int)fontHeight + 3;
            }
            else
            {
                graphic.DrawString(quantita.ToString().PadLeft(2) + " " + Max_string(s3[0].PadRight(28)), fontp, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + (int)fontHeightp + 3;
            }
            if (s3[1].Length > 0)
            {
                if (!is_pizzaiolo)
                {
                    graphic.DrawString("   " + Max_string(s3[1]), font2, new SolidBrush(Color.Black), startX, startY + offset);
                    offset = offset + (int)fontHeight + 3;
                }
                else
                {
                    graphic.DrawString("   " + Max_string(s3[1]), fontp, new SolidBrush(Color.Black), startX, startY + offset);
                    offset = offset + (int)fontHeightp + 3;
                }
            }
        }




        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "v")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "string")]
        public static string Max_string(string v)
        {
            if (v.Length > 28)
            {
                string s = "";
                for (int i = 0; i < 28; i++)
                    s += v[i];
                return s;
            }
            return v;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "1")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Convalidare gli argomenti di metodi pubblici", MessageId = "0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "uguali")]
        public static bool Is_uguali(Prodotto p1, Prodotto p2)
        {
            if (p1.nome != p2.nome)
                return false;
            if (p1.prezzo != p2.prezzo)
                return false;
            for (int i = 0; i < extra_prodotto.NUMERO_EXTRA; i++)
            {
                if (p1.con_senza[i] != p2.con_senza[i])
                    return false;
                if (p1.componente_extra[i] != p2.componente_extra[i])
                    return false;
            }

            return true;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "minuto")]
        public static string Completa_minuto(int minute)
        {
            string s = minute.ToString();
            if (s.Length == 1)
                s = "0" + s;
            return s;
        }




        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool pizzaG;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static bool BtnPrintReciept_Click(object sender, EventArgs e)
        {
            if (impostazioni.stampante == null || impostazioni.stampante.d[0] == null)
            {
                MessageBox.Show("Imposta una stampante dalle impostazioni!");
                return false;
            }

            if (!impostazioni.stampante.d[0].is_valid)
            {
                if (impostazioni.avvisa_nostampanti == 1)
                {
                    DialogResult dialogResult = MessageBox.Show("Non è impostata alcuna stampante, e dalle impostazioni è impostato di avvisarti, proseguire comunque con una stampa \"a vuoto\"?", "Avviso!", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        return true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }
            }

            PrintDialog printDialog = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDialog.Document = printDocument;

            bool pizza = false;
            for (int i = 0; i < Carrello.Count; i++)
            {
                if (Carrello[i].tipo == 0)
                {
                    pizza = true;
                    break;
                }
            }
            pizzaG = pizza;

            if ((pizzaG && Variabili.quale == 0 && impostazioni.separa_stampa == 1) || (impostazioni.separa_stampa == 0 && pizzaG))
            {
                Variabili.Annullato_cognome = false;
                Cognome c2 = new Cognome();
                c2.ShowDialog();
            }

            if ((Variabili.quale == 0 && impostazioni.separa_stampa == 1) || (impostazioni.separa_stampa == 0))
            {
                if (Variabili.Annullato_cognome)
                {
                    return false;
                }
            }

            is_pizzaiolo = false;

            if (impostazioni.stampante.d[0].is_valid)
            {
                printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);
                Funzioni.Aspetta(500);
            }

            if (impostazioni.stampante.d[0].is_valid)
            {
                printDocument.PrinterSettings = impostazioni.stampante.d[0].s;
                printDocument.Print();
            }
            else
            {
                return true;
            }

            //seconda stampa per pizzaiolo
            if (impostazioni.separa_stampa == 0 || (impostazioni.separa_stampa == 1 && Variabili.quale == 0))
            {
                //Impostazioni_stampante d2 = impostazioni.stampante;
                if ((impostazioni.seconda_stampante == '2') && impostazioni.stampante.d[1] == null)
                {
                    MessageBox.Show("Imposta una seconda stampante dalle impostazioni!");
                    return false;
                }
                else
                {

                    if (pizza)
                    {
                        if (impostazioni.seconda_stampante == 'T')
                        {
                            Invio_dati_via_tabellone();
                            return true;
                        }

                        //MessageBox.Show("Premere OK per proseguire con la stampa dello scontrino per il pizzaiolo");
                        Funzioni.Aspetta(500);

                        PrintDialog printDialog2 = new PrintDialog();
                        PrintDocument printDocument2 = new PrintDocument();
                        printDialog2.Document = printDocument2;
                        is_pizzaiolo = true;

                        int sn = -1;
                        if (impostazioni.seconda_stampante == '2')
                        {
                            printDocument2.PrinterSettings = impostazioni.stampante.d[1].s;
                            sn = 1;
                        }
                        else if (impostazioni.seconda_stampante == '1')
                        {
                            printDocument2.PrinterSettings = impostazioni.stampante.d[0].s;
                            sn = 0;
                        }
                        else
                        {
                            MessageBox.Show("Errore! Segnarlo allo sviluppatore! Eccezione nella scelta della seconda stampante per la stampa dello scontrino pizze.");
                            return false;
                        }

                        if (impostazioni.stampante.d[sn].is_valid)
                        {
                            printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);
                            Funzioni.Aspetta(500);
                            printDocument2.Print();
                        }

                        return true;

                    }
                    return true;
                }
            }
            return true;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "via")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tabellone")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "dati")]
        public static void Invio_dati_via_tabellone()
        {
            foreach (Prodotto i in Carrello)
                i.quantita = 1;
            List<Prodotto> Stampare = new List<Prodotto>();
            foreach (Prodotto i in Carrello)
            {
                if (i.tipo == 0)
                    Stampare.Add(i);
            }


            for (int i = 0; i < Stampare.Count; i++)
                for (int j = i + 1; j < Stampare.Count; j++)
                    if (Is_uguali(Stampare[i], Stampare[j]))
                    {
                        Stampare[i].quantita += Stampare[j].quantita;
                        Stampare.RemoveAt(j);
                        j--;
                    }


            List<string> S = new List<string>();
            List<int> Q = new List<int>();
            foreach (Prodotto i in Stampare)
            {
                string s3 = i.nome;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (i.con_senza[j] != Prodotto.niente)
                    {
                        if (!String.IsNullOrEmpty(i.nome_extra[j]))
                        {
                            string s2 = i.nome_extra[j];
                            if (i.con_senza[j] == Prodotto.con)
                                s2 = " CON " + s2;
                            else if (i.con_senza[j] == Prodotto.senza)
                                s2 = " SENZA " + s2;
                            else if (i.con_senza[j] == Prodotto.altro)
                                s2 = " " + s2;

                            s3 += s2;
                        }
                    }
                }
                S.Add(s3);
                Q.Add(i.quantita);
            }

            Invia_al_tabellone(S, Q, Form2.numero_pizza_arrivato, cognome);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "s")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "q")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "al")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tabellone")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "pizza")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "arrivato")]
        public static void Invia_al_tabellone(List<string> s, List<int> q, int numero_pizza_arrivato, string cognome)
        {
            Tabellone t = new Tabellone();
            Tabellone.Ordinazione o = new Tabellone.Ordinazione()
            {
                cognome = cognome,
                numero_pizza = numero_pizza_arrivato,
                Nomi = s,
                Quantita = q
            };
            t.L.Add(o);

            Invio i = new Invio();
            Thread thread = new Thread(() => i.Invia(t));
            thread.Start();

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static BigDecimal percentuale_sconto;


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sconto")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "finalmente")]
        public void Stampa_finalmente(BigDecimal percentuale_sconto)
        {
            AbstractMode.percentuale_sconto = percentuale_sconto;
            Variabili.Stampa = new VariabiliStampa
            {
                Ultimo = false
            };

            Button3_Click(new object(), new EventArgs()); //ordina il carrello!

            bool riuscito = true;

            if (impostazioni.separa_stampa == 1)
            {
                Variabili.Carrello = new List<List<Prodotto>>();

                for (int i = 0; i < Costanti.N_MACROCATEGORIE; i++)
                {
                    List<Prodotto> c22 = new List<Prodotto>();
                    Variabili.Carrello.Add(c22);
                }

                foreach (var item in Carrello)
                {
                    if (item.nome.Contains("GRANITA"))
                    {
                        Variabili.Carrello[8].Add(item);
                    }
                    else if (item.tipo == 0)
                    {
                        Variabili.Carrello[0].Add(item);
                    }
                    else if (item.tipo == 11 || item.tipo == 14 || item.tipo == 13 || item.tipo == 16)
                    {
                        Variabili.Carrello[1].Add(item);
                    }
                    else if ((item.nome.Contains("CAFFE") || item.nome.Contains("LIQUOR")) && !item.nome.Contains("SORBETTO"))
                    {
                        if (item.tipo == 17 || item.tipo == 18)
                        {
                            Variabili.Carrello[1].Add(item);
                        }
                        else
                        {
                            Variabili.Carrello[1].Add(item);
                        }
                    }
                    else if (item.nome.Contains("SORBETTO"))
                    {
                        Variabili.Carrello[2].Add(item);
                    }
                    else if (item.tipo == 1 || item.tipo == 10 || item.tipo == 15 || item.tipo == 12 || item.tipo == 17)
                    {
                        Variabili.Carrello[2].Add(item);
                    }
                    else if (item.tipo == 9 || item.tipo == 8 || item.tipo == 7 || item.tipo == 6 || item.tipo == 5 || item.tipo == 4 || item.tipo == 3)
                    {
                        Variabili.Carrello[3].Add(item);
                    }
                    else if (item.nome.Contains("CREPES"))
                    {
                        Variabili.Carrello[4].Add(item);
                    }
                    else if (item.nome.Contains("ZUCCHERO") || item.nome.Contains("POP CORN") || item.nome.Contains("POP-CORN"))
                    {
                        Variabili.Carrello[5].Add(item);
                    }
                    else if (item.tipo == 2 || item.nome.Contains("PATATINE"))
                    {
                        Variabili.Carrello[6].Add(item);
                    }
                    else if (item.nome.Contains("GONFIABILI"))
                    {
                        Variabili.Carrello[7].Add(item);
                    }
                    else
                    {
                        Variabili.Carrello[9].Add(item);
                    }

                }


                quante_stampare = 0;
                for (int i = 0; i < Costanti.N_MACROCATEGORIE; i++)
                {
                    if (Variabili.Carrello[i].Count > 0)
                    {
                        quante_stampare++;
                    }
                }

                if (quante_stampare > 1)
                {
                    Variabili.resto = false;
                }
                else
                {
                    Variabili.resto = true;
                }

                List<int> qualistampare = new List<int>();

                for (int i = 0; i < Costanti.N_MACROCATEGORIE; i++)
                {
                    if (Variabili.Carrello[i].Count > 0)
                    {
                        qualistampare.Add(i);
                    }
                }


                if (qualistampare.Count > 1)
                {
                    for (int i = 0; i < qualistampare.Count - 1; i++)
                    {
                        Variabili.quale = qualistampare[i];
                        riuscito &= AbstractMode.BtnPrintReciept_Click(new object(), new EventArgs());
                        
                    }

                    Variabili.quale = qualistampare[qualistampare.Count-1];
                    Variabili.Stampa.Ultimo = true;
                    riuscito &= AbstractMode.BtnPrintReciept_Click(new object(), new EventArgs());
                    Variabili.Stampa.Ultimo = false;


                }
                else if (qualistampare.Count == 1)
                {
                    Variabili.quale = qualistampare[0];
                    riuscito &= AbstractMode.BtnPrintReciept_Click(new object(), new EventArgs());
                }


            }
            else
            {
                Variabili.resto = true;
                riuscito &= AbstractMode.BtnPrintReciept_Click(new object(), new EventArgs());
            }


            if (riuscito)//stampa scontrino riesce
            {
                Form2.Salva_carrello_in_statistiche();
                Funzioni.SvuotaCarrello();
                if (Form2.pizzaG)
                {
                    numero_pizza_arrivato++;
                    File.WriteAllText(folder + "numero.txt", numero_pizza_arrivato.ToString());
                }
            }
            Form2.cognome = "";
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.Compare(System.String,System.String)")]
        private static void Button3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Carrello.Count; i++)
                for (int j = 0; j < Carrello.Count - 1; j++)
                {
                    int x2 = Carrello[j].tipo;
                    int x3 = Carrello[j + 1].tipo;
                    if (x2 < x3)
                    {
                        Prodotto t = Carrello[j];
                        Carrello[j] = Carrello[j + 1];
                        Carrello[j + 1] = t;
                    }
                    else if (x2 == x3)
                    {
                        int x = String.Compare(Carrello[j].nome, Carrello[j + 1].nome);
                        if (x > 0)
                        {
                            Prodotto t = Carrello[j];
                            Carrello[j] = Carrello[j + 1];
                            Carrello[j + 1] = t;
                        }
                    }
                }
            AbstractMode.me_form.Refresh_pannello_carrello();
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "scontrino")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "html")]
        public static string Genera_scontrino_html()
        {
            string s = "";
            s += "<html>";
            s += "<head><style>table,th,td{border: 1px solid black;}</style></head>";
            Button3_Click(new object(), new EventArgs()); //ordina il carrello
            s += "<table width=100% cellpadding='10'>";
            s += "<tr>";
            s += "<td>PREZZO</td>";
            s += "<td>NOME</td>";
            s += "</tr>";
            for (int i = 0; i < Carrello.Count; i++)
            {
                s += "<tr>";
                string s2 = Prodotto.Stampa_formato_euro(Carrello[i].prezzo);
                s += "<td>" + s2.Substring(0, s2.Length - 1) + "&#8364;" + "</td>";
                s += "<td>" + WebUtility.HtmlEncode(Carrello[i].nome) + "</td>";
                s += "</tr>";
            }
            s += "</table>";
            s += "</html>";
            return s;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public virtual void Button2_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sconto")]
        public void Stampa_Scontrino_Click(bool conresto, BigDecimal percentuale_sconto)
        {
            if (Carrello.Count < 1)
            {
                MessageBox.Show("Il carrello non può essere vuoto!");
                return;
            }

            if (conresto)
            {
                Stampa_con_controllo_prezzo();
            }
            else
            {
                pagato = Funzioni.PrezzoConSconto(totale_da_pagare, percentuale_sconto / 100d);
                Stampa_finalmente(percentuale_sconto);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "sconto")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "prezzo")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "controllo")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "con")]
        public static void Stampa_con_controllo_prezzo()
        {
            Pagato a = new Pagato();
            a.Show();
        }

        //endclass
    }
}
