﻿using Common;
using ScontriniOratorio;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using ScontriniOratorio.Classi.ClassiComune.Stat;
using ScontriniOratorio.Forms;
using ScontriniOratorio.Forms.Main;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace ScontriniOratori
{
    public partial class Form2 : AbstractMode
    {
        public Form2()
        {
            InitializeComponent();
        }

        //VARIABILI E COSTANTI UTILI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static impostazioni imp = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "vertical")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "spazio")]
        public const int spazio_vertical2 = 17;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int primo_schermo = 0;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool print_ready = false;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "inte")]
        public const string inte = "intestazione.txt";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ordine")]
        public const string ordine = "ordine.txt";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "seconda")]
        public const string seconda = "seconda.txt";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Font default_font = new Font(FontFamily.GenericSansSerif, 7f);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool dispari_evento = true;

        //LISTE DI PRODOTTI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static List<Categoria_Prodotto> Prodotti;

        //LISTE DI OGGETTI GRAFICI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static List<List<List<Item_lista>>> Oggetti_Prodotti;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<Item_lista> Oggetti_Carrello;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static List<List<Pannello_intestato>> Pannelli;

        //GENIALATA
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Form2 me;

        //FUNZIONI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button6_Click(object sender, EventArgs e)
        {
            ImpostazioniAltroForm f = new ImpostazioniAltroForm();
            f.ShowDialog();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            me = this;
            AbstractMode.me_form = this;
            Variabili.MainMode = 2;

            fontHeightp = fontp.GetHeight();

            this.WindowState = FormWindowState.Maximized;
            this.MinimumSize = this.Size;

            Cose_iniziali();

            //TestBigDecimal();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        public static void TestBigDecimal()
        {
            string s = "";
            for (decimal i = 0; i < 1000; i++)
            {
                if (true)
                {
                    BigDecimal b = i + (((i % 100) / 100));
                    s += Prodotto.Stampa_formato_euro(b);
                    s += '\n';
                }
            }
            MessageBox.Show(s);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "iniziali")]
        public static void Cose_iniziali()
        {
            for (int i = 0; i < 4; i++)
            {
                Form2.me.tabControl1.TabPages[i].Controls.Clear();
            }
            Size s = me.tabControl1.TabPages[0].Size;
            Variabili.Tabs = Form2.me.tabControl1;
            Form2.Pannelli = new List<List<Pannello_intestato>>();
            Variabili.Pannelli = Form2.Pannelli;

            Funzioni.Prepara_pannelli_new(s);
            Riempi_liste(folder + "\\Prezzi\\v2");

            Elimina_zip();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "impostazioni")]
        public static void Carica_impostazioni()
        {
            impostazioni.stampante = new Impostazioni_stampante();
            for (int i = 0; i < 2; i++)
            {
                Carica_impostazioni_stampante(i);
            }

            Carica_impostazioni_intestazione();
            Carica_impostazioni_ordine_prodotti();
            Carica_impostazioni_seconda_stampante();
            Carica_numero_pizza();
            Carica_scritta_evento();
            Carica_indirizzo_tabellone();
            Carica_impostazioni_separa_stampa();
            Carica_impostazioni_avvisa_nostampanti();
            Carica_impostazioni_intestazione_estesa();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static void Carica_impostazioni_intestazione_estesa()
        {
            string sp = "intestazione_estesa.txt";
            string s = Carica_impostazione(sp);

            if (String.IsNullOrEmpty(s))
            {
                try
                {
                    File.Delete(folder + sp);
                }
                catch
                {
                    ;
                }
                File.WriteAllText(folder + sp, "1");
                impostazioni.intestazione_estesa = true;
            }
            else if (s == "0")
            {
                impostazioni.intestazione_estesa = false;
            }
            else if (s == "1")
            {
                impostazioni.intestazione_estesa = true;
            }
            else
            {
                MessageBox.Show("Errore nel leggere il file " + sp);
                Application.Exit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "impostazione")]
        public static string Carica_impostazione(string sp)
        {
            string s = "";
            try
            {
                s = File.ReadAllText(folder + sp);
            }
            catch
            {
                ;
            }

            return s;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static void Carica_impostazioni_avvisa_nostampanti()
        {
            string sp = "avvisa_nostampanti.txt";
            string s = Carica_impostazione(sp);

            if (String.IsNullOrEmpty(s))
            {
                try
                {
                    File.Delete(folder + sp);
                }
                catch
                {
                    ;
                }
                File.WriteAllText(folder + sp, "1");
                impostazioni.avvisa_nostampanti = 1;
            }
            else if (s == "0")
            {
                impostazioni.avvisa_nostampanti = 0;
            }
            else if (s == "1")
            {
                impostazioni.avvisa_nostampanti = 1;
            }
            else
            {
                MessageBox.Show("Errore nel leggere il file " + sp);
                Application.Exit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static void Carica_impostazioni_separa_stampa()
        {
            string s = "";
            string sp = "separa_stampa.txt";
            try
            {
                s = File.ReadAllText(folder + sp);
            }
            catch
            {
                ;
            }
            if (String.IsNullOrEmpty(s))
            {
                try
                {
                    File.Delete(folder + inte);
                }
                catch
                {
                    ;
                }
                File.WriteAllText(folder + sp, "1");
                impostazioni.separa_stampa = 1;
            }
            else if (s == "0")
            {
                impostazioni.separa_stampa = 0;
            }
            else if (s == "1")
            {
                impostazioni.separa_stampa = 1;
            }
            else
            {
                MessageBox.Show("Errore nel leggere il file " + sp);
                Application.Exit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private static void Carica_indirizzo_tabellone()
        {
            try
            {
                int a1 = Convert.ToInt32(File.ReadAllText(folder + "i1.txt"));
                int a2 = Convert.ToInt32(File.ReadAllText(folder + "i2.txt"));
                int a3 = Convert.ToInt32(File.ReadAllText(folder + "i3.txt"));
                int a4 = Convert.ToInt32(File.ReadAllText(folder + "i4.txt"));
                byte[] b2 = { (byte)a1, (byte)a2, (byte)a3, (byte)a4 };
                AbstractMode.Indirizzo_ip_tabellone = new IPAddress(b2);
            }
            catch
            {
                byte[] b3 = { 0, 0, 0, 0 };
                AbstractMode.Indirizzo_ip_tabellone = new IPAddress(b3);
                Indirizzo_tabellone_form.Salva_su_disco();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void Carica_scritta_evento()
        {
            Scritta_evento_form.S = new List<string>();
            try
            {
                string line;
                System.IO.StreamReader file = new System.IO.StreamReader(folder + Scritta_evento_form.percorso + ".txt");
                while ((line = file.ReadLine()) != null)
                {
                    Scritta_evento_form.S.Add(line);
                }
                file.Close();
            }
            catch
            {
                Scritta_evento_form.S = new List<string>();
                File.WriteAllText(folder + Scritta_evento_form.percorso + ".txt", "");
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        private static void Carica_numero_pizza()
        {
            try
            {
                numero_pizza_arrivato = Convert.ToInt32(File.ReadAllText(folder + "numero.txt"));
            }
            catch
            {
                numero_pizza_arrivato = 1;
            }
            if (numero_pizza_arrivato < 1)
                numero_pizza_arrivato = 1;
            File.WriteAllText(folder + "numero.txt", numero_pizza_arrivato.ToString());
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static void Carica_impostazioni_seconda_stampante()
        {
            string s = "";
            try
            {
                s = File.ReadAllText(folder + seconda);
            }
            catch
            {
                ;
            }
            if (String.IsNullOrEmpty(s))
            {
                try
                {
                    File.Delete(folder + seconda);
                }
                catch
                {
                    ;
                }
                File.WriteAllText(folder + seconda, "1");
                impostazioni.seconda_stampante = '1';
            }
            else if (s == "1")
            {
                impostazioni.seconda_stampante = '1';
            }
            else if (s == "2")
            {
                impostazioni.seconda_stampante = '2';
            }
            else if (s == "T")
            {
                impostazioni.seconda_stampante = 'T';
            }
            else
            {
                MessageBox.Show("Errore nel leggere il file della modalità di invio pizze. E' stata scelta la stampante primaria. Questa impostazione è modificabile nelle impostazioni");
                File.WriteAllText(folder + seconda, "1");
                impostazioni.seconda_stampante = '1';
                //Application.Exit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static void Carica_impostazioni_ordine_prodotti()
        {
            string s = "";
            try
            {
                s = File.ReadAllText(folder + ordine);
            }
            catch
            {
                ;
            }
            if (String.IsNullOrEmpty(s))
            {
                try
                {
                    File.Delete(folder + ordine);
                }
                catch
                {
                    ;
                }
                File.WriteAllText(folder + ordine, "NO");
                impostazioni.ordine = impostazioni.definito_utente;
            }
            else if (s == "NO")
            {
                impostazioni.ordine = impostazioni.definito_utente;
            }
            else if (s == "ALFABETICO")
            {
                impostazioni.ordine = impostazioni.alfabetico;
            }
            else
            {
                MessageBox.Show("Errore nel leggere il file " + ordine);
                Application.Exit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String)")]
        private static void Carica_impostazioni_intestazione()
        {
            string s = "";
            try
            {
                s = File.ReadAllText(folder + inte);
            }
            catch
            {
                ;
            }
            if (String.IsNullOrEmpty(s))
            {
                try
                {
                    File.Delete(folder + inte);
                }
                catch
                {
                    ;
                }
                File.WriteAllText(folder + inte, "CIMA");
                impostazioni.intestazione = impostazioni.cima;
            }
            else if (s == "FONDO")
            {
                impostazioni.intestazione = impostazioni.fondo;
            }
            else if (s == "CIMA")
            {
                impostazioni.intestazione = impostazioni.cima;
            }
            else
            {
                MessageBox.Show("Errore nel leggere il file " + inte);
                Application.Exit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void Elimina_zip()
        {
            try
            {
                File.Delete(folder + "Scontrini.zip");
            }
            catch
            {
                ;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Console.WriteLine(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        private static void Carica_impostazioni_stampante(int n)
        {
            string sn = folder + ((n + 1).ToString()) + ".printersettings";
            bool? riuscito = impostazioni.stampante.Carica(sn, n);
            Console.WriteLine("CARICA IMPOSTAZIONI: " + riuscito);
            if (riuscito == null || riuscito == false)
            {
                impostazioni.stampante.d[n].s = null;
                impostazioni.stampante.d[n].is_valid = false;
                impostazioni.stampante.d[n].Salva(sn);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1809:AvoidExcessiveLocals")]
        private static void Riempi_liste(string folder)
        {
            Prodotti = new List<Categoria_Prodotto>
            {
                new Categoria_Prodotto("Pizze"),
                new Categoria_Prodotto("Bevande"),
                new Categoria_Prodotto("Self"),
                new Categoria_Prodotto("Bar"),
                new Categoria_Prodotto("Gelati"),
                new Categoria_Prodotto("Altro")
            };

            Riempi_liste2(folder, 0, new List<string>() { "pizze", "patatine" }); //Pizze
            Riempi_liste2(folder, 1, new List<string>() { "acqua", "bibite", "vino", "alcolici", "birra" }); //Bevande
            Riempi_liste2(folder, 2, new List<string>() { "primi_piatti", "secondi_piatti", "secondi_freddi", "contorni", "menu", "panini" }); //Self
            Riempi_liste2(folder, 3, new List<string>() { "caffetteria", "dolciumi", "snack" }); //Bar
            Riempi_liste2(folder, 4, new List<string>() { "gelati", "granite", "sorbetto" }); //Gelati
            Riempi_liste2(folder, 5, new List<string>() { "coperto", "piadine", "crepes", "altro", "gonfiabili" }); //Altro

            Oggetti_Prodotti = new List<List<List<Item_lista>>>();
            for (int i = 0; i < Prodotti.Count; i++)
            {
                List<List<Item_lista>> temp4 = new List<List<Item_lista>>();
                Oggetti_Prodotti.Add(temp4);
            }
            Oggetti_Carrello = new List<Item_lista>();

            Carrello = new List<Prodotto>();
            Extra_List = new List<Prodotto>();
            Riempi_extra_list(folder);

            Stats_Class = new Statistiche();
            for (int i = 0; i < Prodotti.Count; i++)
            {
                Lista_Categorie x1 = new Lista_Categorie
                {
                    nome = Prodotti[i].nome
                };
                Stats_Class.C.Add(x1);

                for (int i2 = 0; i2 < Prodotti[i].Count(); i2++)
                {
                    Categoria_Stat x2 = new Categoria_Stat
                    {
                        nome = Prodotti[i].get(i2).nome
                    };
                    Stats_Class.C[i].L.Add(x2);
                }
            }

            if (impostazioni.ordine == impostazioni.alfabetico)
                Form2.me.Button7_Click(new object(), new EventArgs()); //ordina per nome i prodotti

            Refresh_pannelli_prodotti_new();
        }

        private static void Riempi_liste2(string folder, int v1, List<string> v2)
        {
            for (int i = 0; i < v2.Count; i++)
            {
                List<string> x1 = null;
                try
                {
                    x1 = File.ReadAllLines(folder + "\\" + Prodotti[v1].nome + "\\" + v2[i] + ".txt", Encoding.ASCII).ToList<string>();
                }
                catch
                {
                    x1 = new List<string>();
                    File.WriteAllText(folder + "\\" + Prodotti[v1].nome + "\\" + v2[i] + ".txt", "");
                }

                Prodotti[v1].L.Add(new SottoCategoria_Prodotto(v2[i]));

                x1.ForEach(x =>
                    {
                        Prodotto x2 = Separa(x);
                        x2.nomecategoria = v2[i].ToUpper();
                        x2.tipo = v1;
                        x2.Sottotipo = i;
                        Prodotti[v1].AddProduct(i, x2);
                    }
                );
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "a6")]
        private static void Refresh_pannelli_prodotti_new()
        {
            const int altezza_prodotto = 80;
            const int rientranza = 5;
            const int larghezza_colonna = 120;
            const int x_prezzo = 20;

            for (int p = 0; p < Prodotti.Count; p++)
            {
                Oggetti_Prodotti[p].Clear();
                for (int p2 = 0; p2 < Prodotti[p].Count(); p2++)
                {
                    try
                    {
                        Pannelli[p][p2].p.Controls.Clear();
                    }
                    catch
                    {
                        continue;
                    }

                    float h2 = Pannelli[p][p2].p.Size.Height / altezza_prodotto;
                    int righe = Funzioni.Arrotonda(h2);
                    float colonne2 = ((float)Prodotti[p].get(p2).Count()) / ((float)righe);
                    int colonne = Funzioni.ArrotondaEccesso(colonne2);
                    if (colonne < 1)
                        colonne = 1;

                    float w2 = Pannelli[p][p2].p.Size.Width / larghezza_colonna;
                    w2 = Funzioni.Arrotonda(w2);
                    if (w2 > colonne)
                    {
                        float righe2 = ((float)Prodotti[p].get(p2).Count()) / ((float)w2);
                        righe = Funzioni.ArrotondaEccesso(righe2);
                        colonne = (int)w2;
                    }

                    int n_prodotti = Prodotti[p].get(p2).Count();
                    for (int i = 1; i <= righe; i++)
                    {
                        int n_prodotti_test = i * colonne;
                        if (n_prodotti_test >= n_prodotti)
                        {
                            righe = i;
                            break;
                        }
                    }

                    float h3 = Pannelli[p][p2].p.Size.Height / altezza_prodotto;
                    h3 = Funzioni.Arrotonda(h3);
                    if (h3 > righe)
                    {
                        righe = (((int)h3) + righe) / 2;
                    }

                    for (int i = 1; i <= colonne; i++)
                    {
                        int n_prodotti_test = i * righe;
                        if (n_prodotti_test >= n_prodotti)
                        {
                            colonne = i;
                            break;
                        }
                    }

                    for (int i = 0; i < righe; i++)
                    {
                        for (int j = 0; j < colonne; j++)
                        {
                            Panel prodotto_x = new Panel
                            {
                                Location = new Point((Pannelli[p][p2].p.Size.Width / colonne) * j + rientranza, altezza_prodotto * i + rientranza),
                                Size = new Size(Pannelli[p][p2].p.Size.Width / colonne - (2 * rientranza), altezza_prodotto - (2 * rientranza)),
                                Parent = Pannelli[p][p2].p
                            };

                            Label prezzo = null;
                            try
                            {
                                prezzo = new Label()
                                {
                                    Size = new Size(prodotto_x.Width, x_prezzo),
                                    Text = Prodotto.Stampa_formato_euro(Prodotti[p].get(p2).Get(i * colonne + j).prezzo),
                                    Location = new Point(0, prodotto_x.Size.Height - x_prezzo),
                                    Visible = true,
                                    Parent = prodotto_x,
                                    BorderStyle = BorderStyle.FixedSingle,
                                    Font = default_font
                                };
                            }
                            catch
                            {
                                break;
                            }
                            Label nome = new Label()
                            {
                                Size = new Size(prodotto_x.Width, prodotto_x.Height - x_prezzo),
                                Text = Prodotti[p].get(p2).Get(i * colonne + j).nome.ToString(),
                                Location = new Point(0, 0),
                                Visible = true,
                                Parent = prodotto_x,
                                BorderStyle = BorderStyle.FixedSingle,
                                Font = default_font
                            };
                            nome.DoubleClick += new EventHandler(Cliccato_prodotto);
                            prezzo.DoubleClick += new EventHandler(Cliccato_prodotto);

                            Item_lista x4 = new Item_lista()
                            {
                                nome = nome,
                                prezzo = prezzo,
                                pannello_contenitore = prodotto_x
                            };

                            try
                            {
                                Oggetti_Prodotti[p][p2].Add(x4);
                            }
                            catch
                            {
                                for (int a5 = 0; a5 <= p2; a5++)
                                {
                                    try
                                    {
                                        int a6 = Oggetti_Prodotti[p][a5].Count;
                                    }
                                    catch
                                    {
                                        Oggetti_Prodotti[p].Add(new List<Item_lista>());
                                    }
                                }
                                Oggetti_Prodotti[p][p2].Add(x4);
                            }
                        }
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void Riempi_extra_list(string folder)
        {
            if (folder[folder.Length - 1] == '\\')
                folder = folder.Remove(folder.Length - 1);

            if (folder[folder.Length - 1] != '\\')
                folder += '\\';

            string[] extra;
            try
            {
                extra = File.ReadAllLines(folder + "Pizze\\extra.txt");
            }
            catch
            {
                File.WriteAllText(folder + "Pizze\\extra.txt", "");
                extra = File.ReadAllLines(folder + "Pizze\\extra.txt");
            }
            for (int i = 0; i < extra.Length; i++)
            {
                string t = extra[i];
                Prodotto t2 = Separa(t);
                Extra_List.Add(t2);
            }

            extra_prodotto.NUMERO_EXTRA = Form2.Extra_List.Count;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        public override void Refresh_pannello_carrello()
        {
            const int x_prezzo = 50;

            bool eccede = false;
            if (Carrello.Count * 20 > Form2.me.panel3.Size.Height)
                eccede = true;

            Form2.me.panel3.Controls.Clear();
            Oggetti_Carrello.Clear();

            int spazio_vertical = 0;
            if (Carrello.Count * 20 > Form2.me.panel3.Size.Height)
                spazio_vertical = spazio_vertical2;

            BigDecimal totale = 0;
            int spazio_bottone = 20;
            int spazio_nome = Form2.me.panel3.Size.Width - x_prezzo - 2;
            for (int i = 0; i < Carrello.Count; i++)
            {
                Label prezzo = new Label()
                {
                    Size = new Size(x_prezzo, 20)
                };
                BigDecimal totale2 = Form2.Carrello[i].prezzo;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].componente_extra[j] > 0)
                        {
                            totale2 += Form2.Extra_List[Carrello[i].componente_extra[j] - 1].prezzo;
                        }
                        else
                        {
                            if (Carrello[i].checkExtraEuro)
                            {
                                totale2 += new BigDecimal(1, 0); //1€
                            }
                        }
                    }
                }
                prezzo.Text = Prodotto.Stampa_formato_euro(totale2);

                prezzo.Location = new Point(Form2.me.panel3.Size.Width - x_prezzo - 2 - spazio_vertical, 20 * i);
                prezzo.Visible = true;
                prezzo.Parent = Form2.me.panel3;
                prezzo.BorderStyle = BorderStyle.FixedSingle;
                prezzo.Font = default_font;

                Label nome = new Label()
                {
                    Size = new Size(spazio_nome - spazio_vertical, 20),
                    Text = Carrello[i].nome.ToString()
                };
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].nome_extra[j].Length != 0)
                            nome.Text += " CON " + Carrello[i].nome_extra[j].ToUpper();
                    }
                    else if (Carrello[i].con_senza[j] == Prodotto.senza)
                    {
                        if (Carrello[i].nome_extra[j].Length != 0)
                            nome.Text += " SENZA " + Carrello[i].nome_extra[j].ToUpper();
                    }
                    else if (Carrello[i].con_senza[j] == Prodotto.altro)
                    {
                        if (Carrello[i].nome_extra[j].Length != 0)
                            nome.Text += " | " + Carrello[i].nome_extra[j].ToUpper();
                    }
                }
                nome.Location = new Point(0, 20 * i);
                nome.Visible = true;
                nome.Parent = Form2.me.panel3;
                nome.BorderStyle = BorderStyle.FixedSingle;
                nome.Font = default_font;

                nome.DoubleClick += new EventHandler(Rimosso_carrello);
                prezzo.DoubleClick += new EventHandler(Rimosso_carrello);

                Button extra_button = new Button()
                {
                    Visible = Carrello[i].tipo == Costanti.Prodotto.pizza,

                    Size = new Size(spazio_bottone - 2, 18),
                    Location = new Point(Form2.me.panel3.Size.Width - spazio_bottone - 2 - spazio_vertical - x_prezzo, 20 * i + 1),
                    Text = "",
                    TextAlign = ContentAlignment.MiddleCenter,
                    Parent = Form2.me.panel3
                };
                extra_button.BringToFront();
                bool modificato = false;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].componente_extra[j] > 0)
                            modificato = true;
                        else if (Carrello[i].checkExtraEuro)
                            modificato = true;
                    }
                    else if (Carrello[i].con_senza[j] == Prodotto.senza)
                    {
                        if (!String.IsNullOrEmpty(Carrello[i].nome_extra[j]))
                            modificato = true;
                    }
                    else if (Carrello[i].con_senza[j] == Prodotto.altro)
                    {
                        if (!String.IsNullOrEmpty(Carrello[i].nome_extra[j]))
                            modificato = true;
                    }
                }
                extra_button.BackColor = modificato ? Color.Blue : nome.BackColor;

                extra_button.Click += new EventHandler(Extra_button_cliccato);
                /*
                panel3.Controls.Add(prezzo);
                panel3.Controls.Add(nome);
                panel3.Controls.Add(extra_button);*/

                Item_lista x4 = new Item_lista()
                {
                    prezzo = prezzo,
                    nome = nome,
                    pulsante = extra_button
                };
                Oggetti_Carrello.Add(x4);

                totale += Carrello[i].prezzo;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].componente_extra[j] > 0)
                            totale += Extra_List[Carrello[i].componente_extra[j] - 1].prezzo;
                    }
                }
            }

            if (checkBox1.Checked)
            {
                totale = Funzioni.PrezzoConSconto(totale, new BigDecimal(1, -1));
                Form2.me.label11.Text = Prodotto.Stampa_formato_euro(totale);
            }
            else
                Form2.me.label11.Text = Prodotto.Stampa_formato_euro(totale);

            totale_da_pagare = totale;

            bool eccede2 = false;
            if (Carrello.Count * 20 > Form2.me.panel3.Size.Height)
                eccede2 = true;

            if (eccede == false && eccede2 == true)
                Refresh_pannello_carrello();
        }

        private static void Extra_button_cliccato(object sender, EventArgs e)
        {
            int i2 = -1;
            for (int i = 0; i < Oggetti_Carrello.Count; i++)
            {
                if (sender == Oggetti_Carrello[i].pulsante)
                {
                    i2 = i;
                    break;
                }
            }

            if (extra_form == null)
            {
                extra_form = new extra_prodotto(i2);
                extra_form.Show();
                if (extra_form != null)
                    extra_form.Focus();
            }
            else if (extra_form.index == i2)
            {
                extra_form.Show();
                if (extra_form != null)
                    extra_form.Focus();
            }
        }

        private static void Rimosso_carrello(object sender, EventArgs e)
        {
            for (int i = 0; i < Carrello.Count; i++)
            {
                if (sender == Oggetti_Carrello[i].nome || sender == Oggetti_Carrello[i].prezzo)
                {
                    Oggetti_Carrello.RemoveAt(i);
                    Carrello.RemoveAt(i);
                    AbstractMode.me_form.Refresh_pannello_carrello();
                    return;
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDecimal(System.String)")]
        private static Prodotto Separa(string t)
        {
            if (t.Length == 0)
            {
                return null;
            }
            Prodotto t2 = new Prodotto();
            int r;
            try
            {
                r = t.IndexOf(' ');
            }
            catch
            {
                return null;
            }

            try
            {
                t2.prezzo = Convert.ToDecimal(t.Substring(0, r));
            }
            catch
            {
                return null;
            }
            t2.nome = t.Substring(r + 1);
            if (t2.nome.Length == 0)
                return null;
            return t2;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Console.WriteLine(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "appdata")]
        public static void Directory_appdata(bool forced = false)
        {
            folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            try
            {
                Directory.CreateDirectory(folder + "\\Oratorio");
            }
            catch
            {
                Console.WriteLine("Errore");
            }

            try
            {
                Directory.CreateDirectory(folder + "\\Oratorio\\Scontrini");
                folder += "\\Oratorio\\Scontrini\\";
            }
            catch
            {
                Console.WriteLine("Errore");
            }

            string[] filePaths = Directory.GetFiles(folder);
            try
            {
                if (filePaths.Length == 0 || forced)
                {
                    File.WriteAllBytes(folder + "Scontrini.zip", ScontriniOratorio.Properties.Resources.Scontrini);
                    Funzioni.TryDeleteDirectory(folder + "\\Pizze", true);
                    Funzioni.TryDeleteDirectory(folder + "\\Self", true);
                    Funzioni.TryDeleteDirectory(folder + "\\Bar", true);

                    Funzioni.ExtractToDirectory(folder + "Scontrini.zip", folder);
                    File.Delete(folder + "Scontrini.zip");
                    return;
                }
            }
            catch
            {
                ;
            }

            try
            {
                Directory.CreateDirectory(folder + "\\Pizze");
            }
            catch
            {
                Console.WriteLine("Errore");
            }
            try
            {
                Directory.CreateDirectory(folder + "\\Self");
            }
            catch
            {
                Console.WriteLine("Errore");
            }
            try
            {
                Directory.CreateDirectory(folder + "\\Bar");
            }
            catch
            {
                Console.WriteLine("Errore");
            }

            if (!Directory.Exists(folder + "\\Prezzi"))
            {
                Directory.CreateDirectory(folder + "\\Prezzi");
                Directory.CreateDirectory(folder + "\\Prezzi\\v1");
                Directory.CreateDirectory(folder + "\\Prezzi\\v1\\Pizze");
                Directory.CreateDirectory(folder + "\\Prezzi\\v1\\Self");
                Directory.CreateDirectory(folder + "\\Prezzi\\v1\\Bar");
                Funzioni.CopiaCartella(folder + "\\Pizze", folder + "\\Prezzi\\v1\\Pizze");
                Funzioni.CopiaCartella(folder + "\\Self", folder + "\\Prezzi\\v1\\Self");
                Funzioni.CopiaCartella(folder + "\\Bar", folder + "\\Prezzi\\v1\\Bar");
            }

            /*
            Funzioni.TryDeleteDirectory(folder + "\\Pizze", true);
            Funzioni.TryDeleteDirectory(folder + "\\Self", true);
            Funzioni.TryDeleteDirectory(folder + "\\Bar", true);
            */
        }

        /*
        private void button4_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Prodotti[0].Count; i++)
                for (int j = 0; j < Prodotti[0].Count - 1; j++)
                {
                    if (Prodotti[0][j].prezzo  > Prodotti[0][j+1].prezzo )
                    {
                        prodotto t = Prodotti[0][j];
                        Prodotti[0][j] = Prodotti[0][j + 1];
                        Prodotti[0][j + 1] = t;
                    }
                }
            refresh_pannello_cibi();
        }
        */
        /*
        private void button5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Prodotti[1].Count; i++)
                for (int j = 0; j < Prodotti[1].Count - 1; j++)
                {
                    if (Prodotti[1][j].prezzo > Prodotti[1][j + 1].prezzo)
                    {
                        prodotto t = Prodotti[1][j];
                        Prodotti[1][j] = Prodotti[1][j + 1];
                        Prodotti[1][j + 1] = t;
                    }
                }
            refresh_pannello_bevande();
        }
        */

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.Compare(System.String,System.String)")]
        private void Button7_Click(object sender, EventArgs e)
        {
            for (int k = 0; k < N_CATEGORIE; k++)
                for (int k2 = 0; k2 < Prodotti[k].Count(); k2++)
                {
                    for (int i = 0; i < Prodotti[k].Count(); i++)
                        for (int j = 0; j < Prodotti[k].Count() - 1; j++)
                        {
                            int x = String.Compare(Prodotti[k].get(k2).Get(j).nome, Prodotti[k].get(k2).Get(j + 1).nome);
                            if (x > 0)
                            {
                                Prodotti[k].get(k2).Scambia(j, j + 1);
                            }
                        }
                }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDecimal(System.String)")]
        private static void Cliccato_prodotto(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            for (int p = 0; p < N_CATEGORIE; p++)
            {
                for (int p2 = 0; p2 < Oggetti_Prodotti[p].Count; p2++)
                {
                    for (int i = 0; i < Oggetti_Prodotti[p][p2].Count; i++)
                    {
                        Label l1 = Oggetti_Prodotti[p][p2][i].prezzo;
                        Label n1 = Oggetti_Prodotti[p][p2][i].nome;
                        if (sender == l1 || sender == n1)
                        {
                            Prodotto t = new Prodotto()
                            {
                                tipo = p,
                                nome = Oggetti_Prodotti[p][p2][i].nome.Text,
                                prezzo = Convert.ToDecimal(Oggetti_Prodotti[p][p2][i].prezzo.Text.Substring(0, Oggetti_Prodotti[p][p2][i].prezzo.Text.Length - 1)),

                                con_senza = new List<int>(),
                                componente_extra = new List<int>(),
                                nome_extra = new List<string>()
                            };
                            for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                            {
                                t.con_senza.Add(Prodotto.niente);
                                t.componente_extra.Add(0);
                                t.nome_extra.Add("");
                            }

                            if (t.con_senza.Count == 0)
                                t.con_senza.Add(Prodotto.con);
                            else
                                t.con_senza[0] = Prodotto.con;

                            t.componente_extra[0] = 0;

                            if (t.con_senza.Count == 1)
                                t.con_senza.Add(Prodotto.senza);
                            else
                                t.con_senza[1] = Prodotto.senza;

                            if (t.con_senza.Count == 2)
                                t.con_senza.Add(Prodotto.altro);
                            else
                                t.con_senza[2] = Prodotto.altro;

                            Aggiungi_A_Carrello(t);
                            return;
                        }
                    }
                }
            }
        }

        private static void Aggiungi_A_Carrello(Prodotto t)
        {
            Carrello.Add(t);
            AbstractMode.me_form.Refresh_pannello_carrello();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Stampa_Scontrino_Click(true, 0);
        }

        /*
        private bool controllo_prezzo()
        {
            string s = textBox1.Text;
            for (int i = 0; i < s.Length; i++)
                if (s[i] == '_' || s[i] == ' ')
                {
                    s = s.Remove(i, 1);
                    s = s.Insert(i, "0");
                }
            for (int i = 0; i < s.Length; i++)
                if (s[i] == '.')
                {
                    s = s.Remove(i, 1);
                    s = s.Insert(i, ",");
                }
            int virgole = 0;
            for (int i = 0; i < s.Length; i++)
                if (s[i] == ',')
                    virgole++;

            if (virgole > 1)
            {
                MessageBox.Show("Inserisci l'importo pagato corretto");
                return false;
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (!((s[i] >= '0' && s[i] <= '9') || s[i] == ','))
                {
                    MessageBox.Show("Inserisci l'importo pagato corretto");
                    return false;
                }
            }

            if (s.Length > 0 && s[s.Length - 1] == ',')
            {
                s = s.Remove(s.Length - 1, 1);
            }
            try
            {
                pagato = (Convert.ToDecimal(s));
            }
            catch
            {
                MessageBox.Show("Inserisci l'importo pagato corretto");
                return false;
            }
            if (pagato < totale_da_pagare)
            {
                MessageBox.Show("L'importo inserito è minore del prezzo! Hai inserito: " + stampa_formato_euro(pagato));
                return false;
            }
            return true;
        }
        */

        /*
    private void Stampalo(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
        ((WebBrowser)sender).ShowPrintDialog();
    }

        */

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "statistiche")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "in")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "carrello")]
        public static void Salva_carrello_in_statistiche()
        {
            for (int i = 0; i < Carrello.Count; i++)
            {
                Prodotto_stat x = new Prodotto_stat()
                {
                    p = Carrello[i],
                    momento = DateTime.Now
                };
                Stats_Class.C[Carrello[i].tipo].L[Carrello[i].Sottotipo].L.Add(x);
            }
        }

        /*
        private void Stampa_scontrino()
        {
            // Create a WebBrowser instance.
            WebBrowser webBrowserForPrinting = new WebBrowser();

            // Add an event handler that prints the document after it loads.
            webBrowserForPrinting.DocumentCompleted +=
                new WebBrowserDocumentCompletedEventHandler(PrintDocument);

            // Set the Url property to load the document.
            webBrowserForPrinting.Url = new Uri(folder+"last_scontrino.html");
        }

            */
        /*
    private void PrintDocument(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
        WebBrowser wb = (WebBrowser)sender;
        // Print the document now that it is fully loaded.
        wb.Print();

        // Dispose the WebBrowser now that the task is complete.
        wb.Dispose();
    }
    */

        private void Form2_SizeChanged(object sender, EventArgs e)
        {
            if (!(this.WindowState == FormWindowState.Maximized || this.WindowState == FormWindowState.Minimized))
                this.WindowState = FormWindowState.Maximized;
        }

        private void Form2_Activated(object sender, EventArgs e)
        {
            if (extra_form != null)
                extra_form.Focus();
            else if (stats != null)
                stats.Focus();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Tabellone tabellone;

        /*
        public static void Stampa_Evento(ref Graphics graphic, ref int offset, int startX, int startY)
        {
            if (Is_evento(DateTime.Now))
            {
                offset += 18;
                Image x = Immagine_evento(DateTime.Now);

                int altezza_immagine = 100;
                if (DateTime.Now.Day == 19 && DateTime.Now.Month == 9 && DateTime.Now.Year == 2015)
                    altezza_immagine = 150;
                altezza_immagine=x.Height;
                Console.WriteLine(altezza_immagine.ToString());
                graphic.DrawImage(x, startX, startY + offset, 262, altezza_immagine);
                offset += altezza_immagine-20;
            }
        }
        */

        /*
        public static Image Immagine_evento(DateTime Now)
        {
            if (Now.Day == 19 && Now.Month == 9 && Now.Year == 2015) //Saluto a Don Omar
                return ScontriniOratorio.Properties.Resources.img_19_09_2015;
            if (Now.Day == 18 && Now.Month == 9 && Now.Year == 2015) //Saluto a Don Omar
                return ScontriniOratorio.Properties.Resources.img_18_09_2015;
            if (Now.Day == 4 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_04_06_2016;
            if (Now.Day == 5 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_05_06_2016;
            if (Now.Day == 10 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_03_06_2016_02;
            if (Now.Day == 11 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_11_06_2016;
            if (Now.Day == 12 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_12_06_2016;

            return null;
        }
        */

        /*
    public static bool Is_evento(DateTime Now)
    {
        Image x = null;
        x = Immagine_evento(Now);
        if (x == null)
            return false;
        return true;
    }
    */

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons)")]
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (chiedi_ancora_uscita)
            {
                DialogResult dialogResult = MessageBox.Show("Sei sicuro di voler uscire? Hai salvato le statistiche su file prima di andare? Se si vuole veramente uscire, cliccare su \"Sì\"", "Attenzione!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Esci_davvero();
                }
                else if (dialogResult == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private bool chiedi_ancora_uscita = true;

        private void Esci_davvero()
        {
            //ultime cose da fare prima di uscire
            Server2.StopListening();

            //uscita
            chiedi_ancora_uscita = false;
            Application.Exit();
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                Stampa_Scontrino_Click(false, 10);
            else
                Stampa_Scontrino_Click(false, 0);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Tabellone_display_form tabellone_form = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        private void ReloadProdottiFiltrati(string p)
        {
            old_lunghezza = textBox1.Text.Length;

            p = p.ToUpper();
            listBox1.Items.Clear();
            List<Prodotto> LP = FindProdotti(p);
            foreach (Prodotto pr in LP)
            {
                //string o = Stampa_formato_euro(pr.prezzo) + "    " + Stats_Class.C[pr.tipo].nome.PadRight(20) + "  "+ pr.nome;

                listBox1.Items.Add(pr);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.IndexOf(System.String)")]
        private static List<Prodotto> FindProdotti(string p)
        {
            List<Prodotto> LP = new List<Prodotto>();
            for (int i = 0; i < Prodotti.Count; i++)
            {
                for (int i2 = 0; i2 < Prodotti[i].Count(); i2++)
                {
                    int n2 = Prodotti[i].get(i2).Count();
                    for (int b1 = 0; b1 < n2; b1++)
                    {
                        Prodotto pr = Prodotti[i].get(i2).Get(b1);

                        int i3 = pr.nome.IndexOf(p);
                        if (i3 >= 0 && i3 < pr.nome.Length)
                            LP.Add(pr);
                    }
                }
            }
            return LP;
        }

        private int old_lunghezza = 0;

        private void TextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Enter:
                    {
                        PremutoInvioListBox();
                        return;
                    }
                case Keys.Up:
                    {
                        if (listBox1.Items.Count > 0)
                        {
                            if (listBox1.SelectedIndex < 0)
                            {
                                listBox1.SelectedIndex = 0;
                                return;
                            }

                            if (listBox1.SelectedIndex > 0)
                            {
                                listBox1.SelectedIndex--;
                            }
                        }
                        return;
                    }
                case Keys.Down:
                    {
                        if (listBox1.Items.Count > 0)
                        {
                            if (listBox1.SelectedIndex < 0)
                            {
                                listBox1.SelectedIndex = 0;
                                return;
                            }

                            if (listBox1.SelectedIndex < listBox1.Items.Count - 1)
                            {
                                listBox1.SelectedIndex++;
                            }
                        }

                        return;
                    }
                case Keys.Delete:
                case Keys.Back:
                    {
                        if (textBox1.Text.Length == 0 && old_lunghezza == 0)
                        {
                            return;
                        }

                        ReloadProdottiFiltrati(textBox1.Text);
                        return;
                    }
                default:
                    {
                        //textBox1.Text = textBox1.Text.ToUpper();
                        ReloadProdottiFiltrati(textBox1.Text);
                        return;
                    }
            }
        }

        private void PremutoInvioListBox()
        {
            if (listBox1.SelectedIndex >= 0)
            {
                Prodotto tp = (Prodotto)listBox1.Items[listBox1.SelectedIndex];
                tp.con_senza = new List<int>();
                for (int i = 0; i < extra_prodotto.NUMERO_EXTRA; i++)
                {
                    tp.con_senza.Add(Prodotto.niente);
                }
                Aggiungi_A_Carrello(tp);
            }
        }

        private void ListBox1_DoubleClick(object sender, EventArgs e)
        {
            PremutoInvioListBox();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            Refresh_pannello_carrello();
        }

        public override void Button2_Click(object sender, EventArgs e)
        {
            Funzioni.SvuotaCarrello();
        }
    }
}