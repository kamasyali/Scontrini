﻿using Common;
using ScontriniOratorio;
using ScontriniOratorio.Classi.ClassiComune.Prodotti;
using ScontriniOratorio.Classi.ClassiComune.Stat;
using ScontriniOratorio.Forms;
using ScontriniOratorio.Forms.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratori
{
    public partial class Form1 : AbstractMode
    {
        public Form1()
        {
            InitializeComponent();
        }

        //VARIABILI E COSTANTI UTILI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static impostazioni imp = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "x")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "prezzo")]
        public const int x_prezzo = 50;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "vertical")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "spazio")]
        public const int spazio_vertical2 = 17;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static int primo_schermo = 0;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool print_ready = false;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static string inte = "intestazione.txt";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static string ordine = "ordine.txt";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static string seconda = "seconda.txt";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Font default_font = new Font(FontFamily.GenericSansSerif, 7f);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static bool dispari_evento = true;

        //LISTE DI PRODOTTI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static List<List<List<Prodotto>>> Prodotti;

        //LISTA STATISTICHE
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]

        //LISTE DI OGGETTI GRAFICI
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static List<List<Item_lista>> Oggetti_Prodotti;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public static List<Item_lista> Oggetti_Carrello;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static List<List<Pannello_intestato>> Pannelli;

        //GENIALATA
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Form1 me;

        //FUNZIONI
        private void Button6_Click(object sender, EventArgs e)
        {
            if (imp == null)
                imp = new impostazioni();
            imp.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            me = this;
            AbstractMode.me_form = this;
            Variabili.MainMode = 1;

            this.WindowState = FormWindowState.Maximized;
            this.MinimumSize = this.Size;

            //TestBigDecimal();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        public static void TestBigDecimal()
        {
            string s = "";
            for (decimal i = 0; i < 1000; i++)
            {
                if (true)
                {
                    BigDecimal b = i + (((i % 100) / 100));
                    s += Prodotto.Stampa_formato_euro(b);
                    s += '\n';
                }
            }
            MessageBox.Show(s);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "iniziali")]
        public static void Cose_iniziali()
        {
            for (int i = 0; i < 3; i++)
            {
                Form1.me.tabControl1.TabPages[i].Controls.Clear();
            }
            Size s = me.tabControl1.TabPages[0].Size;
            Variabili.Tabs = Form1.me.tabControl1;
            Form1.Pannelli = new List<List<Pannello_intestato>>();
            Variabili.Pannelli = Form1.Pannelli;
            Funzioni.Prepara_pannelli(s);
            Riempi_liste(folder + "\\Prezzi\\v1");

            Elimina_zip();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void Elimina_zip()
        {
            try
            {
                File.Delete(folder + "Scontrini.zip");
            }
            catch
            {
                ;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void Riempi_liste(string folder)
        {
            string[][] letto = new string[N_CATEGORIE][];
            for (int i = 0; i < N_CATEGORIE; i++)
            {
                try
                {
                    letto[i] = File.ReadAllLines(folder + Form1.me.file_read[i], Encoding.ASCII);
                }
                catch
                {
                    File.WriteAllText(folder + Form1.me.file_read[i], "");
                    letto[i] = File.ReadAllLines(folder + Form1.me.file_read[i]);
                }

                if (i == 2)
                {
                    ;
                }
            }

            Prodotti = new List<List<List<Prodotto>>>();
            for (int i = 0; i < N_CATEGORIE; i++)
            {
                List<Prodotto> temp = new List<Prodotto>();
                Prodotti.Add(new List<List<Prodotto>>());
                Prodotti[i].Add(temp);
            }

            for (int i = 0; i < N_CATEGORIE; i++)
            {
                for (int j = 0; j < letto[i].Length; j++)
                {
                    string t = letto[i][j];
                    Prodotto t2 = Separa(t);
                    if (t2 != null)
                    {
                        t2.tipo = i;
                        Prodotti[i][0].Add(t2);
                    }
                }
            }
            Oggetti_Prodotti = new List<List<Item_lista>>();
            for (int i = 0; i < N_CATEGORIE; i++)
            {
                List<Item_lista> temp4 = new List<Item_lista>();
                Oggetti_Prodotti.Add(temp4);
            }
            Oggetti_Carrello = new List<Item_lista>();

            Carrello = new List<Prodotto>();
            Extra_List = new List<Prodotto>();
            Riempi_extra_list(folder);

            Stats_Class = new Statistiche();
            for (int i = 0; i < N_CATEGORIE; i++)
            {
                Categoria_Stat temp5 = new Categoria_Stat();
                Stats_Class.C.Add(new Lista_Categorie());
                Stats_Class.C[i].L.Add(temp5);
            }
            Stats_Class.C[0].L[0].nome = "Pizze";
            Stats_Class.C[1].L[0].nome = "Bevande";
            Stats_Class.C[2].L[0].nome = "Altro";
            Stats_Class.C[3].L[0].nome = "Coperto";
            Stats_Class.C[4].L[0].nome = "Primi piatti";
            Stats_Class.C[5].L[0].nome = "Secondi piatti";
            Stats_Class.C[6].L[0].nome = "Secondi freddi";
            Stats_Class.C[7].L[0].nome = "Contorno";
            Stats_Class.C[8].L[0].nome = "Dolce";
            Stats_Class.C[9].L[0].nome = "Panini";
            Stats_Class.C[10].L[0].nome = "Bevande";
            Stats_Class.C[11].L[0].nome = "Caffetteria";
            Stats_Class.C[12].L[0].nome = "Bibite";
            Stats_Class.C[13].L[0].nome = "Dolciumi";
            Stats_Class.C[14].L[0].nome = "Snack e salati";
            Stats_Class.C[15].L[0].nome = "Alcolici";
            Stats_Class.C[16].L[0].nome = "Gelati";
            Stats_Class.C[17].L[0].nome = "Altro";
            Stats_Class.C[18].L[0].nome = "Altro";

            if (impostazioni.ordine == impostazioni.alfabetico)
                Form1.me.Button7_Click(new object(), new EventArgs()); //ordina per nome i prodotti

            Refresh_pannelli_prodotti();

            for (int i = 0; i < Prodotti.Count; i++)
            {
                for (int j = 0; j < Prodotti[i].Count; j++)
                {
                    try
                    {
                        string s3 = Stats_Class.C[Prodotti[i][0][j].tipo].L[Prodotti[i][0][j].Sottotipo].nome;
                        if (String.IsNullOrEmpty(s3))
                        {
                            ;
                        }
                        Prodotti[i][0][j].nomecategoria = s3;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void Riempi_extra_list(string folder)
        {
            if (folder[folder.Length - 1] != '\\')
                folder += '\\';

            string[] extra;
            try
            {
                extra = File.ReadAllLines(folder + "Pizze\\extra.txt");
            }
            catch
            {
                File.WriteAllText(folder + "Pizze\\extra.txt", "");
                extra = File.ReadAllLines(folder + "Pizze\\extra.txt");
            }
            for (int i = 0; i < extra.Length; i++)
            {
                string t = extra[i];
                Prodotto t2 = Separa(t);
                Extra_List.Add(t2);
            }

            extra_prodotto.NUMERO_EXTRA = Form1.Extra_List.Count;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        public override void Refresh_pannello_carrello()
        {
            bool eccede = false;
            if (Carrello.Count * 20 > Form1.me.panel3.Size.Height)
                eccede = true;

            Form1.me.panel3.Controls.Clear();
            Oggetti_Carrello.Clear();

            int spazio_vertical = 0;
            if (Carrello.Count * 20 > Form1.me.panel3.Size.Height)
                spazio_vertical = spazio_vertical2;

            BigDecimal totale = 0;
            int spazio_bottone = 20;
            int spazio_nome = Form1.me.panel3.Size.Width - x_prezzo - 2;
            for (int i = 0; i < Carrello.Count; i++)
            {
                Label prezzo = new Label()
                {
                    Size = new Size(x_prezzo, 20)
                };
                BigDecimal totale2 = Form1.Carrello[i].prezzo;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].componente_extra[j] > 0)
                        {
                            totale2 += Form1.Extra_List[Carrello[i].componente_extra[j] - 1].prezzo;
                        }
                    }
                }
                prezzo.Text = Prodotto.Stampa_formato_euro(totale2);

                prezzo.Location = new Point(Form1.me.panel3.Size.Width - x_prezzo - 2 - spazio_vertical, 20 * i);
                prezzo.Visible = true;
                prezzo.Parent = Form1.me.panel3;
                prezzo.BorderStyle = BorderStyle.FixedSingle;
                prezzo.Font = default_font;

                Label nome = new Label()
                {
                    Size = new Size(spazio_nome - spazio_vertical, 20),
                    Text = Carrello[i].nome.ToString()
                };
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                        if (Carrello[i].nome_extra[j].Length != 0)
                            nome.Text += " CON " + Carrello[i].nome_extra[j];
                        else if (Carrello[i].con_senza[j] == Prodotto.senza)
                            if (Carrello[i].nome_extra[j].Length != 0)
                                nome.Text += " SENZA " + Carrello[i].nome_extra[j];
                            else if (Carrello[i].con_senza[j] == Prodotto.altro)
                                if (Carrello[i].nome_extra[j].Length != 0)
                                    nome.Text += " | " + Carrello[i].nome_extra[j];
                }
                nome.Location = new Point(0, 20 * i);
                nome.Visible = true;
                nome.Parent = Form1.me.panel3;
                nome.BorderStyle = BorderStyle.FixedSingle;
                nome.Font = default_font;

                nome.DoubleClick += new EventHandler(Rimosso_carrello);
                prezzo.DoubleClick += new EventHandler(Rimosso_carrello);

                Button extra_button = new Button()
                {
                    Visible = Carrello[i].tipo == Costanti.Prodotto.pizza,

                    Size = new Size(spazio_bottone - 2, 18),
                    Location = new Point(Form1.me.panel3.Size.Width - spazio_bottone - 2 - spazio_vertical - x_prezzo, 20 * i + 1),
                    Text = "",
                    TextAlign = ContentAlignment.MiddleCenter,
                    Parent = Form1.me.panel3
                };
                extra_button.BringToFront();
                bool modificato = false;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].componente_extra[j] > 0)
                            modificato = true;
                    }
                    else if (Carrello[i].con_senza[j] == Prodotto.senza)
                    {
                        if (!String.IsNullOrEmpty(Carrello[i].nome_extra[j]))
                            modificato = true;
                    }
                    else if (Carrello[i].con_senza[j] == Prodotto.altro)
                    {
                        if (!String.IsNullOrEmpty(Carrello[i].nome_extra[j]))
                            modificato = true;
                    }
                }
                extra_button.BackColor = modificato ? Color.Blue : nome.BackColor;

                extra_button.Click += new EventHandler(Extra_button_cliccato);
                /*
                panel3.Controls.Add(prezzo);
                panel3.Controls.Add(nome);
                panel3.Controls.Add(extra_button);*/

                Item_lista x4 = new Item_lista()
                {
                    prezzo = prezzo,
                    nome = nome,
                    pulsante = extra_button
                };
                Oggetti_Carrello.Add(x4);

                totale += Carrello[i].prezzo;
                for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
                {
                    if (Carrello[i].con_senza[j] == Prodotto.con)
                    {
                        if (Carrello[i].componente_extra[j] > 0)
                            totale += Extra_List[Carrello[i].componente_extra[j] - 1].prezzo;
                    }
                }
            }
            Form1.me.label11.Text = Prodotto.Stampa_formato_euro(totale);
            totale_da_pagare = totale;

            bool eccede2 = false;
            if (Carrello.Count * 20 > Form1.me.panel3.Size.Height)
                eccede2 = true;

            if (eccede == false && eccede2 == true)
                Refresh_pannello_carrello();
        }

        private static void Extra_button_cliccato(object sender, EventArgs e)
        {
            int i2 = -1;
            for (int i = 0; i < Oggetti_Carrello.Count; i++)
            {
                if (sender == Oggetti_Carrello[i].pulsante)
                {
                    i2 = i;
                    break;
                }
            }

            if (extra_form == null)
            {
                extra_form = new extra_prodotto(i2);
                extra_form.Show();
                if (extra_form != null)
                    extra_form.Focus();
            }
            else if (extra_form.index == i2)
            {
                extra_form.Show();
                if (extra_form != null)
                    extra_form.Focus();
            }
        }

        private static void Rimosso_carrello(object sender, EventArgs e)
        {
            for (int i = 0; i < Carrello.Count; i++)
            {
                if (sender == Oggetti_Carrello[i].nome || sender == Oggetti_Carrello[i].prezzo)
                {
                    Oggetti_Carrello.RemoveAt(i);
                    Carrello.RemoveAt(i);
                    AbstractMode.me_form.Refresh_pannello_carrello();
                    return;
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDecimal(System.String)")]
        private static Prodotto Separa(string t)
        {
            if (t.Length == 0)
            {
                return null;
            }
            Prodotto t2 = new Prodotto();
            int r;
            try
            {
                r = t.IndexOf(' ');
            }
            catch
            {
                return null;
            }

            try
            {
                t2.prezzo = Convert.ToDecimal(t.Substring(0, r));
            }
            catch
            {
                return null;
            }
            t2.nome = t.Substring(r + 1);
            if (t2.nome.Length == 0)
                return null;
            return t2;
        }

        /*
        private void button4_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Prodotti[0].Count; i++)
                for (int j = 0; j < Prodotti[0].Count - 1; j++)
                {
                    if (Prodotti[0][j].prezzo  > Prodotti[0][j+1].prezzo )
                    {
                        prodotto t = Prodotti[0][j];
                        Prodotti[0][j] = Prodotti[0][j + 1];
                        Prodotti[0][j + 1] = t;
                    }
                }
            refresh_pannello_cibi();
        }
        */
        /*
        private void button5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Prodotti[1].Count; i++)
                for (int j = 0; j < Prodotti[1].Count - 1; j++)
                {
                    if (Prodotti[1][j].prezzo > Prodotti[1][j + 1].prezzo)
                    {
                        prodotto t = Prodotti[1][j];
                        Prodotti[1][j] = Prodotti[1][j + 1];
                        Prodotti[1][j + 1] = t;
                    }
                }
            refresh_pannello_bevande();
        }
        */

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.Compare(System.String,System.String)")]
        private void Button7_Click(object sender, EventArgs e)
        {
            for (int k = 0; k < N_CATEGORIE; k++)
                for (int i = 0; i < Prodotti[k].Count; i++)
                    for (int j = 0; j < Prodotti[k].Count - 1; j++)
                    {
                        int x = String.Compare(Prodotti[k][0][j].nome, Prodotti[k][0][j + 1].nome);
                        if (x > 0)
                        {
                            Prodotto t = Prodotti[k][0][j];
                            Prodotti[k][0][j] = Prodotti[k][0][j + 1];
                            Prodotti[k][0][j + 1] = t;
                        }
                    }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "prodotti")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "pannelli")]
        public static void Refresh_pannelli_prodotti()
        {
            const int altezza_prodotto = 17;
            for (int p = 0; p < N_CATEGORIE; p++)
            {
                for (int p2 = 0; p2 < Prodotti[p].Count; p2++)
                {
                    Oggetti_Prodotti[p].Clear();
                    Pannelli[p][0].p.Controls.Clear();

                    int spazio_vertical = 0;
                    if (Prodotti[p][p2].Count * altezza_prodotto > Pannelli[p][0].p.Size.Height)
                        spazio_vertical = spazio_vertical2;

                    for (int i = 0; i < Prodotti[p][p2].Count; i++)
                    {
                        Label prezzo = new Label()
                        {
                            Size = new Size(x_prezzo, altezza_prodotto),
                            Text = Prodotto.Stampa_formato_euro(Prodotti[p][p2][i].prezzo),
                            Location = new Point(Pannelli[p][0].p.Size.Width - x_prezzo - spazio_vertical - 2, altezza_prodotto * i),
                            Visible = true,
                            Parent = Pannelli[p][0].p,
                            BorderStyle = BorderStyle.FixedSingle,
                            Font = default_font
                        };
                        Label nome = new Label()
                        {
                            Size = new Size(Pannelli[p][0].p.Size.Width - x_prezzo, altezza_prodotto),
                            Text = Prodotti[p][p2][i].nome.ToString(),
                            Location = new Point(0, altezza_prodotto * i),
                            Visible = true,
                            Parent = Pannelli[p][0].p,
                            BorderStyle = BorderStyle.FixedSingle,
                            Font = default_font
                        };
                        nome.DoubleClick += new EventHandler(Cliccato_prodotto);
                        prezzo.DoubleClick += new EventHandler(Cliccato_prodotto);

                        Item_lista x4 = new Item_lista()
                        {
                            nome = nome,
                            prezzo = prezzo
                        };
                        Oggetti_Prodotti[p].Add(x4);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToDecimal(System.String)")]
        private static void Cliccato_prodotto(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            for (int p = 0; p < N_CATEGORIE; p++)
            {
                for (int i = 0; i < Oggetti_Prodotti[p].Count; i++)
                {
                    if (sender == Oggetti_Prodotti[p][i].prezzo || sender == Oggetti_Prodotti[p][i].nome)
                    {
                        Prodotto t = new Prodotto()
                        {
                            tipo = p,
                            nome = Oggetti_Prodotti[p][i].nome.Text,
                            prezzo = Convert.ToDecimal(Oggetti_Prodotti[p][i].prezzo.Text.Substring(0, Oggetti_Prodotti[p][i].prezzo.Text.Length - 1)),
                        };

                        Aggiungi_A_Carrello(ref t);
                        return;
                    }
                }
            }
        }

        private static void Aggiungi_A_Carrello(ref Prodotto t)
        {
            t.con_senza = new List<int>();
            t.componente_extra = new List<int>();
            t.nome_extra = new List<string>();

            for (int j = 0; j < extra_prodotto.NUMERO_EXTRA; j++)
            {
                t.con_senza.Add(Prodotto.niente);
                t.componente_extra.Add(0);
                t.nome_extra.Add("");
            }

            t.con_senza[0] = Prodotto.con;
            t.componente_extra[0] = 0;
            t.con_senza[1] = Prodotto.senza;
            t.con_senza[2] = Prodotto.altro;

            Carrello.Add(t);
            AbstractMode.me_form.Refresh_pannello_carrello();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Stampa_Scontrino_Click(true, 0);
        }

        /*
        private bool controllo_prezzo()
        {
            string s = textBox1.Text;
            for (int i = 0; i < s.Length; i++)
                if (s[i] == '_' || s[i] == ' ')
                {
                    s = s.Remove(i, 1);
                    s = s.Insert(i, "0");
                }
            for (int i = 0; i < s.Length; i++)
                if (s[i] == '.')
                {
                    s = s.Remove(i, 1);
                    s = s.Insert(i, ",");
                }
            int virgole = 0;
            for (int i = 0; i < s.Length; i++)
                if (s[i] == ',')
                    virgole++;

            if (virgole > 1)
            {
                MessageBox.Show("Inserisci l'importo pagato corretto");
                return false;
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (!((s[i] >= '0' && s[i] <= '9') || s[i] == ','))
                {
                    MessageBox.Show("Inserisci l'importo pagato corretto");
                    return false;
                }
            }

            if (s.Length > 0 && s[s.Length - 1] == ',')
            {
                s = s.Remove(s.Length - 1, 1);
            }
            try
            {
                pagato = (Convert.ToDecimal(s));
            }
            catch
            {
                MessageBox.Show("Inserisci l'importo pagato corretto");
                return false;
            }
            if (pagato < totale_da_pagare)
            {
                MessageBox.Show("L'importo inserito è minore del prezzo! Hai inserito: " + stampa_formato_euro(pagato));
                return false;
            }
            return true;
        }
        */

        /*
    private void Stampalo(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
        ((WebBrowser)sender).ShowPrintDialog();
    }

        */

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "statistiche")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "in")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "carrello")]
        public static void Salva_carrello_in_statistiche()
        {
            for (int i = 0; i < Carrello.Count; i++)
            {
                Prodotto_stat x = new Prodotto_stat()
                {
                    p = Carrello[i],
                    momento = DateTime.Now
                };
                Stats_Class.C[Carrello[i].tipo].L[Carrello[i].Sottotipo].L.Add(x);
            }
        }

        /*
        private void Stampa_scontrino()
        {
            // Create a WebBrowser instance.
            WebBrowser webBrowserForPrinting = new WebBrowser();

            // Add an event handler that prints the document after it loads.
            webBrowserForPrinting.DocumentCompleted +=
                new WebBrowserDocumentCompletedEventHandler(PrintDocument);

            // Set the Url property to load the document.
            webBrowserForPrinting.Url = new Uri(folder+"last_scontrino.html");
        }

            */
        /*
    private void PrintDocument(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
        WebBrowser wb = (WebBrowser)sender;
        // Print the document now that it is fully loaded.
        wb.Print();

        // Dispose the WebBrowser now that the task is complete.
        wb.Dispose();
    }
    */

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (!(this.WindowState == FormWindowState.Maximized || this.WindowState == FormWindowState.Minimized))
                this.WindowState = FormWindowState.Maximized;
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (extra_form != null)
                extra_form.Focus();
            else if (stats != null)
                stats.Focus();
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            if (stats == null)
            {
                stats = new Statistiche_form(1);
            }
            stats.Show();
            stats.Focus();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Tabellone tabellone;

        /*
        public static void Stampa_Evento(ref Graphics graphic, ref int offset, int startX, int startY)
        {
            if (Is_evento(DateTime.Now))
            {
                offset += 18;
                Image x = Immagine_evento(DateTime.Now);

                int altezza_immagine = 100;
                if (DateTime.Now.Day == 19 && DateTime.Now.Month == 9 && DateTime.Now.Year == 2015)
                    altezza_immagine = 150;
                altezza_immagine=x.Height;
                Console.WriteLine(altezza_immagine.ToString());
                graphic.DrawImage(x, startX, startY + offset, 262, altezza_immagine);
                offset += altezza_immagine-20;
            }
        }
        */

        /*
        public static Image Immagine_evento(DateTime Now)
        {
            if (Now.Day == 19 && Now.Month == 9 && Now.Year == 2015) //Saluto a Don Omar
                return ScontriniOratorio.Properties.Resources.img_19_09_2015;
            if (Now.Day == 18 && Now.Month == 9 && Now.Year == 2015) //Saluto a Don Omar
                return ScontriniOratorio.Properties.Resources.img_18_09_2015;
            if (Now.Day == 4 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_04_06_2016;
            if (Now.Day == 5 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_05_06_2016;
            if (Now.Day == 10 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_03_06_2016_02;
            if (Now.Day == 11 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_11_06_2016;
            if (Now.Day == 12 && Now.Month == 6 && Now.Year == 2016)
                return ScontriniOratorio.Properties.Resources.img_12_06_2016;

            return null;
        }
        */

        /*
    public static bool Is_evento(DateTime Now)
    {
        Image x = null;
        x = Immagine_evento(Now);
        if (x == null)
            return false;
        return true;
    }
    */

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Non passare valori letterali come parametri localizzati", MessageId = "System.Windows.Forms.MessageBox.Show(System.String,System.String,System.Windows.Forms.MessageBoxButtons)")]
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (chiedi_ancora_uscita)
            {
                DialogResult dialogResult = MessageBox.Show("Sei sicuro di voler uscire? Hai salvato le statistiche su file prima di andare? Se si vuole veramente uscire, cliccare su \"Sì\"", "Attenzione!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Esci_davvero();
                }
                else if (dialogResult == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private bool chiedi_ancora_uscita = true;

        private void Esci_davvero()
        {
            //ultime cose da fare prima di uscire
            Server2.StopListening();

            //uscita
            chiedi_ancora_uscita = false;
            Application.Exit();
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            Stampa_Scontrino_Click(false, 0);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button4_Click(object sender, EventArgs e)
        {
            giochi_invernali_2015 x = new giochi_invernali_2015();
            x.ShowDialog();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Tabellone_display_form tabellone_form = null;

        private void Button5_Click(object sender, EventArgs e)
        {
            tabellone_form = new Tabellone_display_form();
            tabellone_form.ShowDialog();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button7_Click_1(object sender, EventArgs e)
        {
            Cre_Form x = new Cre_Form();
            x.Show();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button8_Click(object sender, EventArgs e)
        {
            Form_Test_BigDecimal x = new Form_Test_BigDecimal();
            x.Show();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button10_Click(object sender, EventArgs e)
        {
            ConvertiStatistiche x = new ConvertiStatistiche();
            x.Show();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToUpper")]
        private void ReloadProdottiFiltrati(string p)
        {
            const int max_result = 50;

            old_lunghezza = textBox1.Text.Length;

            p = p.ToUpper();
            listBox1.Items.Clear();
            List<Prodotto> LP = FindProdotti(p);

            for (int i = 0; i < LP.Count && i < max_result; i++)
            {
                listBox1.Items.Add(LP[i]);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.IndexOf(System.String)")]
        private static List<Prodotto> FindProdotti(string p)
        {
            List<Prodotto> LP = new List<Prodotto>();
            for (int i = 0; i < N_CATEGORIE; i++)
            {
                for (int j = 0; j < Prodotti[i].Count; j++)
                {
                    foreach (Prodotto pr in Prodotti[i][j])
                    {
                        int i2 = pr.nome.IndexOf(p);
                        if (i2 >= 0 && i2 < pr.nome.Length)
                        {
                            if (String.IsNullOrEmpty(pr.nomecategoria))
                            {
                                pr.nomecategoria = Stats_Class.C[i].L[j].nome;
                            }
                            LP.Add(pr);
                        }
                    }
                }
            }
            return LP;
        }

        private int old_lunghezza = 0;

        private void TextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Enter:
                    {
                        PremutoInvioListBox();
                        return;
                    }
                case Keys.Up:
                    {
                        if (listBox1.Items.Count > 0)
                        {
                            if (listBox1.SelectedIndex < 0)
                            {
                                listBox1.SelectedIndex = 0;
                                return;
                            }

                            if (listBox1.SelectedIndex > 0)
                            {
                                listBox1.SelectedIndex--;
                            }
                        }
                        return;
                    }
                case Keys.Down:
                    {
                        if (listBox1.Items.Count > 0)
                        {
                            if (listBox1.SelectedIndex < 0)
                            {
                                listBox1.SelectedIndex = 0;
                                return;
                            }

                            if (listBox1.SelectedIndex < listBox1.Items.Count - 1)
                            {
                                listBox1.SelectedIndex++;
                            }
                        }

                        return;
                    }
                case Keys.Delete:
                case Keys.Back:
                    {
                        if (textBox1.Text.Length == 0 && old_lunghezza == 0)
                        {
                            return;
                        }

                        ReloadProdottiFiltrati(textBox1.Text);
                        return;
                    }
                default:
                    {
                        //textBox1.Text = textBox1.Text.ToUpper();
                        ReloadProdottiFiltrati(textBox1.Text);
                        return;
                    }
            }
        }

        private void PremutoInvioListBox()
        {
            if (listBox1.SelectedIndex >= 0)
            {
                Prodotto tp = (Prodotto)listBox1.Items[listBox1.SelectedIndex];
                Aggiungi_A_Carrello(ref tp);
            }
        }

        private void ListBox1_DoubleClick(object sender, EventArgs e)
        {
            PremutoInvioListBox();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            fontHeightp = fontp.GetHeight();
            Cose_iniziali();
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            Funzioni.SvuotaCarrello();
        }
    }
}