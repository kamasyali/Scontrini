﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio.Forms
{
    public partial class ConvertiStatistiche : Form
    {
        public ConvertiStatistiche()
        {
            InitializeComponent();
        }

        Statistiche s = null;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Elimina gli oggetti prima che siano esterni all'ambito")]
        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog x = new OpenFileDialog();
            var r = x.ShowDialog();
            if (r == DialogResult.OK)
            {
                s = new Statistiche();
                s.Carica(x.FileName);
                s.Salva();
            }
        }

        
    }
}
