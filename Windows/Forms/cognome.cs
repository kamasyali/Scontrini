﻿using ScontriniOratori;
using ScontriniOratorio.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScontriniOratorio
{
    public partial class Cognome : Form
    {
        public Cognome()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Variabili.Annullato_cognome = false;
            Form1.cognome= textBox1.Text;
            this.Close();
        }

        private void Cognome_Load(object sender, EventArgs e)
        {
            Variabili.Annullato_cognome = true;
        }

        private void Cognome_Shown(object sender, EventArgs e)
        {
            textBox1.Focus();
        }

        private void Cognome_FormClosing(object sender, FormClosingEventArgs e)
        {
            ;
        }
    }
}
