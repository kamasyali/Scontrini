﻿using ScontriniOratori;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScontriniClassi.Classi
{
    public class Categoria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<Prodotto_stat> L;
        public string nome;

        public Categoria()
        {
            L = new List<Prodotto_stat>();
            nome = "";
        }
    }
    public class Prodotto_stat
    {
        public Prodotto p;
        public DateTime momento;

        public Prodotto_stat()
        {
            p = new Prodotto();
        }
    }
}
