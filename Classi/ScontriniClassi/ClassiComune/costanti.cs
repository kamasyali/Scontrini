﻿namespace ScontriniOratorio
{
    public static class Costanti
    {
        public static class Socket
        {
            public const string fine_socket = "x-<EOF>-x";
            public const string conferma_ricevuta_socket = "x-<RICEVUTO>-x";
        }

        public static class Prodotto
        {
            public const int pizza = 0;
            public const int bevanda = 1;
            public const int altro_pizze = 2;            
        }

        public const int N_MACROCATEGORIE = 8;
    }
}