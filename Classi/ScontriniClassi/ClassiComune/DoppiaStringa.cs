﻿using System.Collections.Generic;

namespace Common
{
    public struct DoppiaStringa
    {
        public string p_intera; //parte intera
        public string p_decimale; //parte decimale

        public override bool Equals(object obj)
        {
            if (!(obj is DoppiaStringa))
            {
                return false;
            }

            var stringa = (DoppiaStringa)obj;
            return p_intera == stringa.p_intera &&
                   p_decimale == stringa.p_decimale;
        }

        public override int GetHashCode()
        {
            var hashCode = 1253303615;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(p_intera);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(p_decimale);
            return hashCode;
        }

        public static bool operator == (DoppiaStringa a, DoppiaStringa b)
        {
            return a.Equals(b);
        }

        public static bool operator != (DoppiaStringa a, DoppiaStringa b)
        {
            return !a.Equals(b);
        }
    }
}