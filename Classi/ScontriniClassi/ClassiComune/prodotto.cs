﻿using Common;
using ScontriniClassi.Classi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScontriniOratori
{
    public class Prodotto
    {

        public Common.BigDecimal prezzo;
        public string nome;
        public int tipo;
        public int Sottotipo { get; set; }
        public int quantita = 1;
        public bool old = false;

        public string nomecategoria;
       
        public const int con = 1;
        public const int senza = 2;
        public const int niente = 3;
        public const int altro = 4;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<int> con_senza;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> nome_extra;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<int> componente_extra;

        public override string ToString()
        {
            return Stampa_formato_euro(prezzo).PadRight(15) + nomecategoria.PadRight(20) + nome;
        }

        public static string Stampa_formato_euro(BigDecimal prezzo, char terminatore = '€')
        {
            const char separatore = ',';
            var s = prezzo.ToString2();
            return s.p_intera.PadLeft(3, ' ') + separatore + s.p_decimale.PadRight(2, '0').Substring(0, 2) + terminatore;
            /*
            int n = s.IndexOf(separatore);
            string fin;
            if (n < 0)
            {
                fin = s + ",00";
            }
            else
            {
                string s2 = s.Substring(n + 1);
                if (s2.Length == 0)
                {
                    s2 = ",00";
                }
                else if (s2.Length == 1)
                {
                    s2 = "," + s2 + "0";
                }
                else
                {
                    s2 = "," + s2;
                }
                fin = s.Substring(0, n) + s2;
            }
            n= fin.IndexOf(',');
            return fin.Substring(0, n)+","+fin.Substring(n+1,2)+"€";
            */
        }
    }
}
