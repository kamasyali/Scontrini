﻿using System;
using System.Numerics;

namespace Common
{
    /// <summary>
    /// Arbitrary precision decimal.
    /// All operations are exact, except for division. Division never determines more digits than the given precision.
    /// Source: https://gist.github.com/JcBernack/0b4eef59ca97ee931a2f45542b9ff06d
    /// Based on https://stackoverflow.com/a/4524254
    /// Author: Jan Christoph Bernack (contact: jc.bernack at gmail.com)
    /// License: public domain
    /// </summary>
    public struct BigDecimal
        : IComparable
        , IComparable<BigDecimal>
    {
        /// <summary>
        /// Specifies whether the significant digits should be truncated to the given precision after each operation.
        /// </summary>
        public static bool AlwaysTruncate = false;

        /// <summary>
        /// Sets the maximum precision of division operations.
        /// If AlwaysTruncate is set to true all operations are affected.
        /// </summary>
        public static int Precision = 50;

        public BigInteger Mantissa_BigInt { get; set; }

        public void Mantissa_BigToString()
        {
            Mantissa_String = Mantissa_BigInt.ToString();
        }

        public void Mantissa_StringToBig()
        {
            if (Mantissa_String==null)
            {
                //todo Bisognerebbe andare a cercare il prodotto nel catalogo per scoprirne il prezzo
                return;
            }
            Mantissa_BigInt = BigInteger.Parse(Mantissa_String);
        }

        public string Mantissa_String { get; set; }
        public int Exponent { get; set; }

        public BigDecimal(BigInteger mantissa, int exponent)
            : this()
        {
            Mantissa_BigInt = mantissa;
            Exponent = exponent;
            Normalize();
            if (AlwaysTruncate)
            {
                Truncate();
            }
        }

        /// <summary>
        /// Removes trailing zeros on the mantissa
        /// </summary>
        public void Normalize()
        {
            if (Mantissa_BigInt.IsZero)
            {
                Exponent = 0;
            }
            else
            {
                BigInteger remainder = 0;
                while (remainder == 0)
                {
                    var shortened = BigInteger.DivRem(Mantissa_BigInt, 10, out remainder);
                    if (remainder == 0)
                    {
                        Mantissa_BigInt = shortened;
                        Exponent++;
                    }
                }
            }
        }

        /// <summary>
        /// Truncate the number to the given precision by removing the least significant digits.
        /// </summary>
        /// <returns>The truncated number</returns>
        public BigDecimal Truncate(int precision)
        {
            // copy this instance (remember it's a struct)
            var shortened = this;
            // save some time because the number of digits is not needed to remove trailing zeros
            shortened.Normalize();
            // remove the least significant digits, as long as the number of digits is higher than the given Precision
            while (NumberOfDigits(shortened.Mantissa_BigInt) > precision)
            {
                shortened.Mantissa_BigInt /= 10;
                shortened.Exponent++;
            }
            // normalize again to make sure there are no trailing zeros left
            shortened.Normalize();
            return shortened;
        }


        public BigDecimal Truncate()
        {
            return Truncate(Precision);
        }

        public BigDecimal Floor()
        {
            return Truncate(BigDecimal.NumberOfDigits(Mantissa_BigInt) + Exponent);
        }

        public static int NumberOfDigits(BigInteger value)
        {
            // do not count the sign
            //return (value * value.Sign).ToString().Length;
            // faster version
            return (int)Math.Ceiling(BigInteger.Log10(value * value.Sign));
        }

        #region Conversions

        public static implicit operator BigDecimal(int value)
        {
            return new BigDecimal(value, 0);
        }

        public static implicit operator BigDecimal(double value)
        {
            var mantissa = (BigInteger)value;
            var exponent = 0;
            double scaleFactor = 1;
            while (Math.Abs(value * scaleFactor - (double)mantissa) > 0)
            {
                exponent -= 1;
                scaleFactor *= 10;
                mantissa = (BigInteger)(value * scaleFactor);
            }
            return new BigDecimal(mantissa, exponent);
        }

        public static implicit operator BigDecimal(decimal value)
        {
            var mantissa = (BigInteger)value;
            var exponent = 0;
            decimal scaleFactor = 1;
            while ((decimal)mantissa != value * scaleFactor)
            {
                exponent -= 1;
                scaleFactor *= 10;
                mantissa = (BigInteger)(value * scaleFactor);
            }
            return new BigDecimal(mantissa, exponent);
        }

        public static explicit operator double(BigDecimal value)
        {
            return (double)value.Mantissa_BigInt * Math.Pow(10, value.Exponent);
        }

        public static explicit operator float(BigDecimal value)
        {
            return Convert.ToSingle((double)value);
        }

        public static explicit operator decimal(BigDecimal value)
        {
            return (decimal)value.Mantissa_BigInt * (decimal)Math.Pow(10, value.Exponent);
        }

        public static explicit operator int(BigDecimal value)
        {
            return (int)(value.Mantissa_BigInt * BigInteger.Pow(10, value.Exponent));
        }

        public static explicit operator uint(BigDecimal value)
        {
            return (uint)(value.Mantissa_BigInt * BigInteger.Pow(10, value.Exponent));
        }

        #endregion

        #region Operators

        public static BigDecimal operator +(BigDecimal value)
        {
            return value;
        }

        public static BigDecimal operator -(BigDecimal value)
        {
            value.Mantissa_BigInt *= -1;
            return value;
        }

        public static BigDecimal operator ++(BigDecimal value)
        {
            return value + 1;
        }

        public static BigDecimal operator --(BigDecimal value)
        {
            return value - 1;
        }

        public static BigDecimal operator +(BigDecimal left, BigDecimal right)
        {
            return Add(left, right);
        }

        public static BigDecimal operator -(BigDecimal left, BigDecimal right)
        {
            return Add(left, -right);
        }

        private static BigDecimal Add(BigDecimal left, BigDecimal right)
        {
            return left.Exponent > right.Exponent
                ? new BigDecimal(AlignExponent(left, right) + right.Mantissa_BigInt, right.Exponent)
                : new BigDecimal(AlignExponent(right, left) + left.Mantissa_BigInt, left.Exponent);
        }

        public static BigDecimal operator *(BigDecimal left, BigDecimal right)
        {
            return new BigDecimal(left.Mantissa_BigInt * right.Mantissa_BigInt, left.Exponent + right.Exponent);
        }

        public static BigDecimal operator /(BigDecimal dividend, BigDecimal divisor)
        {
            var exponentChange = Precision - (NumberOfDigits(dividend.Mantissa_BigInt) - NumberOfDigits(divisor.Mantissa_BigInt));
            if (exponentChange < 0)
            {
                exponentChange = 0;
            }
            dividend.Mantissa_BigInt *= BigInteger.Pow(10, exponentChange);
            return new BigDecimal(dividend.Mantissa_BigInt / divisor.Mantissa_BigInt, dividend.Exponent - divisor.Exponent - exponentChange);
        }

        public static BigDecimal operator %(BigDecimal left, BigDecimal right)
        {
            return left - right * (left / right).Floor();
        }

        public static bool operator ==(BigDecimal left, BigDecimal right)
        {
            return left.Exponent == right.Exponent && left.Mantissa_BigInt == right.Mantissa_BigInt;
        }

        public static bool operator !=(BigDecimal left, BigDecimal right)
        {
            return left.Exponent != right.Exponent || left.Mantissa_BigInt != right.Mantissa_BigInt;
        }

        public static bool operator <(BigDecimal left, BigDecimal right)
        {
            return left.Exponent > right.Exponent ? AlignExponent(left, right) < right.Mantissa_BigInt : left.Mantissa_BigInt < AlignExponent(right, left);
        }

        public static bool operator >(BigDecimal left, BigDecimal right)
        {
            return left.Exponent > right.Exponent ? AlignExponent(left, right) > right.Mantissa_BigInt : left.Mantissa_BigInt > AlignExponent(right, left);
        }

        public static bool operator <=(BigDecimal left, BigDecimal right)
        {
            return left.Exponent > right.Exponent ? AlignExponent(left, right) <= right.Mantissa_BigInt : left.Mantissa_BigInt <= AlignExponent(right, left);
        }

        public static bool operator >=(BigDecimal left, BigDecimal right)
        {
            return left.Exponent > right.Exponent ? AlignExponent(left, right) >= right.Mantissa_BigInt : left.Mantissa_BigInt >= AlignExponent(right, left);
        }

        public static implicit operator BigDecimal(string v)
        {
            BigDecimal bd = new BigDecimal();

            char separatore = ' ';
            int dove_sepatore = -1;

            int x = v.IndexOf('.');
            if (x >= 0 && x < v.Length)
            {
                separatore = '.';
                dove_sepatore = x;
            }
            else
            {
                int y = v.IndexOf(',');
                if (y >= 0 && y < v.Length)
                {
                    separatore = ',';
                    dove_sepatore = y;
                }
                else
                {
                    bd.Mantissa_BigInt = BigInteger.Parse(v);
                    bd.Exponent = 0;
                    return bd;
                }
            }


            string v2 = v.Replace(separatore.ToString(), "");
            bd.Exponent = -(v.Length - dove_sepatore - 1);
            bd.Mantissa_BigInt = BigInteger.Parse(v2);
            return bd;
        }

        /// <summary>
        /// Returns the mantissa of value, aligned to the exponent of reference.
        /// Assumes the exponent of value is larger than of reference.
        /// </summary>
        private static BigInteger AlignExponent(BigDecimal value, BigDecimal reference)
        {
            return value.Mantissa_BigInt * BigInteger.Pow(10, value.Exponent - reference.Exponent);
        }

        #endregion

        #region Additional mathematical functions

        public static BigDecimal Exp(double exponent)
        {
            var tmp = (BigDecimal)1;
            while (Math.Abs(exponent) > 100)
            {
                var diff = exponent > 0 ? 100 : -100;
                tmp *= Math.Exp(diff);
                exponent -= diff;
            }
            return tmp * Math.Exp(exponent);
        }

        public static BigDecimal Pow(double basis, double exponent)
        {
            var tmp = (BigDecimal)1;
            while (Math.Abs(exponent) > 100)
            {
                var diff = exponent > 0 ? 100 : -100;
                tmp *= Math.Pow(basis, diff);
                exponent -= diff;
            }
            return tmp * Math.Pow(basis, exponent);
        }

        #endregion

        public override string ToString()
        {
            return string.Concat(Mantissa_BigInt.ToString(), "E", Exponent);
        }

        public DoppiaStringa ToString2()
        {
            DoppiaStringa r;

            bool negativo = false;
            BigInteger t = Mantissa_BigInt;
            if (t<0)
            {
                t = -t;
                negativo = true;
            }

            if (Exponent==0)
            {
                string m = t.ToString();
                r.p_intera = m;
                r.p_decimale = "00";
            }
            else if (Exponent<0)
            {
                string m = t.ToString();
                int e = -Exponent;
                e = m.Length - e;
                if (e==0)
                {
                    r.p_intera = "0";
                    r.p_decimale = t.ToString();
                }
                else if (e < 0)
                {
                    int q = m.Length - e + 1;
                    m = m.PadLeft(q, '0');
                    e += q - 1;
                    r.p_intera = m.Substring(0, e);
                    r.p_decimale = m.Substring(e);
                }
                else
                {
                    r.p_intera = m.Substring(0, e);
                    r.p_decimale = m.Substring(e);
                }
            }
            else //exponent > 0
            {
                string m = t.ToString();
                m = m + "".PadRight(Exponent,'0');
                r.p_decimale = "00";
                r.p_intera = m;
            }

            if (negativo)
                r.p_intera = "-" + r.p_intera;

            return r;
        }

        public bool Equals(BigDecimal other)
        {
            return other.Mantissa_BigInt.Equals(Mantissa_BigInt) && other.Exponent.Equals(Exponent);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }
            return obj is BigDecimal && Equals((BigDecimal)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Mantissa_BigInt.GetHashCode() * 397) ^ Exponent;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj is null || !(obj is BigDecimal))
            {
                throw new ArgumentException("Can't compare BigDecimal");
            }
            return CompareTo((BigDecimal)obj);
        }

        public int CompareTo(BigDecimal other)
        {
            return this < other ? -1 : (this > other ? 1 : 0);
        }
    }
}